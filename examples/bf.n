# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/stdlib.n"a;
import "posix/unistd.n"a;
import "std/io.n"a;

let STR_CELL_OOB     : ^ascii = $"ERROR: CELL OUT OF BOUNDS"a;
let STR_SOURCE_OOB   : ^ascii = $"ERROR: SOURCE OUT OF BOUNDS"a;
let STR_BRKT_OOB     : ^ascii = $"ERROR: BRACKET STACK OUT OF BOUNDS"a;
let STR_READ_FAILURE : ^ascii = $"ERROR: READ FAILURE"a;

let EXIT_FAILURE_BAD_ARGS      : s = 1s;
let EXIT_FAILURE_BAD_IO        : s = 2s;
let EXIT_FAILURE_OUT_OF_BOUNDS : s = 3s;

let cells : [30000u]mut u8 = [0u8, ..30000u];
let cell_ptr : mut ^mut u8 = $cells;
let source : [100000u]mut ascii = ['\0'a, ..100000u];
let source_ptr : mut ^mut ascii = $source;
let source_len : mut u = void;

let lbrkt_stack : [4096u]mut ^mut ascii = void;
let lbrkt_stack_ptr: mut ^mut ^mut ascii = $lbrkt_stack;

func validate_cell_bounds : () -> void
{
    if cell_ptr < $cells || cell_ptr >= $cells +^ countof(cells)
    {
        println(STR_CELL_OOB);
        exit(EXIT_FAILURE_OUT_OF_BOUNDS as sint);
    }
}

func validate_source_bounds : () -> void
{
    if  source_ptr < $source || source_ptr >= source_ptr +^ source_len
    {
        println(STR_SOURCE_OOB);
        exit(EXIT_FAILURE_OUT_OF_BOUNDS as sint);
    }
}

func validate_lbrkt_bounds : () -> void
{
    if lbrkt_stack_ptr < $lbrkt_stack
        || lbrkt_stack_ptr >= $lbrkt_stack +^ countof(lbrkt_stack)
    {
        println(STR_BRKT_OOB);
        exit(EXIT_FAILURE_OUT_OF_BOUNDS as sint);
    }
}

# >
func rmov : () -> void
{
    cell_ptr = cell_ptr +^ 1u;
    validate_cell_bounds();
}

# <
func lmov : () -> void
{
    cell_ptr = cell_ptr -^ 1u;
    validate_cell_bounds();
}

# +
func incr : () -> void
{
    @cell_ptr = @cell_ptr + 1u8;
}

# -
func decr : () -> void
{
    @cell_ptr = @cell_ptr - 1u8;
}

# .
func put : () -> void
{
    printch((@cell_ptr) as ascii);
}

# ,
func get : () -> void
{
    if -1s as ssize_t == read(STDIN_FILENO, cell_ptr, 1u as size_t)
    {
        println(STR_READ_FAILURE);
        exit(EXIT_FAILURE_BAD_IO as sint);
    }
}

# [
func lbrkt : () -> void
{
    if @cell_ptr == 0u8 # Skip the body of the [] block.
    {
        # Number of [ brackets minus number of ] brackets.
        # Initialized to one for opening [.
        let brkt_ct : mut u = 1u;

        loop brkt_ct != 0u
        {
            source_ptr = source_ptr +^ 1u;
            validate_source_bounds();
            if @source_ptr == '['a
            {
                brkt_ct = brkt_ct + 1u;
            }
            elif @source_ptr == ']'a
            {
                brkt_ct = brkt_ct - 1u;
            }
        }
    }
    else # Push this opening bracket onto the bracket stack.
    {
        @lbrkt_stack_ptr = source_ptr;
        lbrkt_stack_ptr = lbrkt_stack_ptr +^ 1u;
        validate_lbrkt_bounds();
    }
}

# ]
func rbrkt : () -> void
{
    # Pop off the last opening bracket off the bracket stack.
    lbrkt_stack_ptr = lbrkt_stack_ptr -^ 1u;
    source_ptr = @lbrkt_stack_ptr;
    validate_lbrkt_bounds();
}

func entry : (argc : u, argv : ^^ascii) -> s
{
    if argc != 2u
    {
        let usage_text : ^ascii = $"Usage: bf <file>\n"a;
        print(usage_text);
        return EXIT_FAILURE_BAD_ARGS;
    }

    let source_fd : sint = open(argv[1u], O_RDONLY, 0u as mode_t);
    let read_len : ssize_t = read(source_fd, $source, countof(source));
    close(source_fd);
    if read_len == -1s as ssize_t
    {
        println(STR_READ_FAILURE);
        return EXIT_FAILURE_BAD_IO;
    }
    source_len = read_len as u;
    # Add null terminator to the end of the string to make detecting the end
    # of the source string easier.
    source[source_len] = '\0'a;

    loop @source_ptr != '\0'a
    {
        let ch : ascii = @source_ptr;

        if   ch == '>'a {rmov();  source_ptr = source_ptr +^ 1u;}
        elif ch == '<'a {lmov();  source_ptr = source_ptr +^ 1u;}
        elif ch == '+'a {incr();  source_ptr = source_ptr +^ 1u;}
        elif ch == '-'a {decr();  source_ptr = source_ptr +^ 1u;}
        elif ch == '.'a {put();   source_ptr = source_ptr +^ 1u;}
        elif ch == ','a {get();   source_ptr = source_ptr +^ 1u;}
        elif ch == '['a {lbrkt(); source_ptr = source_ptr +^ 1u;}
        elif ch == ']'a {rbrkt();                               }
        else            {         source_ptr = source_ptr +^ 1u;}
    }

    return EXIT_SUCCESS as s;
}
