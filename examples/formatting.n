# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/format.n"a;
import "std/io.n"a;

func entry : () -> void
{
    let buf : [512u]mut ascii = void;
    let str_fmtr : mut str_formatter = void;

    let pos_num : u = 123u;
    let neg_num : s = -123s;
    let ch : ascii = 'Z'a;
    let str : ^ascii = $"this is an astr"a;

    str_fmtr.assign($buf, countof(buf), $"Formatting the number {d}."a);
    str_fmtr.format_u(pos_num);
    println($buf);
    str_fmtr.assign($buf, countof(buf), $"  Binary formatting: {b}"a);
    str_fmtr.format_u(pos_num);
    println($buf);
    str_fmtr.assign($buf, countof(buf), $"  Decimal formatting: {d}"a);
    str_fmtr.format_u(pos_num);
    println($buf);
    str_fmtr.assign($buf, countof(buf), $"  Hexidecimal formatting (upper case): {X}"a);
    str_fmtr.format_u(pos_num);
    println($buf);
    str_fmtr.assign($buf, countof(buf), $"  Hexidecimal formatting (lower case): {x}"a);
    str_fmtr.format_u(pos_num);
    println($buf);

    str_fmtr.assign($buf, countof(buf), $"Formatting the number {d}."a);
    str_fmtr.format_s(neg_num);
    println($buf);
    str_fmtr.assign($buf, countof(buf), $"  Binary formatting: {b}"a);
    str_fmtr.format_s(neg_num);
    println($buf);
    str_fmtr.assign($buf, countof(buf), $"  Decimal formatting: {d}"a);
    str_fmtr.format_s(neg_num);
    println($buf);
    str_fmtr.assign($buf, countof(buf), $"  Hexidecimal formatting (upper case): {X}"a);
    str_fmtr.format_s(neg_num);
    println($buf);
    str_fmtr.assign($buf, countof(buf), $"  Hexidecimal formatting (lower case): {x}"a);
    str_fmtr.format_s(neg_num);
    println($buf);

    str_fmtr.assign($buf, countof(buf), $"Formatting the character \'{a}\'."a);
    str_fmtr.format_ascii(ch);
    println($buf);

    str_fmtr.assign($buf, countof(buf), $"Formatting the str \"{s}\"."a);
    str_fmtr.format_astr(str);
    println($buf);

    str_fmtr.assign($buf, countof(buf), $"Formatting a range [5, 9) of the previous string \"{s}\"."a);
    str_fmtr.format_astr_range(str +^ 5u, str +^ 9u);
    println($buf);

    str_fmtr.assign($buf, countof(buf), $"Formatting the pointer {p}."a);
    str_fmtr.format_pointer($buf);
    println($buf);

    str_fmtr.assign($buf, countof(buf), $"Multiple format specifiers {d}, {s}, {p}."a);
    str_fmtr.format_u(pos_num);
    str_fmtr.format_astr(str);
    str_fmtr.format_pointer($buf);
    println($buf);
}
