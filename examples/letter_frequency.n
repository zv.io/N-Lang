# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# From http://rosettacode.org/wiki/Letter_frequency
# Task:
#   Open a text file and count the occurrences of each letter.
#   Some of these programs count all characters (including punctuation), but
#   some only count letters A to Z.
# This version of the letter frequency program will only count letters from
# A to Z.
# The program does not differentiate between upper and lower case, so Z and z
# are counted as the same letter.
import "std/character.n"a;
import "std/io.n"a;
import "std/string.n"a;

func entry : (argc : u, argv : ^^ascii) -> s
{
    if (argc != 2u)
    {
        let usage : ^ascii = $"Usage: letter_frequency <file>"a;
        println(usage);
        return 1s;
    }

    let path : ^ascii = argv[1u];
    let str : mut string = void;
    str.init();
    if (-1s == read_file_into_string(path, ?str))
    {
        eprint($"Failed to read in file \'"a);
        eprint(path);
        eprintln($"\'."a);
        return 1s;
    }

    let freq : [26u]mut u = [0u, ..26u];
    loop i : mut u in [0u, str.length())
    {
        let ch : ascii = ascii_to_upper(@str.at(i));
        if (ascii_is_alphabetic(ch))
        {
            let target_freq : ^mut u = ?freq[ch as u8 - 'A'a as u8];
            @target_freq = @target_freq + 1u;
        }
    }

    loop i : mut u in [0u, 26u)
    {
        printch(('A'a as u + i) as ascii);
        printch(':'a);
        loop j : mut u in [0u, freq[i])
        {
            printch('*'a);
        }
        printch('\n'a);
    }

    return 0s;
}
