# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# The ROT 13 (Rotate by 13) is a special case of the Caesar Cipher which uses
# a shift of 13. The 13 shift over an alphabet of 26 characters creates an
# operation which is idempotent with itself.

import "std/io.n"a;
import "std/math.n"a;
import "std/str.n"a;

func rotate13 : (c : ascii) -> ascii
{
    let ret : mut ascii = void;
    let d : u8 = c as u8;
    let a : u8 = 'a'a as u8;
    let A : u8 = 'A'a as u8;

    if d >= a && d <= ('z'a as u8)
    {
        ret = (a + u8_mod((d - a) + 13u8, 26u8)) as ascii;
    }
    elif d >= A && d <= ('Z'a as u8)
    {
        ret = (A + u8_mod((d - A) + 13u8, 26u8)) as ascii;
    }

    return ret;
}

func entry : (argc : u, argv : ^^ ascii) -> void
{
    if argc > 1u
    {
        loop i : mut u in [1u, argc)
        {
            loop j : mut u in [0u, str_length(argv[i]))
            {
                printch(rotate13(argv[i][j]));
            }
            printch('\n'a);
        }
    }
}
