# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/io.n"a;

let buf : [512u]mut ascii = void;
let fmtr : mut str_formatter = void;

func doprinti : (i : u) -> void
{
    fmtr.assign($buf, countof(buf), $"i = {d}"a);
    fmtr.format_u(i);
    println($buf);
}

func entry : () -> void
{
    println($"Range-loop syntax examples:"a);

    println($"\nloop i : mut u in [0u, 3u]"a);
    loop i : mut u in [0u, 3u]
    {
        doprinti(i);
    }

    println($"\nloop i : mut u in [0u, 3u)"a);
    loop i : mut u in [0u, 3u)
    {
        doprinti(i);
    }

    println($"\nloop i : mut u in (0u, 3u]"a);
    loop i : mut u in (0u, 3u]
    {
        doprinti(i);
    }

    println($"\nloop i : mut u in (0u, 3u)"a);
    loop i : mut u in (0u, 3u)
    {
        doprinti(i);
    }
}
