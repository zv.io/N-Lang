/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nc/link.h>
#include <stdlib.h>

stbool link_modules(void)
{
    const char *nc99 = getenv("NC99");
    nassert(nc99 && *nc99);
    nstr_t nstr_defer_fini posix_c99_system_cmd;
    nstr_init_fmt(&posix_c99_system_cmd, "%s -o %s", nc99, config.output_file);
    for (size_t i = 0; i < module_cache_len; ++i)
    {
        nstr_cat_fmt(&posix_c99_system_cmd, " %s", module_cache[i].out_path.data);
    }
    nstr_cat_cstr(&posix_c99_system_cmd, " -lm");
    nlogf(LOG_DEBUG, "Invoking the POSIX c99 compiler (linking).");
    nlogf(LOG_DEBUG, "$ %s", posix_c99_system_cmd.data);

    if (0 != system(posix_c99_system_cmd.data))
    {
        nlogf(LOG_ERROR, "Failed to link using the POSIX c99 compiler.");
        return STBOOL_FAILURE;
    }

    return STBOOL_SUCCESS;
}
