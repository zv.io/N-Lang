/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nc/token.h>

//! Convert a token to a printable string
char const*
token_type_to_cstr(enum token_type ttype)
{
  switch (ttype) {
    // Keywords
    case TOKEN_KEYWORD_ADDRESSOF:
      return "addressof";
    case TOKEN_KEYWORD_ALIAS:
      return "alias";
    case TOKEN_KEYWORD_ALIGNAS:
      return "alignas";
    case TOKEN_KEYWORD_ALIGNOF:
      return "alignof";
    case TOKEN_KEYWORD_AS:
      return "as";
    case TOKEN_KEYWORD_BOOL:
      return "bool";
    case TOKEN_KEYWORD_BREAK:
      return "break";
    case TOKEN_KEYWORD_ASCII:
      return "ascii";
    case TOKEN_KEYWORD_CONTINUE:
      return "continue";
    case TOKEN_KEYWORD_COUNTOF:
      return "countof";
    case TOKEN_KEYWORD_DEFER:
      return "defer";
    case TOKEN_KEYWORD_ELIF:
      return "elif";
    case TOKEN_KEYWORD_ELSE:
      return "else";
    case TOKEN_KEYWORD_EXPORT:
      return "export";
    case TOKEN_KEYWORD_EXTERN:
      return "extern";
    case TOKEN_KEYWORD_F128:
      return "f128";
    case TOKEN_KEYWORD_F16:
      return "f16";
    case TOKEN_KEYWORD_F32:
      return "f32";
    case TOKEN_KEYWORD_F64:
      return "f64";
    case TOKEN_KEYWORD_FALSE:
      return "false";
    case TOKEN_KEYWORD_FUNC:
      return "func";
    case TOKEN_KEYWORD_IF:
      return "if";
    case TOKEN_KEYWORD_IMPORT:
      return "import";
    case TOKEN_KEYWORD_IN:
      return "in";
    case TOKEN_KEYWORD_LET:
      return "let";
    case TOKEN_KEYWORD_LOOP:
      return "loop";
    case TOKEN_KEYWORD_MUT:
      return "mut";
    case TOKEN_KEYWORD_NULL:
      return "null";
    case TOKEN_KEYWORD_RESTRICT:
      return "restrict";
    case TOKEN_KEYWORD_RETURN:
      return "return";
    case TOKEN_KEYWORD_S:
      return "s:";
    case TOKEN_KEYWORD_S16:
      return "s16";
    case TOKEN_KEYWORD_S32:
      return "s32";
    case TOKEN_KEYWORD_S64:
      return "s64";
    case TOKEN_KEYWORD_S8:
      return "s8";
    case TOKEN_KEYWORD_SIZEOF:
      return "sizeof";
    case TOKEN_KEYWORD_STRING:
      return "string";
    case TOKEN_KEYWORD_STRUCT:
      return "struct";
    case TOKEN_KEYWORD_TRUE:
      return "true";
    case TOKEN_KEYWORD_TYPEOF:
      return "typeof";
    case TOKEN_KEYWORD_U:
      return "u:";
    case TOKEN_KEYWORD_U16:
      return "u16";
    case TOKEN_KEYWORD_U32:
      return "u32";
    case TOKEN_KEYWORD_U64:
      return "u64";
    case TOKEN_KEYWORD_U8:
      return "u8";
    case TOKEN_KEYWORD_VOID:
      return "void";
    case TOKEN_KEYWORD_VOLATILE:
      return "volatile";

    // LENGTH 3 OPERATORS
    case TOKEN_OPERATOR_DASH_CARET_CARET:
      return "-^^";
    // LENGTH 2 OPERATORS
    case TOKEN_OPERATOR_AMPERSAND_AMPERSAND:
      return "&&";
    case TOKEN_OPERATOR_BANG_EQUAL:
      return "!=";
    case TOKEN_OPERATOR_DASH_CARET:
      return "-^";
    case TOKEN_OPERATOR_DASH_GREATERTHAN:
      return "->";
    case TOKEN_OPERATOR_DOT_DOT:
      return "..";
    case TOKEN_OPERATOR_EQUAL_EQUAL:
      return "==";
    case TOKEN_OPERATOR_GREATERTHAN_EQUAL:
      return ">=";
    case TOKEN_OPERATOR_GREATERTHAN_GREATERTHAN:
      return ">>";
    case TOKEN_OPERATOR_LESSTHAN_EQUAL:
      return "<=";
    case TOKEN_OPERATOR_LESSTHAN_LESSTHAN:
      return "<<";
    case TOKEN_OPERATOR_PIPE_PIPE:
      return "||";
    case TOKEN_OPERATOR_PLUS_CARET:
      return "+^";
    // LENGTH 1 OPERATORS
    case TOKEN_OPERATOR_AMPERSAND:
      return "&";
    case TOKEN_OPERATOR_ASTERISK:
      return "*";
    case TOKEN_OPERATOR_AT:
      return "@";
    case TOKEN_OPERATOR_BANG:
      return "!";
    case TOKEN_OPERATOR_CARET:
      return "^";
    case TOKEN_OPERATOR_COLON:
      return ":";
    case TOKEN_OPERATOR_COMMA:
      return ",";
    case TOKEN_OPERATOR_DOLLAR:
      return "$";
    case TOKEN_OPERATOR_DOT:
      return ".";
    case TOKEN_OPERATOR_EQUAL:
      return "=";
    case TOKEN_OPERATOR_GREATERTHAN:
      return ">";
    case TOKEN_OPERATOR_LEFTBRACE:
      return "{";
    case TOKEN_OPERATOR_LEFTBRACKET:
      return "[";
    case TOKEN_OPERATOR_LEFTPARENTHESIS:
      return "(";
    case TOKEN_OPERATOR_LESSTHAN:
      return "<";
    case TOKEN_OPERATOR_PIPE:
      return "|";
    case TOKEN_OPERATOR_QUESTION:
      return "?";
    case TOKEN_OPERATOR_RIGHTBRACE:
      return "}";
    case TOKEN_OPERATOR_RIGHTBRACKET:
      return "]";
    case TOKEN_OPERATOR_RIGHTPARENTHESIS:
      return ")";
    case TOKEN_OPERATOR_SEMICOLON:
      return ";";
    case TOKEN_OPERATOR_SLASH:
      return "/";
    case TOKEN_OPERATOR_TILDE:
      return "~";
    // SPECIAL CASES
    case TOKEN_OPERATOR_DASH:
      return "-";
    case TOKEN_OPERATOR_PLUS:
      return "+";

    // VALUES
    case TOKEN_IDENTIFIER:
      return "identifier";
    case TOKEN_LITERAL_U8:
      return "literal-u8";
    case TOKEN_LITERAL_U16:
      return "literal-u16";
    case TOKEN_LITERAL_U32:
      return "literal-u32";
    case TOKEN_LITERAL_U64:
      return "literal-u64";
    case TOKEN_LITERAL_U:
      return "literal-u";
    case TOKEN_LITERAL_S8:
      return "literal-s8";
    case TOKEN_LITERAL_S16:
      return "literal-s16";
    case TOKEN_LITERAL_S32:
      return "literal-s32";
    case TOKEN_LITERAL_S64:
      return "literal-s64";
    case TOKEN_LITERAL_S:
      return "literal-s";
    case TOKEN_LITERAL_F16:
      return "literal-f16";
    case TOKEN_LITERAL_F32:
      return "literal-f32";
    case TOKEN_LITERAL_F64:
      return "literal-f64";
    case TOKEN_LITERAL_F128:
      return "literal-f128";
    case TOKEN_LITERAL_ASCII:
      return "literal-ascii";
    case TOKEN_LITERAL_STRING:
      return "literal-string";

    case TOKEN_EOF:
      return "EOF";
  }
  return "unknown";
}
