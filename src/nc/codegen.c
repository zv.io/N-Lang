/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nc/codegen.h>

stbool dophase_codegen(struct module* module)
{
    switch(config.target)
    {
    case TARGET_NATIVE:
        return native_codegen(module);
    case TARGET_C99:
        return c99_codegen(module, NULL);
    default:
        NPANIC_DFLT_CASE();
    }
}

//============================================================================//
//      NATIVE                                                                //
//============================================================================//
stbool native_codegen(struct module* module)
{
    nstr_t nstr_defer_fini intermediate_c_file_name;
    nstr_init_nstr(&intermediate_c_file_name, &module->out_path);
    nstr_cat_cstr(&intermediate_c_file_name, ".c");

    if (!c99_codegen(module, intermediate_c_file_name.data))
    {
        return STBOOL_FAILURE;
    }

    nstr_t nstr_defer_fini posix_c99_system_cmd;
    const char *nc99 = getenv("NC99");
    nassert(nc99 && *nc99);
    nstr_init_fmt(
        &posix_c99_system_cmd,
        "%s -c %s %s -o %s %s",
        nc99,
        config.is_gen_debug_info ? "-g" : "",
        config.is_optimize ? "-O2" : "",
        module->out_path.data,
        intermediate_c_file_name.data
    );
    nlogf(LOG_DEBUG, "Invoking the POSIX c99 compiler.");
    nlogf(LOG_DEBUG, "$ %s", posix_c99_system_cmd.data);
    if (0 != system(posix_c99_system_cmd.data))
    {
        nlogf(LOG_ERROR, "Failed to compile using the POSIX c99 compiler.");
        return STBOOL_FAILURE;
    }

    if (!config.is_save_temps)
    {
        nlogf(LOG_DEBUG, "Removing file '%s'", intermediate_c_file_name.data);
        remove(intermediate_c_file_name.data);
    }

    return STBOOL_SUCCESS;
}

//============================================================================//
//      C99                                                                   //
//============================================================================//
#define C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_VOID                          \
"int main(void)\n"                                                             \
"{\n"                                                                          \
"    /* entry : () -> void */\n"                                               \
"    entry();\n"                                                               \
"    return 0;\n"                                                              \
"}\n"                                                                          \

#define C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_U                             \
"int main(void)\n"                                                             \
"{\n"                                                                          \
"    /* entry : () -> u */\n"                                                  \
"    int rtn = (int)entry();\n"                                                \
"    return rtn >= 0 ? rtn : -rtn;\n"                                          \
"}\n"                                                                          \

#define C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_S                             \
"int main(void)\n"                                                             \
"{\n"                                                                          \
"    /* entry : () -> s */\n"                                                  \
"    return (int)entry();\n"                                                   \
"}\n"                                                                          \

#define C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_VOID                             \
"int main(int argc, char** argv)\n"                                            \
"{\n"                                                                          \
"    /* func entry : (argc : u, argv : **char) -> u */\n"                      \
"    entry(argc, (char const* const*)argv);\n"                                 \
"    return 0;\n"                                                              \
"}\n"                                                                          \

#define C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_U                                \
"int main(int argc, char** argv)\n"                                            \
"{\n"                                                                          \
"    /* func entry : (argc : u, argv : **char) -> u */\n"                      \
"    int rtn = (int)entry(argc, (char const* const*)argv);\n"                  \
"    return rtn >= 0 ? rtn : -rtn;\n"                                          \
"}\n"                                                                          \

#define C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_S                                \
"int main(int argc, char** argv)\n"                                            \
"{\n"                                                                          \
"    /* func entry : (argc : u, argv : **char) -> s */\n"                      \
"    return (int)entry(argc, (char const* const*)argv);\n"                     \
"}\n"                                                                          \

#define C99_CODEGEN_INDENT "    "

#define C99_SCOPE_END_LABEL "___scope_end_%zu"

#define C99_BREAK_CONDITIONAL_DECLARATION      "int ___do_break = 0;\n"
#define C99_BREAK_CONDITIONAL_SET_TRUE "/*break[0]*/___do_break = 1;\n"
#define C99_BREAK_SCOPE_END__NESTED \
    "if (___do_break){goto " C99_SCOPE_END_LABEL ";}\n"
#define C99_BREAK_SCOPE_END__NOT_NESTED \
    "if (___do_break){break;}\n"

#define C99_DEFER_CONDITIONAL_DECLARATION   "int ___do_defer_%u = 0;\n"
#define C99_DEFER_CONDITIONAL_SET_TRUE "/*defer*/___do_defer_%u = 1;\n"
#define C99_DEFER_SCOPE_END_IF_STATEMENT \
    "/*deferred scope*/if (___do_defer_%u)\n"

static inline void c99_codegen_do_indent(struct c99_codegen_context* ctx)
{
    for (size_t i = 0; i < ctx->indent_level; ++i)
    {
        C99_SRC_APPEND(ctx, C99_CODEGEN_INDENT);
    }
}

static inline void _c99_codegen_var_decl(
    struct c99_codegen_context* ctx,
    bool is_extern,
    bool is_export,
    struct id* id,
    struct dt* dt
)
{
    if (is_extern)
    {
        C99_SRC_APPEND(ctx, "extern ");
    }
    else if ((/*global scope*/0 == ctx->indent_level) && !is_export)
    {
        C99_SRC_APPEND(ctx, "static ");
    }
    c99_codegen_dt(ctx, dt, id);
}

static inline void _c99_codegen_func_decl(
    struct c99_codegen_context* ctx,
    bool is_extern,
    bool is_export,
    struct id* id,
    struct dt* dt,
    narr_t(struct id) param_ids
)
{
    nassert(dt_is_base(dt));

    nstr_t nstr_defer_fini storage;
    nstr_init(&storage);
    if (
        (/*global scope*/0 == ctx->indent_level)
        && !is_export
        && !is_extern
    )
    {
        nstr_cat_cstr(&storage, "static ");
    }

    nstr_t nstr_defer_fini rtn =
        dt_to_c_source_nstr(&dt->func_sig->rtn_dt, NULL);

    size_t const params_len = narr_length(param_ids);
    nstr_t nstr_defer_fini params;
    nstr_init(&params);
    if (0 == params_len)
    {
        nstr_cat_cstr(&params, "void");
    }
    for (size_t i = 0; i < params_len; ++i)
    {
        nstr_t nstr_defer_fini param =
            dt_to_c_source_nstr(&dt->func_sig->param_dts[i], &(param_ids[i]));
        nstr_cat_cstr(&params, param.data);
        if (i != params_len-1){nstr_cat_cstr(&params, ", ");}
    }

    nstr_t nstr_defer_fini decl;
    nstr_init_fmt(
        &decl,
        "%s %s %.*s(%s)",
        storage.data,
        rtn.data,
        (int)id->length,
        id->start,
        params.data
    );
    C99_SRC_APPEND(ctx, decl.data);
}

static inline void _c99_codegen_alias_dt(
    struct c99_codegen_context* ctx,
    struct id* new_id,
    struct dt* dt
)
{
    C99_SRC_APPEND(ctx, "typedef ");
    c99_codegen_dt(ctx, dt, NULL);
    C99_SRC_APPEND(ctx, " ");
    c99_codegen_id(ctx, new_id);
    C99_SRC_APPEND(ctx, ";");
}

void c99_codegen_context_init(struct c99_codegen_context* ctx, struct module* module)
{
    ctx->module = module;
    ctx->indent_level = 0;
    nstr_init(&ctx->c_code_nstr);
}

void c99_codegen_context_fini(struct c99_codegen_context* ctx)
{
    nstr_fini(&ctx->c_code_nstr);
}

stbool c99_codegen(struct module* module, char const* out_path_override)
{
    char const* const out_path =
        out_path_override ? out_path_override : module->out_path.data;

    struct c99_codegen_context ctx;
    c99_codegen_context_init(&ctx, module);

    nstr_t nstr_defer_fini nlang_core_c99;
    nstr_init(&nlang_core_c99);
    const char *ncid = getenv("NCID");
    nassert(ncid && *ncid);
    char *ncid_ = malloc(strlen(ncid) + strlen("core.c99.h") + 1);
    nassert(NULL != ncid_);
    sprintf(ncid_, "%s/%s", ncid, "core.c99.h");
    ssize_t bytes_read = file_read_into_nstr(
        ncid_,
        &nlang_core_c99
    );
    free(ncid_);
    if (-1 == bytes_read)
    {
        nlogf(LOG_ERROR, "Failed to read in nlang c99 core.");
        return STBOOL_FAILURE;
    }

    // Append nlang core.
    C99_SRC_APPEND(&ctx, nlang_core_c99.data);
    C99_SRC_APPEND(&ctx, "\n");

    // Append all imports.
    C99_SRC_APPEND(&ctx, "/* BEGIN IMPORTS */\n");
    size_t const exports_len = narr_length(module->exports);
    for (size_t i = 0; i < exports_len; ++i)
    {
        struct id* const export = &module->exports[i];
        if (is_import(module, export->start, export->length))
        {
            struct symbol* import =
                symbol_table_find(module->global_scope.symbol_table, export->start, export->length);
            switch (import->tag)
            {
            case SYMBOL_VARIABLE:
                {
                    _c99_codegen_var_decl(
                        &ctx,
                        true,
                        true,
                        (struct id*)import->id,
                        (struct dt*)import->dt
                    );
                    C99_SRC_APPEND(&ctx, ";");
                }
                break;
            case SYMBOL_FUNCTION:
                {
                    _c99_codegen_func_decl(
                        &ctx,
                        true,
                        true,
                        (struct id*)import->id,
                        (struct dt*)import->dt,
                        import->ref.func_decl->param_ids
                    );
                    C99_SRC_APPEND(&ctx, ";");
                }
                break;
            case SYMBOL_STRUCT:
                {
                    struct struct_def* struct_def = NULL;
                    if (NULL != import->dt)
                    {
                        nassert(DT_STRUCT == import->dt->tag);
                        nassert(0 == dt_length_modifiers(import->dt));
                        struct_def = import->dt->struct_ref->def;
                    }
                    c99_codegen_struct_decl(
                        &ctx,
                        import->ref.struct_decl,
                        struct_def
                    );
                }
                break;
            case SYMBOL_ALIAS:
                {
                    _c99_codegen_alias_dt(
                        &ctx,
                        (struct id*)import->id,
                        (struct dt*)import->dt
                    );
                }
                break;
            default:
                NPANIC_DFLT_CASE();
            }
            C99_SRC_APPEND(&ctx, "\n");
        }
    }
    C99_SRC_APPEND(&ctx, "/* END IMPORTS */\n\n");

    // Append this module's contents.
    c99_codegen_scope(&ctx, &module->global_scope);

    int const contains_entry_func_result =
        scope_contains_entry_func(&module->global_scope);
    if (INVALID_ENTRY_FUNC == contains_entry_func_result)
    {
        nlogf(LOG_ERROR, "Invalid type signature for entry function.");
        return STBOOL_FAILURE;
    }
    else if (ENTRY_FUNC_NO_ARGS_RTN_VOID == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_VOID);
    }
    else if (ENTRY_FUNC_NO_ARGS_RTN_U == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_U);
    }
    else if (ENTRY_FUNC_NO_ARGS_RTN_S == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_NO_ARGS_RTN_S);
    }
    else if (ENTRY_FUNC_ARGS_RTN_VOID == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_VOID);
    }
    else if (ENTRY_FUNC_ARGS_RTN_U == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_U);
    }
    else if (ENTRY_FUNC_ARGS_RTN_S == contains_entry_func_result)
    {
        C99_SRC_APPEND(&ctx, C99_CODEGEN_MAIN__ENTRY_FUNC_ARGS_RTN_S);
    }

    FILE* outfile = fopen(out_path, "w");
    if (NULL == outfile)
    {
        nlogf(
            LOG_ERROR,
            "Failed to open C file '%s'.",
            out_path
        );
        c99_codegen_context_fini(&ctx);
        return STBOOL_FAILURE;
    }
    fprintf(outfile, "%s", ctx.c_code_nstr.data);
    fclose(outfile);

    c99_codegen_context_fini(&ctx);
    return STBOOL_SUCCESS;
}

void c99_codegen_scope(
    struct c99_codegen_context* ctx,
    struct scope* scope
)
{
    uint32_t const current_scope_uid_save = ctx->current_scope_uid;
    ctx->current_scope_uid = scope->uid;

    if (!scope_is_global(scope))
    {
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "{\n");
        ctx->indent_level += 1;

        if (!scope->is_within_defer && scope->is_within_loop_primary)
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(
                ctx,
                C99_BREAK_CONDITIONAL_DECLARATION
            );
        }
        for (size_t i = 0; i < narr_length(scope->deferred_scopes); ++i)
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, C99_DEFER_CONDITIONAL_DECLARATION, i);
        }
    }

    for (size_t i = 0; i < narr_length(scope->stmts); ++i)
    {
        c99_codegen_stmt(ctx, &(scope->stmts[i]));
        C99_SRC_APPEND(ctx, "\n");
    }

    if (!scope_is_global(scope))
    {
        C99_SRC_APPEND(ctx, "\n");
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, C99_SCOPE_END_LABEL ":;\n", scope->uid);

        for (size_t i = 0; i < narr_length(scope->deferred_scopes); ++i)
        {
            size_t const idx = narr_length(scope->deferred_scopes) - i - 1;
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, C99_DEFER_SCOPE_END_IF_STATEMENT, idx);
            c99_codegen_scope(ctx, scope->deferred_scopes[idx]);
        }

        if (!scope->is_within_defer)
        {
            if (scope->is_within_loop_primary)
            {
                c99_codegen_do_indent(ctx);
                C99_SRC_APPEND(
                    ctx,
                    C99_BREAK_SCOPE_END__NOT_NESTED
                );
            }
            else if(scope->is_within_loop)
            {
                c99_codegen_do_indent(ctx);
                C99_SRC_APPEND(
                    ctx,
                    C99_BREAK_SCOPE_END__NESTED,
                    scope->parent->uid
                );
            }
        }

        ctx->indent_level -= 1;
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "}\n");
    }

    ctx->current_scope_uid = current_scope_uid_save;
}

void c99_codegen_stmt(
    struct c99_codegen_context* ctx,
    struct stmt* stmt
)
{
    switch (stmt->tag)
    {
    case STMT_EMPTY:
        C99_SRC_APPEND(ctx, ";");
        break;

    case STMT_VARIABLE_DECLARATION:
        c99_codegen_do_indent(ctx);
        c99_codegen_var_decl(ctx, stmt->var_decl);
        if (NULL != stmt->var_decl->def)
        {
            C99_SRC_APPEND(ctx, " = ");
            c99_codegen_expr(ctx, stmt->var_decl->def);
        }
        C99_SRC_APPEND(ctx, ";");
        break;

    case STMT_FUNCTION_DECLARATION:
        c99_codegen_do_indent(ctx);
        c99_codegen_func_decl(ctx, stmt->func_decl);
        if (NULL == stmt->func_decl->def)
        {
            C99_SRC_APPEND(ctx, ";");
        }
        else
        {
            C99_SRC_APPEND(ctx, "\n");
            c99_codegen_scope(ctx, stmt->func_decl->def);
        }
        break;

    case STMT_STRUCT_DECLARATION:
        c99_codegen_struct_decl(ctx, stmt->struct_decl, stmt->struct_decl->def);
        break;

    case STMT_IF_ELIF_ELSE:
        for (
            size_t i = 0;
            i < narr_length(stmt->if_elif_else->if_elif_ind_cond);
            ++i
        )
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, "%s (", i == 0 ? "if" : "else if");
            c99_codegen_expr(ctx, &stmt->if_elif_else->if_elif_ind_cond[i].condition);
            C99_SRC_APPEND(ctx, ")\n");
            c99_codegen_scope(ctx, stmt->if_elif_else->if_elif_ind_cond[i].body);
        }
        if (stmt->if_elif_else->else_body != NULL)
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, "else\n");
            c99_codegen_scope(ctx, stmt->if_elif_else->else_body);
        }
        break;

    case STMT_LOOP:
        c99_codegen_loop(ctx, stmt->loop);
        break;

    case STMT_DEFER:
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, C99_DEFER_CONDITIONAL_SET_TRUE, stmt->defer.idx);
        break;

    case STMT_BREAK:
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(
            ctx,
            C99_BREAK_CONDITIONAL_SET_TRUE
        );
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(
            ctx,
            "/*break[1]*/goto " C99_SCOPE_END_LABEL ";",
            ctx->current_scope_uid
        );
        break;

    case STMT_CONTINUE:
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(
            ctx,
            "/*continue*/goto " C99_SCOPE_END_LABEL ";",
            ctx->current_scope_uid
        );
        break;

    case STMT_RETURN:
        c99_codegen_do_indent(ctx);
        C99_SRC_APPEND(ctx, "return");
        if (NULL != stmt->rtn_expr)
        {
            C99_SRC_APPEND(ctx, " ");
            c99_codegen_expr(ctx, stmt->rtn_expr);
        }
        C99_SRC_APPEND(ctx, ";");
        break;

    case STMT_EXPR:
        c99_codegen_do_indent(ctx);
        c99_codegen_expr(ctx, stmt->expr);
        C99_SRC_APPEND(ctx, ";");
        break;

    case STMT_SCOPE:
        c99_codegen_scope(ctx, stmt->scope);
        break;

    case STMT_IMPORT:
        /* nothing */
        break;

    case STMT_ALIAS:
        c99_codegen_alias(ctx, stmt->alias);
        break;

    default:
        NPANIC_DFLT_CASE();
    }
}

void c99_codegen_alias(
    struct c99_codegen_context* ctx,
    struct alias* alias
)
{
    switch (alias->tag)
    {
    case ALIAS_DT:
        _c99_codegen_alias_dt(
            ctx,
            &alias->new_id,
            &alias->aliased.dt
        );
        break;

    default:
        NPANIC_DFLT_CASE();
    }
}

void c99_codegen_var_decl(
    struct c99_codegen_context* ctx,
    struct var_decl* decl
)
{
    _c99_codegen_var_decl(
        ctx,
        decl->is_extern,
        decl->is_export,
        &decl->id,
        &decl->dt
    );
}

void c99_codegen_func_decl(
    struct c99_codegen_context* ctx,
    struct func_decl* decl
)
{
    _c99_codegen_func_decl(
        ctx,
        decl->is_extern,
        decl->is_export,
        &decl->id,
        &decl->dt,
        decl->param_ids
    );
}

void c99_codegen_struct_decl(
    struct c99_codegen_context* ctx,
    struct struct_decl* decl,
    struct struct_def* def
)
{
    C99_SRC_APPEND(ctx, "struct ");
    c99_codegen_id(ctx, &decl->id);
    if (NULL != def)
    {
        c99_codegen_struct_def(ctx, def);
    }
    C99_SRC_APPEND(ctx, ";");
}

void c99_codegen_loop(
    struct c99_codegen_context* ctx,
    struct loop* loop
)
{
    switch (loop->tag)
    {
    case LOOP_CONDITIONAL:
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, "while (");
            c99_codegen_expr(ctx, loop->cond.expr);
            C99_SRC_APPEND(ctx, ")\n");
            c99_codegen_scope(ctx, loop->body);
        }
        break;

    case LOOP_RANGE:
        {
            c99_codegen_do_indent(ctx);
            C99_SRC_APPEND(ctx, "for (");
            _c99_codegen_var_decl(
                ctx,
                false,
                false,
                &loop->range.var_decl.id,
                &loop->range.var_decl.dt
            );

            C99_SRC_APPEND(ctx, " = (");
            c99_codegen_expr(ctx, loop->range.lower_bound);
            if (LOWER_INCLUSIVE == loop->range.lower_clusivity)
            {
                C99_SRC_APPEND(ctx, ")/*inclusive*/; ");
            }
            else if (LOWER_EXCLUSIVE ==  loop->range.lower_clusivity)
            {
                C99_SRC_APPEND(ctx, ")/*exclusive*/+1; ");
            }
            else{NPANIC_UNEXPECTED_CTRL_FLOW();}

            int const id_len = (int)loop->range.var_decl.id.length;
            char const* const id_start = loop->range.var_decl.id.start;
            if (UPPER_INCLUSIVE == loop->range.upper_clusivity)
            {
                C99_SRC_APPEND(ctx, "%.*s <= (", id_len, id_start);
                c99_codegen_expr(ctx, loop->range.upper_bound);
                C99_SRC_APPEND(ctx, ")/*inclusive*/; ");
            }
            else if (UPPER_EXCLUSIVE ==  loop->range.upper_clusivity)
            {
                C99_SRC_APPEND(ctx, "%.*s < (", id_len, id_start);
                c99_codegen_expr(ctx, loop->range.upper_bound);
                C99_SRC_APPEND(ctx, ")/*exclusive*/; ");
            }
            else{NPANIC_UNEXPECTED_CTRL_FLOW();}

            C99_SRC_APPEND(ctx, "++%.*s)\n", id_len, id_start);
            c99_codegen_scope(ctx, loop->body);
        }
        break;

    case LOOP_UNSET:
        NPANIC_UNSET_CASE();

    default:
        NPANIC_DFLT_CASE();
    }
}

void c99_codegen_expr(
    struct c99_codegen_context* ctx,
    struct expr* expr
)
{
    if (expr_is_primary(expr))
    {
        switch (expr->tag)
        {
        case EXPR_PRIMARY_IDENTIFIER:
            c99_codegen_id(ctx, &expr->id);
            break;
        case EXPR_PRIMARY_LITERAL:
            c99_codegen_literal(ctx, expr->literal);
            break;
        case EXPR_PRIMARY_PAREN:
            C99_SRC_APPEND(ctx, "(");
            c99_codegen_expr(ctx, expr->paren);
            C99_SRC_APPEND(ctx, ")");
            break;
        default:
            NPANIC_DFLT_CASE();
        }
    }
    else if (expr_is_postfix(expr))
    {
        switch (expr->tag)
        {
        case EXPR_POSTFIX_FUNCTION_CALL:
            {
                nassert(dt_is_function(&expr->postfix.lhs->dt));
                size_t const args_len = narr_length(expr->postfix.args);

                // If the expression is an invocation of a member function, we
                // need to translate to a regular function call using the member
                // function's target.
                // We will preemptively check if the left hand side of the
                // expression is accessing a member_function and if so generate
                // code for the corresponding function.
                bool const is_dot_member_func_call =
                    (EXPR_POSTFIX_ACCESS_DOT == expr->postfix.lhs->tag)
                    && expr->postfix.lhs->postfix.access.is_member_func;
                bool const is_arrow_member_func_call =
                    (EXPR_POSTFIX_ACCESS_ARROW == expr->postfix.lhs->tag)
                    && expr->postfix.lhs->postfix.access.is_member_func;
                bool const is_member_func_call =
                    is_dot_member_func_call || is_arrow_member_func_call;

                if (is_member_func_call)
                {
                    struct expr* const member_expr = expr->postfix.lhs;
                    struct expr* const instance_expr = member_expr->postfix.lhs;
                    nassert(member_expr->postfix.access.is_member_func);

                    struct id const* id = is_dot_member_func_call ?
                        &instance_expr->dt.struct_ref->id :
                        &instance_expr->dt.inner->struct_ref->id;

                    struct symbol* const symbol =
                        symbol_table_find(ctx->module->global_scope.symbol_table, id->start, id->length);
                    nassert(NULL != symbol);

                    struct struct_def* const struct_def =
                        symbol->dt->struct_ref->def;
                    nassert(NULL != struct_def);

                    struct id const* access_id = &member_expr->postfix.access.id;
                    ssize_t const member_func_idx =
                        struct_def_member_func_idx(struct_def, access_id);
                    nassert(MEMBER_FUNC_NOT_FOUND != member_func_idx);

                    struct member_func* const member_func =
                        &struct_def->member_funcs[member_func_idx];

                    C99_SRC_APPEND(ctx, "/*member func call*/");
                    c99_codegen_id(ctx, &member_func->target.id);
                }
                else
                {
                    c99_codegen_expr(ctx, expr->postfix.lhs);
                }
                C99_SRC_APPEND(ctx, "(");
                for (size_t i = 0; i < args_len; ++i)
                {
                    bool const is_possibly_this_arg = i == 0;
                    if (is_possibly_this_arg && is_dot_member_func_call)
                    {
                        C99_SRC_APPEND(ctx, "&(");
                    }
                    c99_codegen_expr(ctx, &expr->postfix.args[i]);
                    if (is_possibly_this_arg && is_dot_member_func_call)
                    {
                        C99_SRC_APPEND(ctx, ")");
                    }
                    if (i != args_len-1)
                    {
                        C99_SRC_APPEND(ctx, ", ");
                    }
                }
                C99_SRC_APPEND(ctx, ")");
            }
            break;

        case EXPR_POSTFIX_SUBSCRIPT:
            {
                c99_codegen_expr(ctx, expr->postfix.lhs);
                nassert(
                    dt_is_pointer(&expr->postfix.lhs->dt)
                    || dt_is_array(&expr->postfix.lhs->dt)
                );

                C99_SRC_APPEND(ctx, "[");
                c99_codegen_expr(ctx, expr->postfix.subscript);
                C99_SRC_APPEND(ctx, "]");
            }
            break;

        case EXPR_POSTFIX_ACCESS_DOT:
            {
                c99_codegen_expr(ctx, expr->postfix.lhs);
                nassert(dt_is_struct(&expr->postfix.lhs->dt));

                C99_SRC_APPEND(ctx, ".");
                c99_codegen_id(ctx, &expr->postfix.access.id);
            }
            break;

        case EXPR_POSTFIX_ACCESS_ARROW:
            {
                c99_codegen_expr(ctx, expr->postfix.lhs);
                nassert(1 == dt_length_modifiers(&expr->postfix.lhs->dt));
                nassert(DT_STRUCT == expr->postfix.lhs->dt.inner->tag);
                C99_SRC_APPEND(ctx, "->");
                c99_codegen_id(ctx, &expr->postfix.access.id);
            }
            break;

        default:
            NPANIC_DFLT_CASE();
        }
    }
    else if (expr_is_unary(expr))
    {
        C99_SRC_APPEND(ctx, "(");
        switch (expr->tag)
        {
        case EXPR_UNARY_ADDR_OF:
            C99_SRC_APPEND(ctx, "&");
            c99_codegen_expr(ctx, expr->unary.rhs_expr);
            break;

        case EXPR_UNARY_DEREF:
            C99_SRC_APPEND(ctx, "*");
            c99_codegen_expr(ctx, expr->unary.rhs_expr);
            break;

        case EXPR_UNARY_DECAY:
            C99_SRC_APPEND(ctx, "/*$*/");
            c99_codegen_expr(ctx, expr->unary.rhs_expr);
            break;

        case EXPR_UNARY_PLUS:
            C99_SRC_APPEND(ctx, "+");
            c99_codegen_expr(ctx, expr->unary.rhs_expr);
            break;

        case EXPR_UNARY_MINUS:
            C99_SRC_APPEND(ctx, "-");
            c99_codegen_expr(ctx, expr->unary.rhs_expr);
            break;

        case EXPR_UNARY_BITWISE_NOT:
            C99_SRC_APPEND(ctx, "~");
            c99_codegen_expr(ctx, expr->unary.rhs_expr);
            break;

        case EXPR_UNARY_LOGICAL_NOT:
            C99_SRC_APPEND(ctx, "!");
            c99_codegen_expr(ctx, expr->unary.rhs_expr);
            break ;

        case EXPR_UNARY_COUNTOF_EXPR:
            {
                nassert(dt_is_array(&expr->unary.rhs_expr->dt));
                nassert(dt_is_array(&expr->unary.rhs_expr->dt));
                size_t const len = expr->unary.rhs_expr->dt.array.length;
                C99_SRC_APPEND(ctx, "/*countof*/%zu", len);
            }
            break;

        case EXPR_UNARY_COUNTOF_DT:
            {
                nassert(dt_is_array(expr->unary.rhs_dt));
                nassert(dt_is_array(expr->unary.rhs_dt));
                size_t const len = expr->unary.rhs_dt->array.length;
                C99_SRC_APPEND(ctx, "/*countof*/%zu", len);
            }
            break;

        case EXPR_UNARY_SIZEOF_EXPR:
            if (DT_ARRAY == expr->unary.rhs_expr->dt.tag)
            {
                C99_SRC_APPEND(
                    ctx,
                    "/*sizeof*/%zu",
                    expr->unary.rhs_expr->dt.array.length
                );
            }
            else
            {
                C99_SRC_APPEND(ctx, "sizeof(");
                c99_codegen_expr(ctx, expr->unary.rhs_expr);
                C99_SRC_APPEND(ctx, ")");
            }
            break;

        case EXPR_UNARY_SIZEOF_DT:
            if (DT_ARRAY == expr->unary.rhs_dt->tag)
            {
                C99_SRC_APPEND(ctx, "/*sizeof*/%zu", expr->unary.rhs_dt->array.length);
            }
            else
            {
                C99_SRC_APPEND(ctx, "sizeof(");
                c99_codegen_dt(ctx, expr->unary.rhs_dt, NULL);
                C99_SRC_APPEND(ctx, ")");
            }
            break;

        default:
            NPANIC_DFLT_CASE();
        }
        C99_SRC_APPEND(ctx, ")");
    }
    else if (expr_is_binary(expr))
    {
        C99_SRC_APPEND(ctx, "(");
        if (EXPR_BINARY_CAST == expr->tag)
        {
            nstr_t nstr_defer_fini dt_nstr =
                dt_to_c_source_nstr(expr->binary.rhs_dt, NULL);
            C99_SRC_APPEND(ctx, "(%s)", dt_nstr.data);
            c99_codegen_expr(ctx, expr->binary.lhs_expr);
        }
        else
        {
            c99_codegen_expr(ctx, expr->binary.lhs_expr);
            switch (expr->tag)
            {
            case EXPR_BINARY_ASSIGN:      C99_SRC_APPEND(ctx, " = ");   break;
            case EXPR_BINARY_LOGICAL_OR:  C99_SRC_APPEND(ctx, " || ");  break;
            case EXPR_BINARY_LOGICAL_AND: C99_SRC_APPEND(ctx, " && ");  break;
            case EXPR_BINARY_BITWISE_OR:  C99_SRC_APPEND(ctx, " | ");   break;
            case EXPR_BINARY_BITWISE_XOR: C99_SRC_APPEND(ctx, " ^ ");   break;
            case EXPR_BINARY_BITWISE_AND: C99_SRC_APPEND(ctx, " & ");   break;
            case EXPR_BINARY_REL_EQ:      C99_SRC_APPEND(ctx, " == ");  break;
            case EXPR_BINARY_REL_NE:      C99_SRC_APPEND(ctx, " != ");  break;
            case EXPR_BINARY_REL_LT:      C99_SRC_APPEND(ctx, " < ");   break;
            case EXPR_BINARY_REL_GT:      C99_SRC_APPEND(ctx, " > ");   break;
            case EXPR_BINARY_REL_LE:      C99_SRC_APPEND(ctx, " <= ");  break;
            case EXPR_BINARY_REL_GE:      C99_SRC_APPEND(ctx, " >= ");  break;
            case EXPR_BINARY_PLUS:        C99_SRC_APPEND(ctx, " + ");   break;
            case EXPR_BINARY_MINUS:       C99_SRC_APPEND(ctx, " - ");   break;
            case EXPR_BINARY_PTR_DIFF:    C99_SRC_APPEND(ctx, " - ");   break;
            case EXPR_BINARY_PTR_PLUS:    C99_SRC_APPEND(ctx, " + ");   break;
            case EXPR_BINARY_PTR_MINUS:   C99_SRC_APPEND(ctx, " - ");   break;
            case EXPR_BINARY_MULT:        C99_SRC_APPEND(ctx, " * ");   break;
            case EXPR_BINARY_DIV:         C99_SRC_APPEND(ctx, " / ");   break;
            default:
                NPANIC_DFLT_CASE();
            }
            c99_codegen_expr(ctx, expr->binary.rhs_expr);
        }
        C99_SRC_APPEND(ctx, ")");
    }
    else
    {
        NPANIC_UNEXPECTED_CTRL_FLOW();
    }
}

void c99_codegen_id(
    struct c99_codegen_context* ctx,
    struct id* id
)
{
    nstr_cat_fmt(&ctx->c_code_nstr, "%.*s", (int)id->length, id->start);
}

void c99_codegen_dt(
    struct c99_codegen_context* ctx,
    struct dt* dt,
    struct id* id
)
{
    nstr_t nstr_defer_fini dt_nstr = dt_to_c_source_nstr(dt, id);
    nstr_cat_fmt(&ctx->c_code_nstr, "%s", dt_nstr.data);
}

void c99_codegen_struct_def(
    struct c99_codegen_context* ctx,
    struct struct_def* def
)
{
    C99_SRC_APPEND(ctx, "{\n");
    ctx->indent_level += 1;

    size_t const member_vars_len = narr_length(def->member_vars);
    for (size_t i = 0; i < member_vars_len; ++i)
    {
        struct member_var* memb = &def->member_vars[i];
        c99_codegen_do_indent(ctx);
        c99_codegen_dt(ctx, &memb->dt, &memb->id);
        C99_SRC_APPEND(ctx, ";\n");
    }

    ctx->indent_level -= 1;
    C99_SRC_APPEND(ctx, "};\n");
}

void c99_codegen_literal(
    struct c99_codegen_context* ctx,
    struct literal* literal
)
{
    switch (literal->tag)
    {
    case LITERAL_U8:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_u8)(%" PRIu8 "ULL))",
            literal->u8
        );
        break;
    case LITERAL_U16:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_u16)(%" PRIu16 "ULL))",
            literal->u16
        );
        break;
    case LITERAL_U32:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_u32)(%" PRIu32 "ULL))",
            literal->u32
        );
        break;
    case LITERAL_U64:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_u64)(%" PRIu64 "ULL))",
            literal->u64
        );
        break;
    case LITERAL_U:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_u)(%" PRIuMAX "ULL))",
            literal->u
        );
        break;
    case LITERAL_S8:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_s8)(%" PRIi8 "ULL))",
            literal->s8
        );
        break;
    case LITERAL_S16:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_s16)(%" PRIi16 "ULL))",
            literal->s16
        );
        break;
    case LITERAL_S32:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_s32)(%" PRIi32 "ULL))",
            literal->s32
        );
        break;
    case LITERAL_S64:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_s64)(%" PRIi64 "ULL))",
            literal->s64
        );
        break;
    case LITERAL_S:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "(/*literal*/(n_s)(%" PRIdMAX "ULL))",
            literal->s
        );
        break;
    case LITERAL_F32:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "%.*s",
            literal->f32.len - N_FLOAT_F32_STR_LEN,
            literal->f32.start
        );
        break;
    case LITERAL_F64:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "%.*s",
            literal->f64.len - N_FLOAT_F32_STR_LEN,
            literal->f64.start
        );
        break;
    case LITERAL_BOOL:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "%s",
            literal->boolean ? "n_true" : "n_false"
        );
        break;
    case LITERAL_CHAR:
        nstr_cat_fmt(
            &ctx->c_code_nstr,
            "\'%s\'",
            n_ascii_to_c_source_char(literal->character)
        );
        break;
    case LITERAL_STR:
        nstr_cat_fmt(&ctx->c_code_nstr, "\"");
        for (size_t i = 0; i < literal->str.len; ++i)
        {
            nstr_cat_fmt(
                &ctx->c_code_nstr,
                "%s",
                n_ascii_to_c_source_char((n_ascii)literal->str.data[i])
            );
        }
        nstr_cat_fmt(&ctx->c_code_nstr, "\"");
        break;
    case LITERAL_NULL:
        nstr_cat_fmt(&ctx->c_code_nstr, "n_null");
        break;
    case LITERAL_ARRAY:
        {
            nstr_cat_fmt(&ctx->c_code_nstr, "{");

            struct dt ATTR_DEFER_FINI(dt_fini) dt = literal_get_dt(literal);
            nassert(dt_is_array(&dt));
            size_t const num_elems = dt.array.length;

            struct dt const* const elem_dt = dt.inner;
            bool const elements_are_integer = dt_is_integer(elem_dt);
            bool const elements_are_char = dt_is_char(elem_dt);

            bool is_fill = false;
            size_t last_defined_idx = (size_t)-1;
            size_t i = 0;
            while (i < num_elems)
            {
                size_t idx;
                if (is_fill)
                {
                    idx = last_defined_idx;
                }
                else if (LITERAL_ARRAY_FILL == literal->array[i].tag)
                {
                    // By the C standard, a last value of zero in an array
                    // declaration that is not fully initialized will fill the
                    // rest of the array with zeros.
                    // We take advantage of that here as a way to save space in
                    // time in outputted C99 files.
                    struct literal const* const last =
                        &(literal->array[last_defined_idx]);
                    bool const last_is_integer_zero =
                            elements_are_integer && (0 == last->u8);
                    bool const last_is_char_zero =
                            elements_are_char && (0 == last->character);
                    if (last_is_integer_zero || last_is_char_zero)
                    {
                        C99_SRC_APPEND(ctx, "0/* ZERO FILL */");
                        break;
                    }
                    is_fill = true;
                    continue;
                }
                else
                {
                    last_defined_idx = i;
                    idx = i;
                }

                c99_codegen_literal(ctx, &literal->array[idx]);
                if (!is_fill || i != num_elems-1)
                {
                    nstr_cat_fmt(&ctx->c_code_nstr, ", ");
                }
                i += 1;
            }

            nstr_cat_fmt(&ctx->c_code_nstr, "}");
        }
        break;
    case LITERAL_ARRAY_FILL:
        NPANIC_UNEXPECTED_CTRL_FLOW();
    default:
        NPANIC_DFLT_CASE();
    }
}
