/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nc/nir.h>
#include <stdlib.h>

//============================================================================//
//      ASCII CHARACTER                                                       //
//============================================================================//
bool n_ascii_is_valid(n_ascii ch)
{
    DIAGNOSTIC_IGNORE("-Wtype-limits");
    return (ch >= N_ASCII_MIN) && (ch <= N_ASCII_MAX);
}

bool n_ascii_is_printable(n_ascii ch)
{
    return (ch >= N_ASCII_PRINTABLE_MIN) && (ch <= N_ASCII_PRINTABLE_MAX);
}

char const* n_ascii_to_n_source_char(n_ascii ch)
{
    nassert(n_ascii_is_valid(ch));
    switch (ch)
    {
    case ' ':  return " ";
    case '!':  return "!";
    case '\"': return "\\\"";
    case '#':  return "#";
    case '$':  return "$";
    case '%':  return "%";
    case '&':  return "&";
    case '\'': return "\\\'";
    case '(':  return "(";
    case ')':  return ")";
    case '*':  return "*";
    case '+':  return "+";
    case ',':  return ",";
    case '-':  return "-";
    case '.':  return ".";
    case '/':  return "/";
    case '0':  return "0";
    case '1':  return "1";
    case '2':  return "2";
    case '3':  return "3";
    case '4':  return "4";
    case '5':  return "5";
    case '6':  return "6";
    case '7':  return "7";
    case '8':  return "8";
    case '9':  return "9";
    case ':':  return ":";
    case ';':  return ";";
    case '<':  return "<";
    case '=':  return "=";
    case '>':  return ">";
    case '\?': return "\?";
    case '@':  return "@";
    case 'A':  return "A";
    case 'B':  return "B";
    case 'C':  return "C";
    case 'D':  return "D";
    case 'E':  return "E";
    case 'F':  return "F";
    case 'G':  return "G";
    case 'H':  return "H";
    case 'I':  return "I";
    case 'J':  return "J";
    case 'K':  return "K";
    case 'L':  return "L";
    case 'M':  return "M";
    case 'N':  return "N";
    case 'O':  return "O";
    case 'P':  return "P";
    case 'Q':  return "Q";
    case 'R':  return "R";
    case 'S':  return "S";
    case 'T':  return "T";
    case 'U':  return "U";
    case 'V':  return "V";
    case 'W':  return "W";
    case 'X':  return "X";
    case 'Y':  return "Y";
    case 'Z':  return "Z";
    case '[':  return "[";
    case '\\': return "\\\\";
    case ']':  return "]";
    case '^':  return "^";
    case '_':  return "_";
    case '`':  return "`";
    case 'a':  return "a";
    case 'b':  return "b";
    case 'c':  return "c";
    case 'd':  return "d";
    case 'e':  return "e";
    case 'f':  return "f";
    case 'g':  return "g";
    case 'h':  return "h";
    case 'i':  return "i";
    case 'j':  return "j";
    case 'k':  return "k";
    case 'l':  return "l";
    case 'm':  return "m";
    case 'n':  return "n";
    case 'o':  return "o";
    case 'p':  return "p";
    case 'q':  return "q";
    case 'r':  return "r";
    case 's':  return "s";
    case 't':  return "t";
    case 'u':  return "u";
    case 'v':  return "v";
    case 'w':  return "w";
    case 'x':  return "x";
    case 'y':  return "y";
    case 'z':  return "z";
    case '{':  return "{";
    case '|':  return "|";
    case '}':  return "}";
    case '~':  return "~";
    case '\0': return "\\0";
    case '\a': return "\\a";
    case '\b': return "\\b";
    case N_ASCII_ESC: return "\\x1B";
    case '\f': return "\\f";
    case '\n': return "\\n";
    case '\r': return "\\r";
    case '\t': return "\\t";
    case '\v': return "\\v";
    default:
        npanic("unknown character");
    }
}

char const* n_ascii_to_c_source_char(n_ascii ch)
{
    nassert(n_ascii_is_valid(ch));
    // The only difference between N characters and C characters is that the
    // question mark chatacter '?' in N must be escaped in C, '\?'. Check for
    // this special case; if the character is a question mark then print its
    // escaped C version, otherwise print the equivalent N version.
    if (ch == '\?')
    {
        return "\\\?";
    }
    return n_ascii_to_n_source_char(ch);
}

ssize_t match_n_ascii(char const* start)
{
    if ('\\' == start[0]) // Escaped character.
    {
        switch (start[1])
        {
        case '0':  /* intentional fallthrough */
        case 'a':  /* intentional fallthrough */
        case 'b':  /* intentional fallthrough */
        case 'e':  /* intentional fallthrough */
        case 'f':  /* intentional fallthrough */
        case 'n':  /* intentional fallthrough */
        case 'r':  /* intentional fallthrough */
        case 't':  /* intentional fallthrough */
        case 'v':  /* intentional fallthrough */
        case '\"': /* intentional fallthrough */
        case '\'': /* intentional fallthrough */
        case '\\':
            return N_ASCII_LEN_ESCAPED;
        default:
            return MATCH_N_ASCII_FAILURE;
        }
    }

    // Unescaped character.
    if (isprint(start[0]) && ('\'' != start[0]) && ('\"' != start[0]))
    {
        return N_ASCII_LEN_UNESCAPED;
    }

    return MATCH_N_ASCII_FAILURE;
}

ssize_t parse_n_ascii(char const* start, n_ascii* nch)
{
    ssize_t const match_result = match_n_ascii(start);

    // Unescaped character.
    if (N_ASCII_LEN_UNESCAPED == match_result)
    {
        *nch = (n_ascii)start[0];
        return N_ASCII_LEN_UNESCAPED;
    }

    // Escaped character.
    if (N_ASCII_LEN_ESCAPED == match_result)
    {
        nassert('\\' == start[0]);
        switch (start[1])
        {
        case '0':  *nch = (n_ascii)'\0'; break;
        case 'a':  *nch = (n_ascii)'\a'; break;
        case 'b':  *nch = (n_ascii)'\b'; break;
        case 'e':  *nch = (n_ascii)'\x1B'; break;
        case 'f':  *nch = (n_ascii)'\f'; break;
        case 'n':  *nch = (n_ascii)'\n'; break;
        case 'r':  *nch = (n_ascii)'\r'; break;
        case 't':  *nch = (n_ascii)'\t'; break;
        case 'v':  *nch = (n_ascii)'\v'; break;
        case '\"': *nch = (n_ascii)'\"'; break;
        case '\'': *nch = (n_ascii)'\''; break;
        case '\\': *nch = (n_ascii)'\\'; break;
        default:
            return PARSE_N_ASCII_FAILURE;
        }
        return N_ASCII_LEN_ESCAPED;
    }

    // Error.
    return PARSE_N_ASCII_FAILURE;
}

ssize_t match_n_ascii_literal(char const* start)
{
    if ('\'' != start[0])
    {
        return MATCH_N_ASCII_LITERAL_MISSING_OPEN_QUOTE;
    }

    ssize_t const match_result = match_n_ascii(start + SINGLE_QUOTE_LEN);
    if (match_result <= 0)
    {
        return MATCH_N_ASCII_LITERAL_BAD_CHAR;
    }

    if ('\'' != start[SINGLE_QUOTE_LEN + match_result])
    {
        return MATCH_N_ASCII_LITERAL_MISSING_CLOSING_QUOTE;
    }
 
    if('a' != start[SINGLE_QUOTE_LEN + SINGLE_QUOTE_TYPE_LEN + match_result])
    {
        return MATCH_N_ASCII_LITERAL_MISSING_TYPE;
    }

    return SINGLE_QUOTE_LEN*2 + SINGLE_QUOTE_TYPE_LEN + match_result;
}

ssize_t parse_n_ascii_literal(char const* start, n_ascii* nch)
{
    ssize_t const match_result = match_n_ascii_literal(start);

    if (match_result < 0)
    {
        return match_result;
    }

    ssize_t parse_result = parse_n_ascii(start + SINGLE_QUOTE_LEN, nch);
    nassert(parse_result >= 0); SUPPRESS_UNUSED(parse_result);

    return match_result;
}

//============================================================================//
//      ASCII STRING                                                          //
//============================================================================//
ssize_t match_n_astr_literal(char const* start)
{
    char const* curr = start;

    if ('\"' != *curr)
    {
        return MATCH_N_ASTR_LITERAL_MISSING_OPEN_QUOTE;
    }
    curr += 1;

    ssize_t match_result;
    while ((match_result = match_n_ascii(curr)) >= 0)
    {
        curr += match_result;
    }

    // Check to see if the match failure was due to a legitimate bad char or
    // just end double quote being reached.
    if ('\"' != *curr)
    {
        return MATCH_N_ASTR_LITERAL_MISSING_CLOSING_QUOTE;
    }
    curr += 1;

    //make sure it has the type.
    if ('a' != *curr)
    {
        return MATCH_N_ASTR_LITERAL_MISSING_TYPE;
    }
    curr += 1;

    // End of the string literal has been reached.
    return (ssize_t)(curr - start);
}

ssize_t parse_n_astr_literal(char const* start, nstr_t* nstr)
{
    ssize_t const match_result = match_n_astr_literal(start);
    if (match_result < 0) // Match failure.
    {
        return match_result;
    }

    char const* curr = start + DOUBLE_QUOTE_LEN;
    char const* end_quote = start + match_result - 
            (DOUBLE_QUOTE_LEN + DOUBLE_QUOTE_TYPE_LEN);
    nassert('\"' == *end_quote);

    nstr_clear(nstr);
    while (curr < end_quote)
    {
        n_ascii nch = 0;
        curr += parse_n_ascii(curr, &nch);
        nstr_cat_fmt(nstr, "%c", nch);
    }

    return match_result;
}

//============================================================================//
//      NUMERIC                                                               //
//============================================================================//
bool is_digit_dec(char ch)
{
    return (ch >= '0' && ch <= '9');
}

bool is_digit_hex(char ch)
{
    return (ch >= '0' && ch <= '9')
        || (ch >= 'a' && ch <= 'f')
        || (ch >= 'A' && ch <= 'F');
}

bool is_digit_bin(char ch)
{
    return ch == '0'
        || ch == '1';
}

int digit_to_int(char digit)
{
    if      (digit >= '0' && digit <= '9')
    {
        return digit - '0';
    }
    else if (digit >= 'a' && digit <= 'f')
    {
        return digit - 'a' + 10;
    }
    else if (digit >= 'A' && digit <= 'F')
    {
        return digit - 'A' + 10;
    }

    return -1;
}

//====================================//
//      INTEGER                       //
//====================================//
size_t n_integer_suffix_len(enum n_integer_type inttype)
{
    switch (inttype)
    {
    case N_INTEGER_U8:  return N_INTEGER_U8_STR_LEN;
    case N_INTEGER_U16: return N_INTEGER_U16_STR_LEN;
    case N_INTEGER_U32: return N_INTEGER_U32_STR_LEN;
    case N_INTEGER_U64: return N_INTEGER_U64_STR_LEN;
    case N_INTEGER_U:   return N_INTEGER_U_STR_LEN;
    case N_INTEGER_S8:  return N_INTEGER_S8_STR_LEN;
    case N_INTEGER_S16: return N_INTEGER_S16_STR_LEN;
    case N_INTEGER_S32: return N_INTEGER_S32_STR_LEN;
    case N_INTEGER_S64: return N_INTEGER_S64_STR_LEN;
    case N_INTEGER_S:   return N_INTEGER_S_STR_LEN;
    default:
        NPANIC_DFLT_CASE();
    }
}

ssize_t match_n_integer_literal(
    char const* start,
    enum n_integer_type* inttype,
    enum n_integer_fmt* intfmt
)
{
    char const* curr = start;

    // Consume negative character.
    IS_DIGIT_NEGATIVE_INDICATIVE(curr);

    enum n_integer_fmt n_integer_fmt;
    if (
        cstr_n_cmp(curr, N_INTEGER_HEX_PREFIX_LOWER, N_INTEGER_HEX_PREFIX_LEN) == 0
        ||
        cstr_n_cmp(curr, N_INTEGER_HEX_PREFIX_UPPER, N_INTEGER_HEX_PREFIX_LEN) == 0
    )
    {
        n_integer_fmt = N_INTEGER_FMT_HEX;
        curr += N_INTEGER_HEX_PREFIX_LEN;
    }
    else if (
        cstr_n_cmp(curr, N_INTEGER_BIN_PREFIX_LOWER, N_INTEGER_BIN_PREFIX_LEN) == 0
        ||
        cstr_n_cmp(curr, N_INTEGER_BIN_PREFIX_UPPER, N_INTEGER_BIN_PREFIX_LEN) == 0
    )
    {
        n_integer_fmt = N_INTEGER_FMT_BIN;
        curr += N_INTEGER_BIN_PREFIX_LEN;
    }
    else
    {
        if (!isdigit(*curr))
        {
            return MATCH_N_INTEGER_INVALID_PREFIX;
        }
        n_integer_fmt = N_INTEGER_FMT_DEC;
    }

    // Validate all digits.
    char const* save = curr;
    LOOP_FOREVER
    {
        char const c = *curr;

        if ('u' == c || 's' == c)
        {
            // All digits have been verified.
            break;
        }

        switch (n_integer_fmt)
        {
        case N_INTEGER_FMT_DEC:
            if (!is_digit_dec(c))
            {
                return MATCH_N_INTEGER_INVALID_DIGIT;
            }
            curr += 1;
            break;
        case N_INTEGER_FMT_HEX:
            if (!is_digit_hex(c))
            {
                return MATCH_N_INTEGER_INVALID_DIGIT;
            }
            curr += 1;
            break;
        case N_INTEGER_FMT_BIN:
            if (!is_digit_bin(c))
            {
                return MATCH_N_INTEGER_INVALID_DIGIT;
            }
            curr += 1;
            break;
        default:
            NPANIC_DFLT_CASE();
        }
    }

    // Strings such as "u8", or "0Xu64" don't contain any actual digits and are
    // considered invalid integers.
    if (save == curr)
    {
        return MATCH_N_INTEGER_NO_DIGITS_PARSED;
    }

    enum n_integer_type type;
    // Parse suffix.
    if (cstr_n_cmp(curr, N_INTEGER_U8_STR, N_INTEGER_U8_STR_LEN) == 0)
    {
        type = N_INTEGER_U8;
        curr += N_INTEGER_U8_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_U16_STR, N_INTEGER_U16_STR_LEN) == 0)
    {
        type = N_INTEGER_U16;
        curr += N_INTEGER_U16_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_U32_STR, N_INTEGER_U32_STR_LEN) == 0)
    {
        type = N_INTEGER_U32;
        curr += N_INTEGER_U32_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_U64_STR, N_INTEGER_U64_STR_LEN) == 0)
    {
        type = N_INTEGER_U64;
        curr += N_INTEGER_U64_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_U_STR, N_INTEGER_U_STR_LEN) == 0)
    {
        type = N_INTEGER_U;
        curr += N_INTEGER_U_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_S8_STR, N_INTEGER_S8_STR_LEN) == 0)
    {
        type = N_INTEGER_S8;
        curr += N_INTEGER_S8_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_S16_STR, N_INTEGER_S16_STR_LEN) == 0)
    {
        type = N_INTEGER_S16;
        curr += N_INTEGER_S16_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_S32_STR, N_INTEGER_S32_STR_LEN) == 0)
    {
        type = N_INTEGER_S32;
        curr += N_INTEGER_S32_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_S64_STR, N_INTEGER_S64_STR_LEN) == 0)
    {
        type = N_INTEGER_S64;
        curr += N_INTEGER_S64_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_INTEGER_S_STR, N_INTEGER_S_STR_LEN) == 0)
    {
        type = N_INTEGER_S;
        curr += N_INTEGER_S_STR_LEN;
    }
    else
    {
        return MATCH_N_INTEGER_INVALID_SUFFIX;
    }

    // Check to make sure that the character following the suffix does not
    // invalidate the string.
    // For example the strings "12u8foo", "12u8_bar", and "12u82" are all
    // invalid, but the strings "12u8", "12u8 ", and "12u8+13u8" are all valid.
    if (isalpha(*curr) || isdigit(*curr) || *curr == '_')
    {
        return MATCH_N_INTEGER_INVALID_SUFFIX;
    }

    if (NULL != inttype)
    {
        *inttype = type;
    }
    if (NULL != intfmt)
    {
        *intfmt = n_integer_fmt;
    }
    return (ssize_t)(curr - start);
}

ssize_t parse_n_integer_literal(
    char const* start,
    struct n_integer_value* intval
)
{
    enum n_integer_type inttype;
    enum n_integer_fmt intfmt;
    char const* curr = start;

    ssize_t const match_result =
        match_n_integer_literal(start, &inttype, &intfmt);
    if (match_result < 0) // Match failure.
    {
        return match_result;
    }

    MEMZERO_INSTANCE(intval, struct n_integer_value);
    intval->tag = inttype;

    bool is_negative = IS_DIGIT_NEGATIVE_INDICATIVE(curr);

    if (N_INTEGER_FMT_HEX == intfmt)
    {
        curr += N_INTEGER_HEX_PREFIX_LEN;
    }
    else if (N_INTEGER_FMT_BIN == intfmt)
    {
        curr += N_INTEGER_BIN_PREFIX_LEN;
    }

DIAGNOSTIC_PUSH;
DIAGNOSTIC_IGNORE("-Wconversion");

    char const* digit_end = start + match_result - n_integer_suffix_len(inttype);
    while (curr != digit_end)
    {
        int digit_val = digit_to_int(*curr);
        nassert(digit_val >= 0);

        // Shift the current value one digit.
        switch (intfmt)
        {
        case N_INTEGER_FMT_DEC:
            switch (inttype)
            {
            case N_INTEGER_U8:  intval->u8  *= 10; break;
            case N_INTEGER_U16: intval->u16 *= 10; break;
            case N_INTEGER_U32: intval->u32 *= 10; break;
            case N_INTEGER_U64: intval->u64 *= 10; break;
            case N_INTEGER_U:   intval->u   *= 10; break;
            case N_INTEGER_S8:  intval->s8  *= 10; break;
            case N_INTEGER_S16: intval->s16 *= 10; break;
            case N_INTEGER_S32: intval->s32 *= 10; break;
            case N_INTEGER_S64: intval->s64 *= 10; break;
            case N_INTEGER_S:   intval->s   *= 10; break;
            }
            break;
        case N_INTEGER_FMT_HEX:
            switch (inttype)
            {
            case N_INTEGER_U8:  intval->u8  *= 16; break;
            case N_INTEGER_U16: intval->u16 *= 16; break;
            case N_INTEGER_U32: intval->u32 *= 16; break;
            case N_INTEGER_U64: intval->u64 *= 16; break;
            case N_INTEGER_U:   intval->u   *= 16; break;
            case N_INTEGER_S8:  intval->s8  *= 16; break;
            case N_INTEGER_S16: intval->s16 *= 16; break;
            case N_INTEGER_S32: intval->s32 *= 16; break;
            case N_INTEGER_S64: intval->s64 *= 16; break;
            case N_INTEGER_S:   intval->s   *= 16; break;
            }
            break;
        case N_INTEGER_FMT_BIN:
            switch (inttype)
            {
            case N_INTEGER_U8:  intval->u8  *= 2; break;
            case N_INTEGER_U16: intval->u16 *= 2; break;
            case N_INTEGER_U32: intval->u32 *= 2; break;
            case N_INTEGER_U64: intval->u64 *= 2; break;
            case N_INTEGER_U:   intval->u   *= 2; break;
            case N_INTEGER_S8:  intval->s8  *= 2; break;
            case N_INTEGER_S16: intval->s16 *= 2; break;
            case N_INTEGER_S32: intval->s32 *= 2; break;
            case N_INTEGER_S64: intval->s64 *= 2; break;
            case N_INTEGER_S:   intval->s   *= 2; break;
            }
            break;
        default:
            NPANIC_DFLT_CASE();
        }

        // Apply current digit.
        if (is_negative)
        {
            switch (inttype)
            {
            case N_INTEGER_U8:  intval->u8  -= (n_u8)digit_val;  break;
            case N_INTEGER_U16: intval->u16 -= (n_u16)digit_val; break;
            case N_INTEGER_U32: intval->u32 -= (n_u32)digit_val; break;
            case N_INTEGER_U64: intval->u64 -= (n_u64)digit_val; break;
            case N_INTEGER_U:   intval->u   -= (n_u)digit_val;   break;
            case N_INTEGER_S8:  intval->s8  -= (n_s8)digit_val;  break;
            case N_INTEGER_S16: intval->s16 -= (n_s16)digit_val; break;
            case N_INTEGER_S32: intval->s32 -= (n_s32)digit_val; break;
            case N_INTEGER_S64: intval->s64 -= (n_s64)digit_val; break;
            case N_INTEGER_S:   intval->s   -= (n_s)digit_val;   break;
            }
        }
        else
        {
            switch (inttype)
            {
            case N_INTEGER_U8:  intval->u8  += (n_u8)digit_val;  break;
            case N_INTEGER_U16: intval->u16 += (n_u16)digit_val; break;
            case N_INTEGER_U32: intval->u32 += (n_u32)digit_val; break;
            case N_INTEGER_U64: intval->u64 += (n_u64)digit_val; break;
            case N_INTEGER_U:   intval->u   += (n_u)digit_val;   break;
            case N_INTEGER_S8:  intval->s8  += (n_s8)digit_val;  break;
            case N_INTEGER_S16: intval->s16 += (n_s16)digit_val; break;
            case N_INTEGER_S32: intval->s32 += (n_s32)digit_val; break;
            case N_INTEGER_S64: intval->s64 += (n_s64)digit_val; break;
            case N_INTEGER_S:   intval->s   += (n_s)digit_val;   break;
            }
        }
        curr += 1;
    }

DIAGNOSTIC_POP;
    return match_result;
}

//====================================//
//      FLOATING POINT                //
//====================================//
ssize_t match_n_float_literal(
    char const* start,
    enum n_float_type* floattype,
    enum n_float_fmt* floatfmt
)
{
    char const* curr = start;

    // Consume plus or minus character.
    IS_DIGIT_NEGATIVE_INDICATIVE(curr);

    // Parse non-scientific notation characters.
    char const* const digits_start = curr;
    char const* decimal_point = NULL;
    while (
        is_digit_dec(*curr)
        ||
        (
            (NULL == decimal_point)
            && ('.' == *curr)
            && /*then do*/(decimal_point = curr)
        )
    )
    {
        curr += 1;
    }
    if (('e' != *curr) && ('E' != *curr) && ('f' != *curr))
    {
        return MATCH_N_FLOAT_INVALID_DIGIT;
    }
    if (NULL == decimal_point)
    {
        return MATCH_N_FLOAT_MISSING_DECIMAL_POINT;
    }
    if (digits_start == decimal_point)
    {
        return MATCH_N_FLOAT_NO_DIGITS_BEFORE_DECIMAL_POINT;
    }
    if (curr == decimal_point+1)
    {
        return MATCH_N_FLOAT_NO_DIGITS_AFTER_DECIMAL_POINT;
    }

    // Check for scientific notation.
    enum n_float_fmt n_float_fmt;
    if ('e' == *curr || 'E' == *curr)
    {
        n_float_fmt = N_FLOAT_FMT_SCIENTIFIC;
        curr += 1;

        // Consume plus or minus character.
        IS_DIGIT_NEGATIVE_INDICATIVE(curr);

        char const* const sci_digits_start = curr;
        // Parse scientific notation digits.
        while (is_digit_dec(*curr))
        {
            curr += 1;
        }
        if (sci_digits_start == curr)
        {
            return MATCH_N_FLOAT_INVALID_SCIENTIFIC_NOTATION;
        }
    }
    else
    {
        n_float_fmt = N_FLOAT_FMT_DECIMAL;
    }

    // Parse suffix.
    enum n_float_type n_float_type;
    if (cstr_n_cmp(curr, N_FLOAT_F32_STR, N_FLOAT_F32_STR_LEN) == 0)
    {
        n_float_type = N_FLOAT_F32;
        curr += N_FLOAT_F32_STR_LEN;
    }
    else if (cstr_n_cmp(curr, N_FLOAT_F64_STR, N_FLOAT_F64_STR_LEN) == 0)
    {
        n_float_type = N_FLOAT_F64;
        curr += N_FLOAT_F64_STR_LEN;
    }
    else
    {
        return MATCH_N_FLOAT_INVALID_SUFFIX;
    }

    // Check to make sure that the character following the suffix does not
    // invalidate the string.
    // For example the strings "12u8foo", "12u8_bar", and "12u82" are all
    // invalid, but the strings "12u8", "12u8 ", and "12u8+13u8" are all valid.
    if (isalpha(*curr) || isdigit(*curr) || *curr == '_')
    {
        return MATCH_N_FLOAT_INVALID_SUFFIX;
    }

    if (NULL != floattype)
    {
        *floattype = n_float_type;
    }
    if (NULL != floatfmt)
    {
        *floatfmt = n_float_fmt;
    }
    return (ssize_t)(curr - start);
}

//============================================================================//
//      IDENTIFIER                                                            //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(id)

void id_init(struct id* id)
{
    INIT_FINI_COUNTER_INCR_INIT(id);
    MEMZERO_INSTANCE(id, typeof(*id));
}

void id_fini(struct id* id)
{
    INIT_FINI_COUNTER_INCR_FINI(id);
    SUPPRESS_UNUSED(id);
}

void id_assign(
    struct id* dest,
    struct id const* src,
    bool clear_srctok

)
{
    POD_COPY(*dest, *src);
    dest->srctok = clear_srctok ? NULL : src->srctok;
}

bool id_eq(
    struct id const* A,
    struct id const* B
)
{
    return (A->length == B->length) && cstr_n_eq(A->start, B->start, A->length);
}

//============================================================================//
//      QUALIFIERS                                                            //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(qualifiers)

void qualifiers_init(struct qualifiers* q)
{
    INIT_FINI_COUNTER_INCR_INIT(qualifiers);
    MEMZERO_INSTANCE(q, typeof(*q));
}

void qualifiers_fini(struct qualifiers* q)
{
    INIT_FINI_COUNTER_INCR_FINI(qualifiers);
    SUPPRESS_UNUSED(q);
}

void qualifiers_assign(
    struct qualifiers* dest,
    struct qualifiers const* src,
    bool clear_srctok
)
{
    POD_COPY(*dest, *src);
    dest->srctok = clear_srctok ? NULL : src->srctok;
}

bool qualifiers_eq(
    struct qualifiers const* A,
    struct qualifiers const* B
)
{
    return A->is_mut == B->is_mut;
}

void qualifers_union(
    struct qualifiers* dest,
    struct qualifiers const* A,
    struct qualifiers const* B
)
{
    dest->is_mut = A->is_mut || B->is_mut;
}

bool qualifiers_is_default(struct qualifiers const* q)
{
    return !q->is_mut;
}

bool qualifiers_is_subset(
    struct qualifiers const* sub,
    struct qualifiers const* sup
)
{
    if (sub->is_mut && !sup->is_mut){return false;}
    return true;
}

bool qualifiers_is_restricting(
    struct qualifiers const* from,
    struct qualifiers const* to
)
{
    return !(to->is_mut && !from->is_mut);
}

//============================================================================//
//      DATA TYPES                                                            //
//============================================================================//
//====================================//
//      DATA TYPE                     //
//====================================//
INIT_FINI_COUNTER_SOURCE(dt)

void dt_init(struct dt* dt)
{
    INIT_FINI_COUNTER_INCR_INIT(dt);
    MEMZERO_INSTANCE(dt, typeof(*dt));

    qualifiers_init(&dt->qualifiers);
}

void dt_fini(struct dt* dt)
{
    INIT_FINI_COUNTER_INCR_FINI(dt);

    if (NULL != dt->inner)
    {
        dt_fini(dt->inner);
        nfree(dt->inner);
    }

    qualifiers_fini(&dt->qualifiers);

    switch (dt->tag)
    {
    case DT_UNSET: /* intentional fallthrough */
    case DT_VOID:  /* intentional fallthrough */
    case DT_U8:    /* intentional fallthrough */
    case DT_U16:   /* intentional fallthrough */
    case DT_U32:   /* intentional fallthrough */
    case DT_U64:   /* intentional fallthrough */
    case DT_U:     /* intentional fallthrough */
    case DT_S8:    /* intentional fallthrough */
    case DT_S16:   /* intentional fallthrough */
    case DT_S32:   /* intentional fallthrough */
    case DT_S64:   /* intentional fallthrough */
    case DT_S:     /* intentional fallthrough */
    case DT_F16:   /* intentional fallthrough */
    case DT_F32:   /* intentional fallthrough */
    case DT_F64:   /* intentional fallthrough */
    case DT_F128:  /* intentional fallthrough */
    case DT_BOOL:  /* intentional fallthrough */
    case DT_ASCII: /* intentional fallthrough */
    case DT_STRUCT:
        /* nothing */
        break;
    case DT_FUNCTION:
        func_sig_fini(dt->func_sig);
        nfree(dt->func_sig);
        break;
    case DT_POINTER: /* intentional fallthrough */
    case DT_ARRAY:
        /* nothing */
        break;
    case DT_UNRESOLVED_ID:
        id_fini(&dt->unresolved.id);
        break;
    case DT_UNRESOLVED_TYPEOF_EXPR:
        expr_fini(dt->unresolved.typeof_expr);
        nfree(dt->unresolved.typeof_expr);
        break;
    case DT_UNRESOLVED_TYPEOF_DT:
        dt_fini(dt->unresolved.typeof_dt);
        nfree(dt->unresolved.typeof_dt);
        break;
    default:
        NPANIC_DFLT_CASE();
    }
}

void dt_reset(struct dt* dt)
{
    dt_fini(dt);
    dt_init(dt);
}

void dt_assign(
    struct dt* dest,
    struct dt const* src,
    bool clear_srctok
)
{
    dt_reset(dest);

    if (NULL != src->inner)
    {
        dest->inner = nalloc(sizeof(typeof(*dest->inner)));
        dt_init(dest->inner);
        dt_assign(dest->inner, src->inner, clear_srctok);
    }

    qualifiers_assign(&dest->qualifiers, &src->qualifiers, clear_srctok);

    dest->tag = src->tag;
    switch (src->tag)
    {
    case DT_UNSET: /* intentional fallthrough */
    case DT_VOID:  /* intentional fallthrough */
    case DT_U8:    /* intentional fallthrough */
    case DT_U16:   /* intentional fallthrough */
    case DT_U32:   /* intentional fallthrough */
    case DT_U64:   /* intentional fallthrough */
    case DT_U:     /* intentional fallthrough */
    case DT_S8:    /* intentional fallthrough */
    case DT_S16:   /* intentional fallthrough */
    case DT_S32:   /* intentional fallthrough */
    case DT_S64:   /* intentional fallthrough */
    case DT_S:     /* intentional fallthrough */
    case DT_F16:   /* intentional fallthrough */
    case DT_F32:   /* intentional fallthrough */
    case DT_F64:   /* intentional fallthrough */
    case DT_F128:  /* intentional fallthrough */
    case DT_BOOL:  /* intentional fallthrough */
    case DT_ASCII:
        /* nothing */
        break;
    case DT_STRUCT:
        dest->struct_ref = src->struct_ref;
        break;
    case DT_FUNCTION:
        dest->func_sig = nalloc(sizeof(typeof(*dest->func_sig)));
        func_sig_init(dest->func_sig);
        func_sig_assign(dest->func_sig, src->func_sig, clear_srctok);
        break;
    case DT_POINTER:
        /* nothing */
        break;
    case DT_ARRAY:
        dest->array.length = src->array.length;
        break;
    case DT_UNRESOLVED_ID:
        id_init(&dest->unresolved.id);
        id_assign(&dest->unresolved.id, &src->unresolved.id, clear_srctok);
        break;
    case DT_UNRESOLVED_TYPEOF_EXPR:
        dest->unresolved.typeof_expr = nalloc(sizeof(typeof(*dest->unresolved.typeof_expr)));
        expr_init(dest->unresolved.typeof_expr);
        expr_assign(dest->unresolved.typeof_expr, src->unresolved.typeof_expr, clear_srctok);
        break;
    case DT_UNRESOLVED_TYPEOF_DT:
        dest->unresolved.typeof_dt = nalloc(sizeof(typeof(*dest->unresolved.typeof_dt)));
        dt_init(dest->unresolved.typeof_dt);
        dt_assign(dest->unresolved.typeof_dt, src->unresolved.typeof_dt, clear_srctok);
        break;
    default:
        NPANIC_DFLT_CASE();
    }

    dest->srctok = clear_srctok ? NULL : src->srctok;
}

size_t dt_length_all(struct dt const* dt)
{
    if (NULL == dt->inner){return 1;}
    return dt_length_all(dt->inner) + 1;
}

size_t dt_length_modifiers(struct dt const* dt)
{
    return dt_length_all(dt) - 1;
}

void dt_push_modifier(struct dt* dt, struct dt* modifier)
{
    modifier->inner = modifier;
    memswap(dt, modifier, sizeof(typeof(*dt)));
}

void dt_pop_modifier(struct dt* dt)
{
    nassert(dt_length_all(dt) != 1);
    struct dt* const inner = dt->inner;
    dt->inner = NULL;
    dt_fini(dt);
    *dt = *inner;
    nfree(inner);
}

bool dt_is_void(struct dt const* dt)
{
    return dt_is_base(dt)
        && (DT_VOID == dt->tag);
}

bool dt_is_integer(struct dt const* dt)
{
    return dt_is_base(dt)
        && (dt->tag >= DT_U8 && dt->tag <= DT_S);
}

bool dt_is_integer_unsigned(struct dt const* dt)
{
    return dt_is_base(dt)
        && (dt->tag >= DT_U8 && dt->tag <= DT_U);
}

bool dt_is_integer_signed(struct dt const* dt)
{
    return dt_is_base(dt)
        && (dt->tag >= DT_S8 && dt->tag <= DT_S);
}

bool dt_is_float(struct dt const* dt)
{
    return dt_is_base(dt)
        && (dt->tag >= DT_F16 && dt->tag <= DT_F128);
}

bool dt_is_bool(struct dt const* dt)
{
    return dt_is_base(dt)
        && (DT_BOOL == dt->tag);
}

bool dt_is_char(struct dt const* dt)
{
    return dt_is_base(dt)
        && (DT_ASCII == dt->tag);
}

bool dt_is_struct(struct dt const* dt)
{
    return dt_is_base(dt)
        && (DT_STRUCT == dt->tag);
}

bool dt_is_function(struct dt const* dt)
{
    return dt_is_base(dt)
        && (DT_FUNCTION == dt->tag);
}

bool dt_is_pointer(struct dt const* dt)
{
    return dt_is_modifier(dt)
        && (DT_POINTER == dt->tag);
}

bool dt_is_array(struct dt const* dt)
{
    return dt_is_modifier(dt)
        && (DT_ARRAY == dt->tag);
}

bool dt_is_base(struct dt const* dt)
{
    return NULL == dt->inner;
}

bool dt_is_modifier(struct dt const* dt)
{
    return NULL != dt->inner;
}

bool dt_is_numeric(struct dt const* dt)
{
    return dt_is_base(dt)
        && (dt->tag >= DT_U8 && dt->tag <= DT_F128);
}

bool dt_is_primitive(struct dt const* dt)
{
    return dt_is_base(dt)
        && (dt->tag >= DT_VOID && dt->tag <= DT_ASCII);
}

bool dt_is_composite(struct dt const* dt)
{
    return dt_is_base(dt)
        && (DT_STRUCT == dt->tag);
}

bool dt_is_void_pointer(struct dt const* dt)
{
    return (dt_length_modifiers(dt) == 1)
        && (DT_POINTER == dt->tag)
        && (DT_VOID == dt->inner->tag);
}

nstr_t dt_to_n_source_nstr(struct dt const* dt)
{
    nstr_t nstr;
    if (NULL == dt->inner){nstr_init(&nstr);}
    else{nstr = dt_to_n_source_nstr(dt->inner);}

    //// Tag-based behavior
    switch (dt->tag)
    {
    case DT_VOID:  nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_VOID)); break;
    case DT_U8:    nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U8));   break;
    case DT_U16:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U16));  break;
    case DT_U32:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U32));  break;
    case DT_U64:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U64));  break;
    case DT_U:     nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_U));    break;
    case DT_S8:    nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S8));   break;
    case DT_S16:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S16));  break;
    case DT_S32:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S32));  break;
    case DT_S64:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S64));  break;
    case DT_S:     nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_S));    break;
    case DT_F16:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_F16));  break;
    case DT_F32:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_F32));  break;
    case DT_F64:   nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_F64));  break;
    case DT_F128:  nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_F128)); break;
    case DT_BOOL:  nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_BOOL)); break;
    case DT_ASCII: nstr_assign_cstr(&nstr, token_type_to_cstr(TOKEN_KEYWORD_ASCII)); break;
    case DT_STRUCT:
        nstr_assign_fmt(
            &nstr,
            "%.*s",
            (int)dt->struct_ref->id.length,
            dt->struct_ref->id.start
        );
       break;
    case DT_FUNCTION:
        {
            nstr_assign_cstr(&nstr, "(");
            size_t const param_dts_len = narr_length(dt->func_sig->param_dts);
            for (size_t p = 0; p < param_dts_len; ++p)
            {
                nstr_t nstr_defer_fini param =
                    dt_to_n_source_nstr(&dt->func_sig->param_dts[p]);
                nstr_cat_nstr(&nstr, &param);
                if (p != param_dts_len-1){nstr_cat_cstr(&nstr, ", ");}
            }
            nstr_t nstr_defer_fini rtn =
                    dt_to_n_source_nstr(&dt->func_sig->rtn_dt);
            nstr_cat_fmt(&nstr, ") -> %s", rtn.data);
        }
        break;
    case DT_POINTER:
        nstr_assign_fmt(&nstr, "* %s", nstr.data);
        break;
    case DT_ARRAY:
        nstr_assign_fmt(
            &nstr,
            "[%zu%s] %s",
            dt->array.length,
            token_type_to_cstr(TOKEN_KEYWORD_U),
            nstr.data
        );
        break;
    case DT_UNRESOLVED_ID: /* intentional fallthrough */
    case DT_UNRESOLVED_TYPEOF_EXPR:
    case DT_UNRESOLVED_TYPEOF_DT:
        NPANIC_UNEXPECTED_CTRL_FLOW();
    case DT_UNSET:
        NPANIC_UNSET_CASE();
    default:
        NPANIC_DFLT_CASE();
    }

    //// Qualifiers
    if (dt->qualifiers.is_mut)
    {
        nstr_assign_fmt(&nstr, "mut %s", nstr.data);
    }

    return nstr;
}


//! Array-list where the 0th element is the outermost data type, the 1st element
//! is the 2nd outermost data type, ..., and the Nth element is the base type.
//! @br
//! This array is used by #_dt_to_c_source_nstr for manage the reversed
//! onion-like way C data types are written.
static thread_local struct dt const (*((_dt_reverse_list)[4096]));
//! Number of elements in #_dt_reverse_list.
static thread_local size_t _dt_reverse_list_len;
//! Tracked index into #_dt_reverse_list.
static thread_local size_t _dt_reverse_list_idx;
// C data types can best be described as an onion.
// The outermost layer of the type onion is the base type.
// The second-to-outermost layer (if it exists) is the innermost modifier.
// The third-to-outermost layer (if it exists) is the second-innermost modifier.
// Etc.
//
// For non function pointer types this takes the form:
//      <base-type> (<modifiers-and-id>)
// For function pointer types this take the form:
//      <return-type> (* (<modifiers-and-id>))(<parameter-types>)
//
// Examples:
// (1)  int (* volatile ((* const (ID))[3]));
//      "Declare ID as const pointer to array 3 of volatile pointer to int."
// (2)  int (* volatile ((* const (ID))[3]))(char, short)
//      "Declare ID as const pointer to array 3 of volatile pointer to function
//      (char, short) returning int."
//
// For data types that do not have identifiers, you omit the "(ID)" portion of
// the string.
// (2) Without the identifier "foo" would be written as:
//      int (* volatile ((* const)[3]))(char, short)
static inline nstr_t _dt_to_c_source_nstr(
    struct dt const* _dt,
    struct id const* id
)
{
    nstr_t nstr;

    // Populate the reverse list.
    _dt_reverse_list[_dt_reverse_list_len++] = _dt;
    if (NULL == _dt->inner)
    {
        nstr_init(&nstr);
        if (NULL != id)
        {
            nstr_assign_fmt(&nstr, "(%.*s)", (int)id->length, id->start);
        }
    }
    else
    {
        nstr = _dt_to_c_source_nstr(_dt->inner, id);
    }

    // Get the target data type at this level of the onion.
    struct dt const* target_dt = _dt_reverse_list[_dt_reverse_list_idx++];
    bool const target_is_void_base =
        dt_is_void(target_dt) && (_dt_reverse_list_len == 1);

    //// Qualifiers
    nstr_t nstr_defer_fini qualifiers_nstr;
    nstr_init(&qualifiers_nstr);
    if (!target_dt->qualifiers.is_mut && !target_is_void_base)
    {
        nstr_cat_cstr(&qualifiers_nstr, " const");
    }

    //// Tag-based behavior
    nstr_t nstr_defer_fini tag_nstr;
    nstr_init(&tag_nstr);
    switch (target_dt->tag)
    {
    case DT_VOID: nstr_assign_fmt(&nstr, "%s %s %s", "void",   qualifiers_nstr.data, nstr.data); break;
    case DT_U8:   nstr_assign_fmt(&nstr, "%s %s %s", "n_u8",   qualifiers_nstr.data, nstr.data); break;
    case DT_U16:  nstr_assign_fmt(&nstr, "%s %s %s", "n_u16",  qualifiers_nstr.data, nstr.data); break;
    case DT_U32:  nstr_assign_fmt(&nstr, "%s %s %s", "n_u32",  qualifiers_nstr.data, nstr.data); break;
    case DT_U64:  nstr_assign_fmt(&nstr, "%s %s %s", "n_u64",  qualifiers_nstr.data, nstr.data); break;
    case DT_U:    nstr_assign_fmt(&nstr, "%s %s %s", "n_u",    qualifiers_nstr.data, nstr.data); break;
    case DT_S8:   nstr_assign_fmt(&nstr, "%s %s %s", "n_s8",   qualifiers_nstr.data, nstr.data); break;
    case DT_S16:  nstr_assign_fmt(&nstr, "%s %s %s", "n_s16",  qualifiers_nstr.data, nstr.data); break;
    case DT_S32:  nstr_assign_fmt(&nstr, "%s %s %s", "n_s32",  qualifiers_nstr.data, nstr.data); break;
    case DT_S64:  nstr_assign_fmt(&nstr, "%s %s %s", "n_s64",  qualifiers_nstr.data, nstr.data); break;
    case DT_S:    nstr_assign_fmt(&nstr, "%s %s %s", "n_s",    qualifiers_nstr.data, nstr.data); break;
    case DT_F16:  nstr_assign_fmt(&nstr, "%s %s %s", "n_f16",  qualifiers_nstr.data, nstr.data); break;
    case DT_F32:  nstr_assign_fmt(&nstr, "%s %s %s", "n_f32",  qualifiers_nstr.data, nstr.data); break;
    case DT_F64:  nstr_assign_fmt(&nstr, "%s %s %s", "n_f64",  qualifiers_nstr.data, nstr.data); break;
    case DT_F128: nstr_assign_fmt(&nstr, "%s %s %s", "n_f128", qualifiers_nstr.data, nstr.data); break;
    case DT_BOOL: nstr_assign_fmt(&nstr, "%s %s %s", "n_bool", qualifiers_nstr.data, nstr.data); break;
    case DT_ASCII: nstr_assign_fmt(&nstr, "%s %s %s", "n_ascii", qualifiers_nstr.data, nstr.data); break;
    case DT_FUNCTION:
        {
            nstr_t nstr_defer_fini rtn =
                    dt_to_c_source_nstr(&target_dt->func_sig->rtn_dt, NULL);

            size_t const param_dts_len = narr_length(target_dt->func_sig->param_dts);
            nstr_t nstr_defer_fini params;
            nstr_init(&params);
            if (0 == param_dts_len)
            {
                nstr_cat_cstr(&params, "void");
            }
            for (size_t p = 0; p < param_dts_len; ++p)
            {
                nstr_t nstr_defer_fini param =
                    dt_to_c_source_nstr(&target_dt->func_sig->param_dts[p], NULL);

                nstr_cat_nstr(&params, &param);
                if (p != param_dts_len-1){nstr_cat_cstr(&params, ", ");}
            }

            nstr_assign_fmt(
                &nstr,
                "%s (*%s%s)(%s)",
                rtn.data,
                qualifiers_nstr.data,
                nstr.data,
                params.data
            );
        }
        break;
    case DT_STRUCT:
        nstr_assign_fmt(
            &nstr,
            "struct %.*s %s",
            (int)target_dt->struct_ref->id.length,
            target_dt->struct_ref->id.start,
            nstr.data
        );
        break;
    case DT_POINTER:
        nstr_assign_fmt(&tag_nstr, "* %s", qualifiers_nstr.data);
        nstr_assign_fmt(
            &nstr,
            _dt_reverse_list_len == _dt_reverse_list_idx+1 ? "%s%s" :  "(%s%s)",
            tag_nstr.data,
            nstr.data
        );
        break;
    case DT_ARRAY:
        nassert(target_dt->array.length > 0);
        nstr_assign_fmt(&tag_nstr, "[%zu]", target_dt->array.length);
        nstr_assign_fmt(
            &nstr,
            _dt_reverse_list_len == _dt_reverse_list_idx+1 ? "%s%s" :  "(%s%s)",
            nstr.data,
            tag_nstr.data
        );
        break;
    case DT_UNRESOLVED_ID: /* intentional fallthrough */
    case DT_UNRESOLVED_TYPEOF_EXPR:
    case DT_UNRESOLVED_TYPEOF_DT:
        NPANIC_UNEXPECTED_CTRL_FLOW();
    case DT_UNSET:
        NPANIC_UNSET_CASE();
    default:
        NPANIC_DFLT_CASE();
    }

    return nstr;
}

nstr_t dt_to_c_source_nstr(struct dt const* dt, struct id const* id)
{
    _dt_reverse_list_len = 0;
    _dt_reverse_list_idx = 0;
    return _dt_to_c_source_nstr(dt, id);
}

static bool check_dt_without_qualifiers_eq(
    struct dt const* from,
    struct dt const* to
)
{
    return (NULL == from->inner
            || check_dt_without_qualifiers_eq(from->inner, to->inner))
        && (from->tag == to->tag)
        && (DT_ARRAY != from->tag
            || from->array.length == to->array.length);
}

static bool check_qualifier_restricting_conversion(
    struct dt const* from,
    struct dt const* to,
    bool is_first_outtermost,
    bool is_second_outtermost
)
{
    return (NULL == from->inner
            || check_qualifier_restricting_conversion(
                from->inner,
                to->inner,
                false,
                is_first_outtermost))
        && (is_first_outtermost
            || (is_second_outtermost
                && qualifiers_is_restricting(&from->qualifiers, &to->qualifiers))
            || qualifiers_eq(&from->qualifiers, &to->qualifiers));
}

static bool check_T_pointer_to_void_pointer_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_pointer(from)
        && dt_is_void_pointer(to)
        && qualifiers_is_restricting(&from->inner->qualifiers, &to->inner->qualifiers);
}

static bool check_void_pointer_to_T_pointer_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_void_pointer(from)
        && dt_is_pointer(to)
        && qualifiers_is_restricting(&from->inner->qualifiers, &to->inner->qualifiers);
}

bool dt_can_implicitly_convert(
    struct dt const* from,
    struct dt const* to
)
{
    if (dt_is_void(from) || dt_is_void(to)){return false;}

    if (check_T_pointer_to_void_pointer_conversion(from, to)
        || check_void_pointer_to_T_pointer_conversion(from, to)){return true;}

    if (dt_length_all(from) != dt_length_all(to)){return false;}
    if (!check_dt_without_qualifiers_eq(from, to)){return false;}
    return check_qualifier_restricting_conversion(from, to, true, false);
}

bool dt_can_implicitly_convert__variable_init(
    struct dt const* from,
    struct dt const* to
)
{
    if (dt_is_array(from) && dt_is_array(to))
    {
        return from->array.length == to->array.length
            && dt_can_implicitly_convert__variable_init(from->inner, to->inner);
    }
    return dt_can_implicitly_convert(from, to);
}

static bool check_integer_to_integer_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_integer(from) && dt_is_integer(to);
}


static bool check_floating_point_to_floating_point_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_float(from) && dt_is_float(to);
}

static bool check_char_to_integer_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_char(from) && dt_is_integer(to);
}

static bool check_integer_to_char_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_integer(from) && dt_is_char(to);
}

static bool check_integer_to_floating_point_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_integer(from) && dt_is_float(to);
}

static bool check_floating_point_to_integer_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_float(from) && dt_is_integer(to);
}

static bool check_pointer_to_integer_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_pointer(from) && dt_is_integer(to);
}

static bool check_integer_to_pointer_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_integer(from) && dt_is_pointer(to);
}

static bool check_pointer_to_pointer_conversion(
    struct dt const* from,
    struct dt const* to
)
{
    return dt_is_pointer(from) && dt_is_pointer(to);
}

bool dt_can_explicitly_convert(
    struct dt const* from,
    struct dt const* to
)
{
    if (dt_is_void(from) || dt_is_void(to)){return false;}
    return dt_can_implicitly_convert(from, to)
        || check_integer_to_integer_conversion(from, to)
        || check_floating_point_to_floating_point_conversion(from, to)
        || check_char_to_integer_conversion(from, to)
        || check_integer_to_char_conversion(from, to)
        || check_integer_to_floating_point_conversion(from, to)
        || check_floating_point_to_integer_conversion(from, to)
        || check_pointer_to_integer_conversion(from, to)
        || check_integer_to_pointer_conversion(from, to)
        || check_pointer_to_pointer_conversion(from, to);
}

//====================================//
//      MEMBER VARIABLE               //
//====================================//
INIT_FINI_COUNTER_SOURCE(member_var)

void member_var_init(struct member_var* memb)
{
    INIT_FINI_COUNTER_INCR_INIT(member_var);
    id_init(&memb->id);
    dt_init(&memb->dt);
    memb->srctok = NULL;
}

void member_var_fini(struct member_var* memb)
{
    INIT_FINI_COUNTER_INCR_FINI(member_var);
    id_fini(&memb->id);
    dt_fini(&memb->dt);
}

//====================================//
//      MEMBER FUNCTION               //
//====================================//
INIT_FINI_COUNTER_SOURCE(member_func)

void member_func_init(struct member_func* memb)
{
    INIT_FINI_COUNTER_INCR_INIT(member_func);
    id_init(&memb->id);
    dt_init(&memb->dt);
    id_init(&memb->target.id);
    memb->srctok = NULL;
}

void member_func_fini(struct member_func* memb)
{
    INIT_FINI_COUNTER_INCR_FINI(member_func);
    id_fini(&memb->id);
    dt_fini(&memb->dt);
    id_fini(&memb->target.id);
}

//====================================//
//      STRUCT DEFINITION             //
//====================================//
INIT_FINI_COUNTER_SOURCE(struct_def)

void struct_def_init(struct struct_def* def)
{
    INIT_FINI_COUNTER_INCR_INIT(struct_def);
    MEMZERO_INSTANCE(def, typeof(*def));

    def->member_vars = narr_alloc(0, sizeof(struct member_var));
    def->member_funcs = narr_alloc(0, sizeof(struct member_func));
    def->srctok = NULL;
}

void struct_def_fini(struct struct_def* def)
{
    INIT_FINI_COUNTER_INCR_FINI(struct_def);

    size_t const member_vars_len = narr_length(def->member_vars);
    size_t const member_funcs_len = narr_length(def->member_funcs);

    for (size_t i = 0; i < member_vars_len; ++i)
    {
        member_var_fini(&(def->member_vars[i]));
    }
    narr_free(def->member_vars);
    for (size_t i = 0; i < member_funcs_len; ++i)
    {
        member_func_fini(&(def->member_funcs[i]));
    }
    narr_free(def->member_funcs);
}

ssize_t struct_def_member_var_idx(
    struct struct_def const* def,
    struct id const* id
)
{
    ssize_t const len = (ssize_t)narr_length(def->member_vars);
    for (ssize_t idx = 0; idx < len; ++idx)
    {
        if (id_eq(id, &def->member_vars[idx].id))
        {
            return idx;
        }
    }
    return MEMBER_VAR_NOT_FOUND;
}

ssize_t struct_def_member_func_idx(
    struct struct_def const* def,
    struct id const* id
)
{
    ssize_t const len = (ssize_t)narr_length(def->member_funcs);
    for (ssize_t idx = 0; idx < len; ++idx)
    {
        if (id_eq(id, &def->member_funcs[idx].id))
        {
            return idx;
        }
    }
    return MEMBER_FUNC_NOT_FOUND;
}

//====================================//
//      FUNCTION SIGNATURE            //
//====================================//
INIT_FINI_COUNTER_SOURCE(func_sig)

void func_sig_init(struct func_sig* func_sig)
{
    INIT_FINI_COUNTER_INCR_INIT(func_sig);
    MEMZERO_INSTANCE(func_sig, typeof(*func_sig));

    dt_init(&func_sig->rtn_dt);
    func_sig->param_dts = narr_alloc(0, sizeof(struct dt));
}

void func_sig_fini(struct func_sig* func_sig)
{
    INIT_FINI_COUNTER_INCR_FINI(func_sig);

    dt_fini(&func_sig->rtn_dt);
    for (size_t i = 0; i < narr_length(func_sig->param_dts); ++i)
    {
        dt_fini(&func_sig->param_dts[i]);
    }
    narr_free(func_sig->param_dts);
}

void func_sig_reset(struct func_sig* func_sig)
{
    func_sig_fini(func_sig);
    func_sig_init(func_sig);
}

void func_sig_assign(
    struct func_sig* dest,
    struct func_sig const* src,
    bool clear_srctok
)
{
    func_sig_reset(dest);

    dt_assign(&dest->rtn_dt, &src->rtn_dt, clear_srctok);

    for (size_t i = 0; i < narr_length(src->param_dts); ++i)
    {
        struct dt param;
        dt_init(&param);
        dt_assign(&param, &(src->param_dts[i]), clear_srctok);
        dest->param_dts = narr_push(dest->param_dts, &param);
    }
}

//============================================================================//
//      LITERAL VALUE                                                         //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(literal)

void literal_init(struct literal* literal)
{
    INIT_FINI_COUNTER_INCR_INIT(literal);
    MEMZERO_INSTANCE(literal, typeof(*literal));

    literal->tag = LITERAL_UNSET;

    literal->srctok = NULL;
}

void literal_fini(struct literal* literal)
{
    INIT_FINI_COUNTER_INCR_FINI(literal);

    if (LITERAL_STR == literal->tag)
    {
        nstr_fini(&literal->str);
    }
    else if (LITERAL_ARRAY == literal->tag)
    {
        for (size_t i = 0; i < narr_length(literal->array); ++i)
        {
            literal_fini(&literal->array[i]);
        }
        narr_free(literal->array);
    }

    literal->tag = LITERAL_UNSET;

    literal->srctok = NULL;
}

void literal_reset(struct literal* literal)
{
    literal_fini(literal);
    literal_init(literal);
}

void literal_assign(
    struct literal* dest,
    struct literal const* src,
    bool clear_srctok

)
{
    literal_reset(dest);

    dest->tag = src->tag;

    switch (src->tag)
    {
    case LITERAL_U8:     dest->u8        = src->u8;        break;
    case LITERAL_U16:    dest->u16       = src->u16;       break;
    case LITERAL_U32:    dest->u32       = src->u32;       break;
    case LITERAL_U64:    dest->u64       = src->u64;       break;
    case LITERAL_U:      dest->u         = src->u;         break;
    case LITERAL_S8:     dest->s8        = src->s8;        break;
    case LITERAL_S16:    dest->s16       = src->s16;       break;
    case LITERAL_S32:    dest->s32       = src->s32;       break;
    case LITERAL_S64:    dest->s64       = src->s64;       break;
    case LITERAL_S:      dest->s         = src->s;         break;
    case LITERAL_F32:    dest->f32       = src->f32;       break;
    case LITERAL_F64:    dest->f64       = src->f64;       break;
    case LITERAL_BOOL:   dest->boolean   = src->boolean;   break;
    case LITERAL_CHAR:   dest->character = src->character; break;
    case LITERAL_STR:
        nstr_init_nstr(&dest->str, &src->str);
        break;
    case LITERAL_NULL:
        /* nothing */
        break;
    case LITERAL_ARRAY:
        dest->array = narr_alloc(0, sizeof(struct literal));
        for (size_t i = 0; i < narr_length(src->array); ++i)
        {
            struct literal literal;
            literal_init(&literal);
            literal_assign(&literal, &src->array[i], clear_srctok);
            dest->array = narr_push(dest->array, &literal);
        }
        break;
    case LITERAL_ARRAY_FILL:
        dest->array_fill_len = src->array_fill_len;
        break;
    default:
        NPANIC_DFLT_CASE();
    }

    dest->srctok = clear_srctok ? NULL : src->srctok;
}

struct dt literal_get_dt(struct literal const* literal)
{
    struct dt dt;
    dt_init(&dt);

    switch (literal->tag)
    {
    case LITERAL_U8:   dt.tag = DT_U8;    break;
    case LITERAL_U16:  dt.tag = DT_U16;   break;
    case LITERAL_U32:  dt.tag = DT_U32;   break;
    case LITERAL_U64:  dt.tag = DT_U64;   break;
    case LITERAL_U:    dt.tag = DT_U;     break;
    case LITERAL_S8:   dt.tag = DT_S8;    break;
    case LITERAL_S16:  dt.tag = DT_S16;   break;
    case LITERAL_S32:  dt.tag = DT_S32;   break;
    case LITERAL_S64:  dt.tag = DT_S64;   break;
    case LITERAL_S:    dt.tag = DT_S;     break;
    case LITERAL_F32:  dt.tag = DT_F32;   break;
    case LITERAL_F64:  dt.tag = DT_F64;   break;
    case LITERAL_BOOL: dt.tag = DT_BOOL;  break;
    case LITERAL_CHAR: dt.tag = DT_ASCII; break;
    case LITERAL_STR:
        {
            struct dt* const inner = nalloc(sizeof(struct dt));
            dt_init(inner);
            inner->tag = DT_ASCII;
            dt.inner = inner;
            dt.tag = DT_ARRAY;
            dt.array.length = literal->str.len + NULL_TERMINATOR_LEN;
        }
        break;
    case LITERAL_NULL:
        {
            struct dt* const inner = nalloc(sizeof(struct dt));
            dt_init(inner);
            inner->tag = DT_VOID;
            inner->qualifiers.is_mut = true;
            dt.inner = inner;
            dt.tag = DT_POINTER;
        }
        break;
    case LITERAL_ARRAY:
        {
            struct dt* const inner = nalloc(sizeof(struct dt));
            *inner = literal_get_dt(&literal->array[0]);
            dt.inner = inner;
            dt.tag = DT_ARRAY;

            struct literal const* const back = narr_back(literal->array);
            if (LITERAL_ARRAY_FILL == back->tag)
            {
                dt.array.length = back->array_fill_len;
            }
            else
            {
                dt.array.length = narr_length(literal->array);
            }
            nassert(dt.array.length > 0);
        }
        break;
    case LITERAL_ARRAY_FILL:
        NPANIC_UNEXPECTED_CTRL_FLOW();
    default:
        NPANIC_DFLT_CASE();
    }

    return dt;
}

//============================================================================//
//      EXPRESSION                                                            //
//============================================================================//

INIT_FINI_COUNTER_SOURCE(expr)

void expr_init(struct expr* expr)
{
    INIT_FINI_COUNTER_INCR_INIT(expr);
    MEMZERO_INSTANCE(expr, typeof(*expr));

    dt_init(&expr->dt);
}

void expr_fini(struct expr* expr)
{
    INIT_FINI_COUNTER_INCR_FINI(expr);

    if (expr_is_postfix(expr))
    {
        expr_fini(expr->postfix.lhs);
        nfree(expr->postfix.lhs);
    }

    switch (expr->tag)
    {
    case EXPR_UNSET:
        /* nothing */
        break;

    case EXPR_PRIMARY_IDENTIFIER:
        id_fini(&expr->id);
        break;

    case EXPR_PRIMARY_LITERAL:
        literal_fini(expr->literal);
        nfree(expr->literal);
        break;

    case EXPR_PRIMARY_PAREN:
        expr_fini(expr->paren);
        nfree(expr->paren);
        break;

    case EXPR_POSTFIX_FUNCTION_CALL:
        for (size_t i = 0; i < narr_length(expr->postfix.args); ++i)
        {
            expr_fini(&expr->postfix.args[i]);
        }
        narr_free(expr->postfix.args);
        break;

    case EXPR_POSTFIX_SUBSCRIPT:
        expr_fini(expr->postfix.subscript);
        nfree(expr->postfix.subscript);
        break;

    case EXPR_POSTFIX_ACCESS_DOT: /* intentional fallthrough */
    case EXPR_POSTFIX_ACCESS_ARROW:
        id_fini(&expr->postfix.access.id);
        break;

    case EXPR_UNARY_ADDR_OF:      /* intentional fallthrough */
    case EXPR_UNARY_DEREF:        /* intentional fallthrough */
    case EXPR_UNARY_DECAY:        /* intentional fallthrough */
    case EXPR_UNARY_PLUS:         /* intentional fallthrough */
    case EXPR_UNARY_MINUS:        /* intentional fallthrough */
    case EXPR_UNARY_BITWISE_NOT:  /* intentional fallthrough */
    case EXPR_UNARY_LOGICAL_NOT:  /* intentional fallthrough */
    case EXPR_UNARY_COUNTOF_EXPR: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_EXPR:
        expr_fini(expr->unary.rhs_expr);
        nfree(expr->unary.rhs_expr);
        break;

    case EXPR_UNARY_COUNTOF_DT: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_DT:
        dt_fini(expr->unary.rhs_dt);
        nfree(expr->unary.rhs_dt);
        break;

    case EXPR_BINARY_ASSIGN:      /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_OR:  /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_AND: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_OR:  /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_XOR: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_AND: /* intentional fallthrough */
    case EXPR_BINARY_REL_EQ:      /* intentional fallthrough */
    case EXPR_BINARY_REL_NE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GE:      /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_L:     /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_R:     /* intentional fallthrough */
    case EXPR_BINARY_PLUS:        /* intentional fallthrough */
    case EXPR_BINARY_MINUS:       /* intentional fallthrough */
    case EXPR_BINARY_PTR_DIFF:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_PLUS:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_MINUS:   /* intentional fallthrough */
    case EXPR_BINARY_MULT:        /* intentional fallthrough */
    case EXPR_BINARY_DIV:
        expr_fini(expr->binary.lhs_expr);
        nfree(expr->binary.lhs_expr);

        expr_fini(expr->binary.rhs_expr);
        nfree(expr->binary.rhs_expr);
        break;

    case EXPR_BINARY_CAST:
        expr_fini(expr->binary.lhs_expr);
        nfree(expr->binary.lhs_expr);

        dt_fini(expr->binary.rhs_dt);
        nfree(expr->binary.rhs_dt);
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    dt_fini(&expr->dt);
}

void expr_reset(struct expr* expr)
{
    expr_fini(expr);
    expr_init(expr);
}

void expr_assign(
    struct expr* dest,
    struct expr const* src,
    bool clear_srctok
)
{
    expr_reset(dest);

    dest->tag = src->tag;

    if (expr_is_postfix(src))
    {
        dest->postfix.lhs = nalloc(sizeof(struct expr));
        expr_init(dest->postfix.lhs);
        expr_assign(dest->postfix.lhs, src->postfix.lhs, clear_srctok);
    }
    switch (src->tag)
    {
    case EXPR_UNSET:
        /* nothing */
        break;

    case EXPR_PRIMARY_IDENTIFIER:
        id_init(&dest->id);
        id_assign(&dest->id, &src->id, clear_srctok);
        break;

    case EXPR_PRIMARY_LITERAL:
        dest->literal = nalloc(sizeof(struct literal));
        literal_init(dest->literal);
        literal_assign(dest->literal, src->literal, clear_srctok);
        break;

    case EXPR_PRIMARY_PAREN:
        dest->paren = nalloc(sizeof(struct expr));
        expr_init(dest->paren);
        expr_assign(dest->paren, src->paren, clear_srctok);
        break;

    case EXPR_POSTFIX_FUNCTION_CALL:
        nassert(NULL != src->postfix.args);
        dest->postfix.args = narr_alloc(0, sizeof(struct expr));
        for (size_t i = 0; i < narr_length(src->postfix.args); ++i)
        {
            struct expr param;
            expr_init(&param);
            expr_assign(&param, &src->postfix.args[i], clear_srctok);
            dest->postfix.args = narr_push(dest->postfix.args, &param);
        }
        nassert(NULL != dest->postfix.args);
        nassert(NULL != dest->postfix.lhs);
        break;

    case EXPR_POSTFIX_SUBSCRIPT:
        dest->postfix.subscript = nalloc(sizeof(struct expr));
        expr_init(dest->postfix.subscript);
        expr_assign(dest->postfix.subscript, src->postfix.subscript, clear_srctok);
        nassert(NULL != dest->postfix.lhs);
        break;

    case EXPR_POSTFIX_ACCESS_DOT: /* intentional fallthrough */
    case EXPR_POSTFIX_ACCESS_ARROW:
        id_init(&dest->postfix.access.id);
        id_assign(&dest->postfix.access.id, &src->postfix.access.id, clear_srctok);
        nassert(NULL != dest->postfix.lhs);
        break;

    case EXPR_UNARY_ADDR_OF:      /* intentional fallthrough */
    case EXPR_UNARY_DEREF:        /* intentional fallthrough */
    case EXPR_UNARY_DECAY:        /* intentional fallthrough */
    case EXPR_UNARY_PLUS:         /* intentional fallthrough */
    case EXPR_UNARY_MINUS:        /* intentional fallthrough */
    case EXPR_UNARY_BITWISE_NOT:  /* intentional fallthrough */
    case EXPR_UNARY_LOGICAL_NOT:  /* intentional fallthrough */
    case EXPR_UNARY_COUNTOF_EXPR: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_EXPR:
        dest->unary.rhs_expr = nalloc(sizeof(struct expr));
        expr_init(dest->unary.rhs_expr);
        expr_assign(dest->unary.rhs_expr, src->unary.rhs_expr, clear_srctok);
        break;

    case EXPR_UNARY_COUNTOF_DT: /* intentional fallthrough */
    case EXPR_UNARY_SIZEOF_DT:
        dest->unary.rhs_dt = nalloc(sizeof(struct dt));
        dt_init(dest->unary.rhs_dt);
        dt_assign(dest->unary.rhs_dt, src->unary.rhs_dt, clear_srctok);
        break;

    case EXPR_BINARY_ASSIGN:      /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_OR:  /* intentional fallthrough */
    case EXPR_BINARY_LOGICAL_AND: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_OR:  /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_XOR: /* intentional fallthrough */
    case EXPR_BINARY_BITWISE_AND: /* intentional fallthrough */
    case EXPR_BINARY_REL_EQ:      /* intentional fallthrough */
    case EXPR_BINARY_REL_NE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GT:      /* intentional fallthrough */
    case EXPR_BINARY_REL_LE:      /* intentional fallthrough */
    case EXPR_BINARY_REL_GE:      /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_L:     /* intentional fallthrough */
    case EXPR_BINARY_SHIFT_R:     /* intentional fallthrough */
    case EXPR_BINARY_PLUS:        /* intentional fallthrough */
    case EXPR_BINARY_MINUS:       /* intentional fallthrough */
    case EXPR_BINARY_PTR_DIFF:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_PLUS:    /* intentional fallthrough */
    case EXPR_BINARY_PTR_MINUS:   /* intentional fallthrough */
    case EXPR_BINARY_MULT:        /* intentional fallthrough */
    case EXPR_BINARY_DIV:
        dest->binary.lhs_expr = nalloc(sizeof(struct expr));
        expr_init(dest->binary.lhs_expr);
        expr_assign(dest->binary.lhs_expr, src->binary.lhs_expr, clear_srctok);

        dest->binary.rhs_expr = nalloc(sizeof(struct expr));
        expr_init(dest->binary.rhs_expr);
        expr_assign(dest->binary.rhs_expr, src->binary.rhs_expr, clear_srctok);
        break;

    case EXPR_BINARY_CAST:
        dest->binary.lhs_expr = nalloc(sizeof(struct expr));
        expr_init(dest->binary.lhs_expr);
        expr_assign(dest->binary.lhs_expr, src->binary.lhs_expr, clear_srctok);

        dest->binary.rhs_dt = nalloc(sizeof(struct dt));
        dt_init(dest->binary.rhs_dt);
        dt_assign(dest->binary.rhs_dt, src->binary.rhs_dt, clear_srctok);
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    dt_assign(&dest->dt, &src->dt, clear_srctok);

    dest->valcat = src->valcat;

    dest->srctok = clear_srctok ? NULL : src->srctok;
}

bool expr_is_primary(struct expr const* expr)
{
    return expr->tag > __EXPR_PRIMARY_BEGIN__
        && expr->tag < __EXPR_PRIMARY_END__;
}

bool expr_is_postfix(struct expr const* expr)
{
    return expr->tag > __EXPR_POSTFIX_BEGIN__
        && expr->tag < __EXPR_POSTFIX_END__;
}

bool expr_is_unary(struct expr const* expr)
{
    return expr->tag > __EXPR_UNARY_BEGIN__
        && expr->tag < __EXPR_UNARY_END__;
}

bool expr_is_binary(struct expr const* expr)
{
    return expr->tag > __EXPR_BINARY_BEGIN__
        && expr->tag < __EXPR_BINARY_END__;
}

bool expr_is_lvalue(struct expr const* expr)
{
    switch (expr->valcat)
    {
    case VALCAT_NONE:   return false;
    case VALCAT_RVALUE: return false;
    case VALCAT_LVALUE: return true;
    default:
        NPANIC_DFLT_CASE();
    }
}

bool expr_is_rvalue(struct expr const* expr)
{
    switch (expr->valcat)
    {
    case VALCAT_NONE:   return false;
    case VALCAT_RVALUE: return true;
    case VALCAT_LVALUE: return true; // lvalues are by definition also rvalues.
    default:
        NPANIC_DFLT_CASE();
    }
}

//============================================================================//
//      IN-SOURCE IDENTIFIER DECLARATION                                      //
//============================================================================//
//====================================//
//      VARIABLE DECLARATION          //
//====================================//
INIT_FINI_COUNTER_SOURCE(var_decl)

void var_decl_init(struct var_decl* decl)
{
    INIT_FINI_COUNTER_INCR_INIT(var_decl);
    MEMZERO_INSTANCE(decl, typeof(*decl));

    id_init(&decl->id);
    dt_init(&decl->dt);
}

void var_decl_fini(struct var_decl* decl)
{
    INIT_FINI_COUNTER_INCR_FINI(var_decl);

    id_fini(&decl->id);
    dt_fini(&decl->dt);
    if (decl->def != NULL)
    {
        expr_fini(decl->def);
        nfree(decl->def);
    }
}

//====================================//
//      FUNCTION DECLARATION          //
//====================================//
INIT_FINI_COUNTER_SOURCE(func_decl)

void func_decl_init(struct func_decl* decl)
{
    INIT_FINI_COUNTER_INCR_INIT(func_decl);
    MEMZERO_INSTANCE(decl, typeof(*decl));

    id_init(&decl->id);

    dt_init(&decl->dt);
    decl->dt.tag = DT_FUNCTION;
    decl->dt.func_sig = nalloc(sizeof(struct func_sig));
    func_sig_init(decl->dt.func_sig);

    decl->param_ids = narr_alloc(0, sizeof(struct id));
}

void func_decl_fini(struct func_decl* decl)
{
    INIT_FINI_COUNTER_INCR_FINI(func_decl);

    id_fini(&decl->id);
    dt_fini(&decl->dt);

    size_t const param_ids_len = narr_length(decl->param_ids);
    for (size_t i = 0; i < param_ids_len; ++i)
    {
        id_fini(&decl->param_ids[i]);
    }
    narr_free(decl->param_ids);

    if (NULL != decl->def)
    {
        scope_fini(decl->def);
        nfree(decl->def);
    }
}

//====================================//
//      STRUCT DECLARATION            //
//====================================//
INIT_FINI_COUNTER_SOURCE(struct_decl)

void struct_decl_init(struct struct_decl* decl)
{
    INIT_FINI_COUNTER_INCR_INIT(struct_decl);
    MEMZERO_INSTANCE(decl, typeof(*decl));

    id_init(&decl->id);
}

void struct_decl_fini(struct struct_decl* decl)
{
    INIT_FINI_COUNTER_INCR_FINI(struct_decl);

    id_fini(&decl->id);
    if (NULL != decl->def)
    {
        struct_def_fini(decl->def);
        nfree(decl->def);
    }
}

//============================================================================//
//      CONTROL CONSTRUCTS                                                    //
//============================================================================//
//====================================//
//      INDICATIVE CONDITIONAL        //
//====================================//
INIT_FINI_COUNTER_SOURCE(indicative_conditional)

void indicative_conditional_init(struct indicative_conditional* cond)
{
    INIT_FINI_COUNTER_INCR_INIT(indicative_conditional);
    MEMZERO_INSTANCE(cond, typeof(*cond));

    expr_init(&cond->condition);

    cond->body = nalloc(sizeof(struct scope));
    scope_init(cond->body, UNDEFINED_SCOPE_PARENT);
}

void indicative_conditional_fini(struct indicative_conditional* cond)
{
    INIT_FINI_COUNTER_INCR_FINI(indicative_conditional);

    expr_fini(&cond->condition);

    scope_fini(cond->body);
    nfree(cond->body);
}

//====================================//
//      IF-ELIF-ELSE                  //
//====================================//
INIT_FINI_COUNTER_SOURCE(if_elif_else)

void if_elif_else_init(struct if_elif_else* if_elif_else)
{
    if_elif_else->if_elif_ind_cond = narr_alloc(0, sizeof(struct indicative_conditional));

    if_elif_else->else_body = NULL;
}

void if_elif_else_fini(struct if_elif_else* if_elif_else)
{
    for (size_t i = 0; i < narr_length(if_elif_else->if_elif_ind_cond); ++i)
    {
        indicative_conditional_fini(&if_elif_else->if_elif_ind_cond[i]);
    }
    narr_free(if_elif_else->if_elif_ind_cond);

    if (NULL != if_elif_else->else_body)
    {
        scope_fini(if_elif_else->else_body);
        nfree(if_elif_else->else_body);
    }
}

//====================================//
//      LOOP                          //
//====================================//
INIT_FINI_COUNTER_SOURCE(loop)

void loop_init(struct loop* loop)
{
    INIT_FINI_COUNTER_INCR_INIT(loop);
    MEMZERO_INSTANCE(loop, typeof(*loop));

    loop->body = nalloc(sizeof(*loop->body));
    scope_init(loop->body, UNDEFINED_SCOPE_PARENT);
}

void loop_fini(struct loop* loop)
{
    INIT_FINI_COUNTER_INCR_FINI(loop);

    switch (loop->tag)
    {
    case LOOP_UNSET:
        /* nothing */
        break;

    case LOOP_CONDITIONAL:
        expr_fini(loop->cond.expr);
        nfree(loop->cond.expr);
        break;

    case LOOP_RANGE:
        var_decl_fini(&loop->range.var_decl);
        expr_fini(loop->range.lower_bound);
        nfree(loop->range.lower_bound);
        expr_fini(loop->range.upper_bound);
        nfree(loop->range.upper_bound);
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    scope_fini(loop->body);
    nfree(loop->body);
}

//============================================================================//
//      ALIAS                                                                 //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(alias)

void alias_init(struct alias* alias)
{
    INIT_FINI_COUNTER_INCR_INIT(alias);
    MEMZERO_INSTANCE(alias, typeof(*alias));

    alias->tag = ALIAS_UNSET;

    id_init(&alias->new_id);

    alias->is_export = false;

    alias->srctok = NULL;
}

void alias_fini(struct alias* alias)
{
    INIT_FINI_COUNTER_INCR_FINI(alias);

    id_fini(&alias->new_id);

    switch (alias->tag)
    {
    case ALIAS_UNSET:
        /* nothing */
        break;

    case ALIAS_DT:
        dt_fini(&alias->aliased.dt);
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    alias->srctok = NULL;
}

//============================================================================//
//      STATEMENT                                                             //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(stmt)

void stmt_init(struct stmt* stmt)
{
    INIT_FINI_COUNTER_INCR_INIT(stmt);
    MEMZERO_INSTANCE(stmt, typeof(*stmt));
}

void stmt_fini(struct stmt* stmt)
{
    INIT_FINI_COUNTER_INCR_FINI(stmt);

    switch (stmt->tag)
    {
    case STMT_UNSET:
        /* nothing */
        break;

    case STMT_EMPTY:
        /* nothing */
        break;

    case STMT_VARIABLE_DECLARATION:
        var_decl_fini(stmt->var_decl);
        nfree(stmt->var_decl);
        break;

    case STMT_FUNCTION_DECLARATION:
        func_decl_fini(stmt->func_decl);
        nfree(stmt->func_decl);
        break;

    case STMT_STRUCT_DECLARATION:
        struct_decl_fini(stmt->struct_decl);
        nfree(stmt->struct_decl);
        break;

    case STMT_IF_ELIF_ELSE:
        if_elif_else_fini(stmt->if_elif_else);
        nfree(stmt->if_elif_else);
        break;

    case STMT_LOOP:
        loop_fini(stmt->loop);
        nfree(stmt->loop);
        break;

    case STMT_DEFER:
        scope_fini(stmt->defer.scope);
        nfree(stmt->defer.scope);
        break;

    case STMT_BREAK:
        /*nothing*/
        break;

    case STMT_CONTINUE:
        /*nothing*/
        break;

    case STMT_RETURN:
        if (NULL != stmt->rtn_expr)
        {
            expr_fini(stmt->rtn_expr);
            nfree(stmt->rtn_expr);
        }
        break;

    case STMT_EXPR:
        expr_fini(stmt->expr);
        nfree(stmt->expr);
        break;

    case STMT_SCOPE:
        scope_fini(stmt->scope);
        nfree(stmt->scope);
        break;

    case STMT_IMPORT:
        nstr_fini(&stmt->import_string);
        break;

    case STMT_ALIAS:
        alias_fini(stmt->alias);
        nfree(stmt->alias);
        break;

    default:
        NPANIC_DFLT_CASE();
    }
}

//============================================================================//
//      LEXICAL SCOPE                                                         //
//============================================================================//
//====================================//
//      SYMBOL                        //
//====================================//
INIT_FINI_COUNTER_SOURCE(symbol)

void symbol_init(struct symbol* symbol)
{
    INIT_FINI_COUNTER_INCR_INIT(symbol);
    MEMZERO_INSTANCE(symbol, typeof(*symbol));

    symbol->tag = SYMBOL_UNSET;

    symbol->id = NULL;
    symbol->dt = NULL;
    symbol->srctok = NULL;

    symbol->module_idx = current_module_idx;

    symbol->uses = 0;

    symbol->is_export = false;

    symbol->is_defined = false;
}

void symbol_init_variable(
    struct symbol* symbol,
    struct var_decl* var_decl
)
{
    nassert(NULL != var_decl);
    nassert(NULL != var_decl->srctok);
    symbol_init(symbol);

    symbol->tag = SYMBOL_VARIABLE;

    symbol->ref.var_decl = var_decl;

    symbol->id = &var_decl->id;
    symbol->dt = &var_decl->dt;
    symbol->is_export = var_decl->is_export;
    symbol->is_export = NULL != var_decl->def;
    symbol->srctok = var_decl->srctok;
}

void symbol_init_function(
    struct symbol* symbol,
    struct func_decl* func_decl
)
{
    nassert(NULL != func_decl);
    nassert(NULL != func_decl->srctok);
    symbol_init(symbol);

    symbol->tag = SYMBOL_FUNCTION;

    symbol->ref.func_decl = func_decl;

    symbol->id = &func_decl->id;
    symbol->dt = &func_decl->dt;
    symbol->is_export = func_decl->is_export;
    symbol->srctok = func_decl->srctok;
}

void symbol_init_struct(
    struct symbol* symbol,
    struct struct_decl* struct_decl
)
{
    nassert(NULL != struct_decl);
    nassert(NULL != struct_decl->srctok);
    symbol_init(symbol);

    symbol->tag = SYMBOL_STRUCT;

    symbol->ref.struct_decl = struct_decl;

    symbol->id = &struct_decl->id;

    symbol->dt = nalloc(sizeof(struct dt));
    dt_init(symbol->dt);
    symbol->dt->tag = DT_STRUCT;
    symbol->dt->struct_ref = struct_decl;

    symbol->is_export = struct_decl->is_export;
    symbol->is_defined = NULL != struct_decl->def;
    symbol->srctok = struct_decl->srctok;
}

void symbol_init_alias(
    struct symbol* symbol,
    struct alias* alias
)
{
    nassert(NULL != alias);
    nassert(NULL != alias->srctok);
    symbol_init(symbol);

    symbol->tag = SYMBOL_ALIAS;

    symbol->ref.alias = alias;

    symbol->id = &alias->new_id;

    switch (alias->tag)
    {
    case ALIAS_DT:
        nassert(DT_UNSET != alias->aliased.dt.tag);
        symbol->dt = &alias->aliased.dt;
        symbol->is_export = alias->is_export;
        break;

    default:
        NPANIC_DFLT_CASE();
    }

    symbol->srctok = alias->srctok;

    symbol->is_defined = true;
}

void symbol_fini(struct symbol* symbol)
{
    INIT_FINI_COUNTER_INCR_FINI(symbol);

    if (SYMBOL_STRUCT == symbol->tag)
    {
        dt_fini(symbol->dt);
        nfree(symbol->dt);
    }
}

void symbol_mark_use(struct symbol* symbol)
{
    symbol->uses += 1;
}

//====================================//
//      SYMBOL TABLE                  //
//====================================//
INIT_FINI_COUNTER_SOURCE(symbol_table)

#define SYMBOL_TABLE_MAX_ELEMENTS DICT_MAX_ELEMENTS
#define SYMBOL_TABLE_MAX_LOAD_FACTOR ((double)0.75)

static void symbol_table_dict_grow(struct symbol_table* symbol_table)
{
    uint32_t const old_dict_prime_idx = symbol_table->_dict_prime_idx;
    uint32_t const new_dict_prime_idx = symbol_table->_dict_prime_idx + 1;
    if (UNLIKELY(new_dict_prime_idx == DICT_PRIMES_LENGTH))
    {
        NPANIC_DICT_TOO_LARGE();
    }
    uint32_t const old_dict_length = dict_primes[old_dict_prime_idx];
    uint32_t const new_dict_length = dict_primes[new_dict_prime_idx];

    // Allocate memory for the resized dictionary.
    uintmax_t const new_dict_size = new_dict_length*sizeof(struct _stdict_slot);
    struct _stdict_slot* const old_dict = symbol_table->_dict;
    struct _stdict_slot* const new_dict = nalloc(new_dict_size);
    memset(new_dict, 0, new_dict_size);

    // Copy elements from the old dictionary into the new dictionary.
    for (uint32_t i = 0; i < old_dict_length; ++i)
    {
        if (!old_dict[i].in_use){continue;}

        uint32_t probe_idx = DICT_HASH_IDX(old_dict[i].hash, new_dict_length);
        while (new_dict[probe_idx].in_use)
        {
            DICT_INCR_LINEAR_PROBE(probe_idx, new_dict_length);
        }
        new_dict[probe_idx] = old_dict[i];
    }

    // Update symbol table members and free the old dictionary.
    symbol_table->_dict = new_dict;
    symbol_table->_dict_prime_idx = new_dict_prime_idx;
    nfree(old_dict);
}

static struct symbol* symbol_table_find_from_hash(
    struct _stdict_slot* dict,
    uint32_t dict_length,
    char const* symstr,
    size_t symstr_length,
    uint32_t hash
)
{
    uint32_t probe_idx = DICT_HASH_IDX(hash, dict_length);
    while (dict[probe_idx].in_use)
    {
        struct symbol* const symbol = &dict[probe_idx].symbol;
        struct id const* const id = symbol->id;
        char const* const id_str = id->start;
        size_t const id_length = id->length;
        bool const is_found =
            symstr_length == id_length
            && cstr_n_eq(symstr, id_str, symstr_length);
        if (is_found){return symbol;}
        DICT_INCR_LINEAR_PROBE(probe_idx, dict_length);
    }
    return NULL;
}

void symbol_table_init(struct symbol_table* symbol_table)
{
    INIT_FINI_COUNTER_INCR_INIT(symbol_table);
    MEMZERO_INSTANCE(symbol_table, typeof(*symbol_table));

    uint32_t const initial_dict_prime_idx = 0;
    uint32_t const initial_dict_length = dict_primes[initial_dict_prime_idx];
    symbol_table->_dict =
        calloc(initial_dict_length, sizeof(struct _stdict_slot));
    symbol_table->_dict_prime_idx = initial_dict_prime_idx;
    symbol_table->_dict_slots_in_use = 0;

    symbol_table->module_idx = current_module_idx;
}

void symbol_table_fini(struct symbol_table* symbol_table)
{
    INIT_FINI_COUNTER_INCR_FINI(symbol_table);

    struct _stdict_slot* const dict = symbol_table->_dict;
    uint32_t const dict_prime_idx = symbol_table->_dict_prime_idx;
    uint32_t const dict_length = dict_primes[dict_prime_idx];
    for (size_t i = 0; i < dict_length; ++i)
    {
        if (dict[i].in_use && symbol_table->module_idx == dict[i].symbol.module_idx)
        {
            symbol_fini(&dict[i].symbol);
        }
    }
    nfree(dict);
}

struct symbol* symbol_table_insert(
    struct symbol_table* symbol_table,
    struct symbol const* symbol
)
{
    struct _stdict_slot* dict = symbol_table->_dict;
    uint32_t dict_prime_idx = symbol_table->_dict_prime_idx;
    uint32_t dict_length = dict_primes[dict_prime_idx];

    char const* const symstr = symbol->id->start;
    size_t const symstr_length = symbol->id->length;
    uint32_t const hash = hash_char_slice(symstr, symstr_length);

    // Check if the symbol already exists in the table.
    struct symbol const* existing_symbol =
        symbol_table_find_from_hash(dict, dict_length, symstr, symstr_length, hash);
    if (NULL != existing_symbol)
    {
        return NULL;
    }

    // Since the symbol does not exist in the table, check if the table needs
    // resizing, resize the table, and then insert the new symbol.
    symbol_table->_dict_slots_in_use += 1;
    double const load_factor =
        dict_slot_load_factor(symbol_table->_dict_slots_in_use, dict_length);
    if (load_factor > SYMBOL_TABLE_MAX_LOAD_FACTOR)
    {
        symbol_table_dict_grow(symbol_table);
        dict = symbol_table->_dict;
        dict_prime_idx = symbol_table->_dict_prime_idx;
        dict_length = dict_primes[dict_prime_idx];
    }

    uint32_t probe_idx = DICT_HASH_IDX(hash, dict_length);
    while (dict[probe_idx].in_use)
    {
        DICT_INCR_LINEAR_PROBE(probe_idx, dict_length);
    }

    dict[probe_idx].symbol = *symbol;
    dict[probe_idx].in_use = true;
    dict[probe_idx].hash = hash;
    symbol_table->most_recent_symbol = &dict[probe_idx].symbol;
    return &dict[probe_idx].symbol;
}

struct symbol* symbol_table_find(
    struct symbol_table* symbol_table,
    char const* symstr,
    size_t symstr_length
)
{
    struct _stdict_slot* dict = symbol_table->_dict;
    uint32_t dict_prime_idx = symbol_table->_dict_prime_idx;
    uint32_t dict_length = dict_primes[dict_prime_idx];
    uint32_t const hash = hash_char_slice(symstr, symstr_length);

    struct symbol* const symbol =
        symbol_table_find_from_hash(dict, dict_length, symstr, symstr_length, hash);

    return symbol;
}

//====================================//
//      SCOPE                         //
//====================================//
INIT_FINI_COUNTER_SOURCE(scope)

void scope_init(struct scope* scope, struct scope const* parent_scope)
{
    INIT_FINI_COUNTER_INCR_INIT(scope);
    MEMZERO_INSTANCE(scope, typeof(*scope));

    scope->uid = (uint32_t)-1;

    scope->parent = parent_scope;

    scope->symbol_table = nalloc(sizeof(struct symbol_table));
    symbol_table_init(scope->symbol_table);

    scope->stmts = narr_alloc(0, sizeof(struct stmt));

    scope->deferred_scopes = narr_alloc(0, sizeof(struct scope*));

    scope->is_within_defer = false;
    scope->is_within_defer_primary = false;
    scope->is_within_loop = false;
    scope->is_within_loop_primary = false;

    scope->srctok = NULL;
}

void scope_fini(struct scope* scope)
{
    INIT_FINI_COUNTER_INCR_FINI(scope);

    symbol_table_fini(scope->symbol_table);
    nfree(scope->symbol_table);

    for (size_t i = 0; i < narr_length(scope->stmts); ++i)
    {
        stmt_fini(&scope->stmts[i]);
    }
    narr_free(scope->stmts);

    narr_free(scope->deferred_scopes);
}

bool scope_is_global(struct scope const* scope)
{
    return scope->parent == GLOBAL_SCOPE_PARENT_PTR;
}

int scope_contains_entry_func(struct scope* scope)
{
    nlogf(LOG_DEBUG, "Checking if scope contains entry function.");

    struct symbol* const symbol =
        symbol_table_find(scope->symbol_table, "entry", cstr_len("entry"));

    if (NULL == symbol)
    {
        nlogf(LOG_DEBUG, "Scope contains no symbols with the name 'entry'.");
        return NO_ENTRY_FUNC;
    }

    struct dt const* const symbol_dt = symbol->dt;
    if (!dt_is_function(symbol_dt))
    {
        nlogf(LOG_DEBUG, "The 'entry' is not a function.");
        return INVALID_ENTRY_FUNC;
    }

    struct func_sig const* const func_sig = symbol_dt->func_sig;

    // func entry : () -> void
    if (
        dt_is_void(&func_sig->rtn_dt)
        && (narr_length(func_sig->param_dts) == 0)
    )
    {
        return ENTRY_FUNC_NO_ARGS_RTN_VOID;
    }

    // func entry : () -> u
    if (
        dt_is_integer(&func_sig->rtn_dt)
        && (DT_U == func_sig->rtn_dt.tag)
        && (narr_length(func_sig->param_dts) == 0)
    )
    {
        return ENTRY_FUNC_NO_ARGS_RTN_U;
    }

    // func entry : () -> s
    if (
        dt_is_integer(&func_sig->rtn_dt)
        && (DT_S == func_sig->rtn_dt.tag)
        && (narr_length(func_sig->param_dts) == 0)
    )
    {
        return ENTRY_FUNC_NO_ARGS_RTN_S;
    }

    // func entry : (argc : u, argv : **char) -> void
    if (
        dt_is_void(&func_sig->rtn_dt)
        && (narr_length(func_sig->param_dts) == 2)

        && dt_is_base(&func_sig->param_dts[0])
        && (DT_U == func_sig->param_dts[0].tag)

        && (dt_length_modifiers(&func_sig->param_dts[1]) == 2)
        && (DT_POINTER == func_sig->param_dts[1].tag)
        && (DT_POINTER == func_sig->param_dts[1].inner->tag)
        && (DT_ASCII == func_sig->param_dts[1].inner->inner->tag)
    )
    {
        return ENTRY_FUNC_ARGS_RTN_VOID;
    }

    // func entry : (argc : u, argv : **char) -> u
    if (
        dt_is_integer(&func_sig->rtn_dt)
        && (DT_U == func_sig->rtn_dt.tag)
        && (narr_length(func_sig->param_dts) == 2)

        && dt_is_base(&func_sig->param_dts[0])
        && (DT_U == func_sig->param_dts[0].tag)

        && (dt_length_modifiers(&func_sig->param_dts[1]) == 2)
        && (DT_POINTER == func_sig->param_dts[1].tag)
        && (DT_POINTER == func_sig->param_dts[1].inner->tag)
        && (DT_ASCII == func_sig->param_dts[1].inner->inner->tag)
    )
    {
        return ENTRY_FUNC_ARGS_RTN_U;
    }

    // func entry : (argc : u, argv : **char) -> s
    if (
        dt_is_integer(&func_sig->rtn_dt)
        && (DT_S == func_sig->rtn_dt.tag)
        && (narr_length(func_sig->param_dts) == 2)

        && dt_is_base(&func_sig->param_dts[0])
        && (DT_U == func_sig->param_dts[0].tag)

        && (dt_length_modifiers(&func_sig->param_dts[1]) == 2)
        && (DT_POINTER == func_sig->param_dts[1].tag)
        && (DT_POINTER == func_sig->param_dts[1].inner->tag)
        && (DT_ASCII == func_sig->param_dts[1].inner->inner->tag)
    )
    {
        return ENTRY_FUNC_ARGS_RTN_S;
    }

    return INVALID_ENTRY_FUNC;
}

//============================================================================//
//      MODULE                                                                //
//============================================================================//
INIT_FINI_COUNTER_SOURCE(module)

void module_init(struct module* module)
{
    nstr_init(&module->src_path);
    nstr_init(&module->out_path);
    nstr_init(&module->src);
    module->tokens = narr_alloc(0, sizeof(struct token));
    scope_init(&module->global_scope, GLOBAL_SCOPE_PARENT_PTR);
    module->exports = narr_alloc(0, sizeof(struct id));
    module->uid_counter = 0;
}

void module_fini(struct module* module)
{
    nstr_fini(&module->src_path);
    nstr_fini(&module->out_path);
    nstr_fini(&module->src);
    narr_free(module->tokens);
    scope_fini(&module->global_scope);
    narr_free(module->exports);
}

uint32_t module_generate_uid(struct module* module)
{
    return module->uid_counter++;
}

void module_add_latest_global_symbol_to_exports(struct module* module)
{
    struct symbol* const latest_symbol =
        module->global_scope.symbol_table->most_recent_symbol;
    module->exports = narr_push(module->exports, latest_symbol->id);
}

bool is_import(
    struct module* module,
    char const* symstr,
    size_t symstr_length
)
{
    struct symbol* const export =
        symbol_table_find(module->global_scope.symbol_table, symstr, symstr_length);

    nassert(NULL != export);
    nassert(NULL != export->srctok);
    nassert(NULL != export->srctok->module);

    nstr_t const* const cur_src_path = &module->src_path;
    nstr_t const* const export_src_path = &export->srctok->module->src_path;
    return nstr_ne(cur_src_path, export_src_path);
}

stbool resolve_module_path(nstr_t* path, char const* relto)
{
    // If the path starts with '.' the path contains a "." or ".." at its start,
    // signifying that the module is not coming from the include path. In this
    // case we'll skip over searching the include path.
    if ('.' != path->data[0])
    {
        nlogf(
            LOG_DEBUG,
            "Resolving module path '%s' relative to include path.",
            path->data
        );
        const char *ncid = getenv("NCID");
        nassert(ncid && *ncid);
        nstr_t nstr_defer_fini path_include;
        nstr_init_cstr(&path_include, ncid);
        nstr_cat_fmt(&path_include, "/%s", path->data);
        if (0 == access(path_include.data, R_OK))
        {
            // File exists in the include path. Use path relative to the include
            // path rather than relative to relto.
            nstr_assign_nstr(path, &path_include);
            nlogf(LOG_DEBUG, "Resolved path as '%s'.", path->data);
            return STBOOL_SUCCESS;
        }
    }

    nlogf(
        LOG_DEBUG,
        "Resolving module path '%s' relative to '%s'.",
        path->data,
        relto
    );

    char relto_dirname_buf[4096] = {0};
    cstr_cpy(relto_dirname_buf, relto);
    char* const relto_dirname = dirname(relto_dirname_buf);

    nstr_t nstr_defer_fini path_rel;
    nstr_init_cstr(&path_rel, relto_dirname);
    nstr_cat_fmt(&path_rel, "/%s", path->data);
    if (0 == access(path_rel.data, R_OK))
    {
        nstr_assign_nstr(path, &path_rel);
        nlogf(LOG_DEBUG, "Resolved path as '%s'.", path->data);
        return STBOOL_SUCCESS;
    }

    return STBOOL_FAILURE;
}

void module_cache_init(void)
{
    nlogf(LOG_DEBUG, "Init module cache.");

    memset(module_cache, 0, sizeof(module_cache));
    module_cache_len = 0;
    current_module_idx = 0;
}

void module_cache_fini(void)
{
    nlogf(LOG_DEBUG, "Fini module cache (%zu modules).", module_cache_len);

    size_t save_current_module_idx = current_module_idx;
    for (size_t i = 0; i < module_cache_len; ++i)
    {
        nlogf(LOG_DEBUG, INDENT_S "Fini module %zu in module cache.", i);
        current_module_idx = i;
        module_fini(&module_cache[i]);
    }
    current_module_idx = save_current_module_idx;
}

size_t module_cache_len;
size_t current_module_idx;
