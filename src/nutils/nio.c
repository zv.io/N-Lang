/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nutils/nio.h>

int log_config = DEFAULT_LOG_CONFIG;
FILE *const * log_stream = DEFAULT_LOG_STREAM;

static pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;

static void _log(
    char const* logstr,
    char const* fmt,
    va_list args
)
{
    pthread_mutex_lock(&log_mutex);
    fprintf(*log_stream, "%s", logstr);
    vfprintf(*log_stream, fmt, args);
    fputc('\n', *log_stream);
    fflush(*log_stream);
    pthread_mutex_unlock(&log_mutex);
}

static void _log_flc(
    char const* path,
    size_t line,
    size_t col,
    char const* logstr,
    char const* fmt,
    va_list args
)
{
    pthread_mutex_lock(&log_mutex);
    fprintf(*log_stream, "%s:%zu:%zu %s", path, line, col, logstr);
    vfprintf(*log_stream, fmt, args);
    fputc('\n', *log_stream);
    fflush(*log_stream);
    pthread_mutex_unlock(&log_mutex);
}

#define STR_LOG_DEBUG   "debug: "
#define STR_LOG_METRIC  "metric: "
#define STR_LOG_INFO    "info: "
#define STR_LOG_WARNING "warning: "
#define STR_LOG_ERROR   "error: "
#define STR_LOG_FATAL   "fatal: "

#define STR_LOG_DEBUG_COLOR \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_YELLOW STR_LOG_DEBUG ANSI_ESC_DEFAULT
#define STR_LOG_DEBUG_COLOR_IF_TTY \
    (isatty(fileno(*log_stream)) ? STR_LOG_DEBUG_COLOR : STR_LOG_DEBUG)

#define STR_LOG_METRIC_COLOR \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_CYAN STR_LOG_METRIC ANSI_ESC_DEFAULT
#define STR_LOG_METRIC_COLOR_IF_TTY \
    (isatty(fileno(*log_stream)) ? STR_LOG_METRIC_COLOR : STR_LOG_METRIC)

#define STR_LOG_INFO_COLOR \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_BLUE STR_LOG_INFO ANSI_ESC_DEFAULT
#define STR_LOG_INFO_COLOR_IF_TTY \
    (isatty(fileno(*log_stream)) ? STR_LOG_INFO_COLOR : STR_LOG_INFO)

#define STR_LOG_WARNING_COLOR \
    ANSI_ESC_BOLD ANSI_ESC_COLOR_MAGENTA STR_LOG_WARNING ANSI_ESC_DEFAULT
#define STR_LOG_WARNING_COLOR_IF_TTY \
    (isatty(fileno(*log_stream)) ? STR_LOG_WARNING_COLOR : STR_LOG_WARNING)

#define STR_LOG_ERROR_COLOR \
    ANSI_ESC_BOLD ASNI_ESC_COLOR_RED STR_LOG_ERROR ANSI_ESC_DEFAULT
#define STR_LOG_ERROR_COLOR_IF_TTY \
    (isatty(fileno(*log_stream)) ? STR_LOG_ERROR_COLOR : STR_LOG_ERROR)

#define STR_LOG_FATAL_COLOR \
    ANSI_ESC_BOLD ASNI_ESC_COLOR_RED STR_LOG_FATAL ANSI_ESC_DEFAULT
#define STR_LOG_FATAL_COLOR_IF_TTY \
    (isatty(fileno(*log_stream)) ? STR_LOG_FATAL_COLOR : STR_LOG_FATAL)

void nlogf(int level, char const* fmt, ...)
{
    int level_with_config_mask = level & log_config;
    if (level_with_config_mask == 0x00) // Log level not enabled.
    {
        return;
    }

    va_list args;
    va_start(args, fmt);
    switch(level_with_config_mask)
    {
    case LOG_DEBUG:
        _log(STR_LOG_DEBUG_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_METRIC:
        _log(STR_LOG_METRIC_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_INFO:
        _log(STR_LOG_INFO_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_WARNING:
        _log(STR_LOG_WARNING_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_ERROR:
        _log(STR_LOG_ERROR_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_FATAL:
        _log(STR_LOG_FATAL_COLOR_IF_TTY, fmt, args);
        break;
    }
    va_end(args);
}

void nlogf_flc(
    int level,
    char const* path,
    size_t line,
    size_t col,
    char const* fmt,
    ...
)
{
    int level_with_config_mask = level & log_config;
    if (level_with_config_mask == 0x00) // Log level not enabled.
    {
        return;
    }

    va_list args;
    va_start(args, fmt);
    switch(level_with_config_mask)
    {
    case LOG_DEBUG:
        _log_flc(path, line, col, STR_LOG_DEBUG_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_METRIC:
        _log_flc(path, line, col, STR_LOG_METRIC_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_INFO:
        _log_flc(path, line, col, STR_LOG_INFO_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_WARNING:
        _log_flc(path, line, col, STR_LOG_WARNING_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_ERROR:
        _log_flc(path, line, col, STR_LOG_ERROR_COLOR_IF_TTY, fmt, args);
        break;
    case LOG_FATAL:
        _log_flc(path, line, col, STR_LOG_FATAL_COLOR_IF_TTY, fmt, args);
        break;
    }
    va_end(args);
}

bool file_exist(char const* path)
{
    return access(path, F_OK) == 0;
}

ssize_t file_size(char const* path)
{
    struct stat st;
    if (stat(path, &st) != 0)
    {
        return -1;
    }
    return (ssize_t)st.st_size;
}

ssize_t file_read_into_buff(char const* path, char* buff, size_t buffsize)
{
    ssize_t fsize = file_size(path);
    if (fsize == -1 || ((size_t)fsize > buffsize-1))
    {
        return -1;
    }
    FILE* fp = fopen(path, "rb");
    if (!fp)
    {
        fclose(fp);
        return -1;
    }
    if (fread(buff, sizeof(char), (size_t)fsize, fp) != (size_t)fsize)
    {
        fclose(fp);
        return -1;
    }
    buff[fsize] = '\0';
    fclose(fp);
    return fsize;
}

ssize_t file_read_into_nstr(char const* path, nstr_t* nstr)
{
    ssize_t fsize = file_size(path);
    if (fsize == -1)
    {
        return -1;
    }
    FILE* fp = fopen(path, "rb");
    if (!fp)
    {
        return -1;
    }
    nstr_resize(nstr, (size_t)fsize);
    if (fread(nstr->data, sizeof(char), (size_t)fsize, fp) != (size_t)fsize)
    {
        fclose(fp);
        return -1;
    }
    nstr->data[fsize] = '\0';
    fclose(fp);
    return fsize;
}

ssize_t fmt_len(char const* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    ssize_t fmtted_strlen = vsnprintf(NULL, 0, fmt, args);
    if (fmtted_strlen == -1)
    {
        va_end(args);
        return -1;
    }
    va_end(args);
    return fmtted_strlen;
}
