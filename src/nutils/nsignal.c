/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nutils/nsignal.h>
#include <nutils/npanic.h>

void generic_sighandler(int signum)
{
    // It is perfectly reasonable for these signals to be raised during normal
    // program execution. Since all of them are requesting program termination,
    // we'll just kill the program wherever it is. In the future it may be worth
    // looking into doing some basic cleanup of open file pointers and whatnot,
    // but for now having the host OS take care of resource cleanup on program
    // exit is more than adequate.
    if (signum == SIGTERM)
    {
        fprintf(stderr, "SIGTERM received\n");
        exit(EXIT_FAILURE);
    }
    if (signum == SIGINT)
    {
        fprintf(stderr, "keyboard interrupt\n");
        exit(EXIT_FAILURE);
    }
    if (signum == SIGQUIT)
    {
        fprintf(stderr, "keyboard quit\n");
        exit(EXIT_FAILURE);
    }

    // If SIGHUP is raised the user most likely closed their terminal window,
    // which is indeed abnormal program termination. Similarly a segfault should
    // be treated as abnormal program termination worthy of a panic.
    if (signum == SIGHUP)
    {
        npanic("terminal hangup");
    }
    if (signum == SIGSEGV)
    {
        npanic("segmentation fault");
    }

    // The rest of the signals are uncommon enough that handling them with a
    // generic panic is probably fine.
    npanic("unhandled signal (signum = %d)", signum);
}

void install_generic_sighandler(void)
{
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = generic_sighandler;
/**
 * TODO: remove all the custom signal handling stuff unless it
 * it is absolutely necessary
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGQUIT, &sa, NULL);

    sigaction(SIGHUP, &sa, NULL);
    sigaction(SIGSEGV, &sa, NULL);

    sigaction(SIGILL, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
    sigaction(SIGFPE, &sa, NULL);
    sigaction(SIGBUS, &sa, NULL);
    sigaction(SIGPIPE, &sa, NULL);
*/
}
