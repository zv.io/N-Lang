/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nutils/narray.h>
#include <nutils/nmemory.h>

struct _narray
{
    size_t _length;
    size_t _capacity;
    size_t _elemsize;
    alignas(alignof(max_align_t)) char _data[];
};

#define _NARR_CAPACITY_MIN    ((size_t)10)
#define _NARR_CAPACITY_FACTOR ((double)1.3)
#define _NARR_NEW_CAPACITY_FROM_LENGTH(len) ((len) < _NARR_CAPACITY_MIN ? \
    _NARR_CAPACITY_MIN : ((size_t)(((double) len)*_NARR_CAPACITY_FACTOR)))
#define _NARR_TOTAL_SIZE(capacity, elsize) \
    (sizeof(struct _narray) + ((capacity)*(elsize)))

#define _NARR_P_HEAD_FROM_HANDLE(narr_hndl) \
    (((char*)narr_hndl) - sizeof(struct _narray))
#define _NARR_P_LENGTH_FROM_HANDLE(narr_hndl) \
    ((size_t*)(_NARR_P_HEAD_FROM_HANDLE(narr_hndl) + offsetof(struct _narray, _length)))
#define _NARR_P_CAPACITY_FROM_HANDLE(narr_hndl) \
    ((size_t*)(_NARR_P_HEAD_FROM_HANDLE(narr_hndl) + offsetof(struct _narray, _capacity)))
#define _NARR_P_ELEMSIZE_FROM_HANDLE(narr_hndl) \
    ((size_t*)(_NARR_P_HEAD_FROM_HANDLE(narr_hndl) + offsetof(struct _narray, _elemsize)))

void* narr_alloc(size_t numelem, size_t elemsize)
{
    size_t const capacity = _NARR_NEW_CAPACITY_FROM_LENGTH(numelem);
    struct _narray* const mem = nalloc(_NARR_TOTAL_SIZE(capacity, elemsize));
    mem->_length = numelem;
    mem->_capacity = capacity;
    mem->_elemsize = elemsize;
    return mem->_data;
}

void narr_free(void* narr)
{
    nfree(_NARR_P_HEAD_FROM_HANDLE(narr));
}

size_t narr_length(void const* narr)
{
    return *_NARR_P_LENGTH_FROM_HANDLE(narr);
}

size_t narr_capacity(void const* narr)
{
    return *_NARR_P_CAPACITY_FROM_HANDLE(narr);
}

size_t narr_sizeof_elem(void const* narr)
{
    return *_NARR_P_ELEMSIZE_FROM_HANDLE(narr);
}

void* narr_reserve(void* narr, size_t numelem)
{
    size_t const min_capacity = narr_length(narr) + numelem;
    size_t const curr_capacity = narr_capacity(narr);
    if (curr_capacity >= min_capacity)
    {
        // Narray with capacity min_capacity can already fit within the
        // currently allocated memory block.
        return narr;
    }
    // new_capacity is large enough to hold the required space plus extra in
    // case of additional expansion.
    size_t const new_capacity = _NARR_NEW_CAPACITY_FROM_LENGTH(min_capacity);
    struct _narray* const new_mem = nrealloc(
        _NARR_P_HEAD_FROM_HANDLE(narr),
        _NARR_TOTAL_SIZE(new_capacity, narr_sizeof_elem(narr))
    );
    new_mem->_capacity = new_capacity;
    return new_mem->_data;
}

void* narr_resize(void* narr, size_t len)
{
    if (narr_length(narr) < len)
    {
        narr = narr_reserve(narr, len - narr_length(narr));
    }
    *_NARR_P_LENGTH_FROM_HANDLE(narr) = len;
    return narr;
}

void* narr_push(void* narr, void const* ptr_to_val)
{
    narr = narr_reserve(narr, 1);
    memcpy(
        (char*)narr + narr_length(narr)*narr_sizeof_elem(narr),
        ptr_to_val,
        narr_sizeof_elem(narr)
    );
    *_NARR_P_LENGTH_FROM_HANDLE(narr) += 1;
    return narr;
}

void narr_pop(void* narr)
{
    *_NARR_P_LENGTH_FROM_HANDLE(narr) -= 1;
}

void* narr_unshift(void* narr, void const* ptr_to_val)
{
    narr = narr_reserve(narr, 1);
    memmove(
        (char*)narr + narr_sizeof_elem(narr),
        (char*)narr,
        narr_length(narr)*narr_sizeof_elem(narr)
    );
    memcpy(
        narr,
        ptr_to_val,
        narr_sizeof_elem(narr)
    );
    *_NARR_P_LENGTH_FROM_HANDLE(narr) += 1;
    return narr;
}

void narr_shift(void* narr)
{
    memmove(
        (char*)narr,
        (char*)narr + narr_sizeof_elem(narr),
        (narr_length(narr)-1)*narr_sizeof_elem(narr)
    );
    *_NARR_P_LENGTH_FROM_HANDLE(narr) -= 1;
}

void* narr_front(void const* narr)
{
    if (0 == narr_length(narr))
    {
        return NULL;
    }
    return (char*)narr;
}

void* narr_back(void const* narr)
{
    if (0 == narr_length(narr))
    {
        return NULL;
    }
    return (char*)narr + (narr_length(narr)-1)*narr_sizeof_elem(narr);
}
