/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nutils/nmetrics.h>
#include <nutils/nio.h>
#include <nutils/npanic.h>

//============================================================================//
//      INIT FINI COUNTER                                                     //
//============================================================================//
void _verify_balanced_init_fini(
    char const* type_name,
    unsigned init_numcalls,
    unsigned fini_numcalls
)
{
    if (init_numcalls != fini_numcalls)
    {
        nlogf(
            LOG_ERROR,
            "Unbalanced calls to init and fini functions for data type '%s'.\n"
                INDENT_S "init = %u\n"
                INDENT_S "fini = %u",
            type_name,
            init_numcalls,
            fini_numcalls
        );
    }
}

//============================================================================//
//      HIGH PRECISION TIMING                                                 //
//============================================================================//
void ngettime(struct timespec* ts)
{
    if (UNLIKELY(-1 == clock_gettime(CLOCK_MONOTONIC, ts)))
    {
       npanic("clock_gettime failure");
    }
}

#define MS_PER_SEC 1000
#define NS_PER_MS  1000000
#define SEC_TO_MS(_sec) (((long long)(_sec)) * MS_PER_SEC)
#define NS_TO_MS(_ns)   (((long long)(_ns))  / NS_PER_MS)
long long nelapsed_ms(struct timespec start, struct timespec stop)
{
    return (SEC_TO_MS(stop.tv_sec) + NS_TO_MS(stop.tv_nsec))
        - (SEC_TO_MS(start.tv_sec) + NS_TO_MS(start.tv_nsec));
}
