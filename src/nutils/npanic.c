/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nutils/npanic.h>
#include <execinfo.h>

#define MAX_BACKTRACE 512

void npanic(char const* fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    fprintf(stderr, "panic: ");
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    fflush(stderr);
    va_end(args);

    void* backtrace_buff[MAX_BACKTRACE];
    int num_backtrace_frames = backtrace(backtrace_buff, MAX_BACKTRACE);
    char** symbols = backtrace_symbols(backtrace_buff, num_backtrace_frames);

    if (NULL == symbols)
    {
        fprintf(stderr, "A failure occurred in `backtrace_symbols`.\n");
        fprintf(stderr, "Printing backtrace using `backtrace_symbols_fd`.\n");
        backtrace_symbols_fd(
            backtrace_buff,
            num_backtrace_frames,
            STDERR_FILENO
        );
        goto do_exit;
    }

    for (int i = 0; i < num_backtrace_frames; ++i)
    {
        fprintf(stderr, "%s\n", symbols[i]);
    }

do_exit:
    fflush(stderr);
    exit(EXIT_FAILURE);
}
