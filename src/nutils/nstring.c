/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <nutils/nstring.h>
#include <nutils/nassert.h>
#include <nutils/nmemory.h>

//============================================================================//
//      CSTRING                                                               //
//============================================================================//
int cstr_cmp(char const* lhs, char const* rhs)
{
    return strcmp(lhs, rhs);
}

bool cstr_eq(char const* lhs, char const* rhs)
{
    return cstr_cmp(lhs, rhs) == 0;
}

bool cstr_ne(char const* lhs, char const* rhs)
{
    return cstr_cmp(lhs, rhs) != 0;
}

int cstr_n_cmp(char const* lhs, char const* rhs, size_t n)
{
    return strncmp(lhs, rhs, n);
}

bool cstr_n_eq(char const* lhs, char const* rhs, size_t n)
{
    return cstr_n_cmp(lhs, rhs, n) == 0;
}

bool cstr_n_ne(char const* lhs, char const* rhs, size_t n)
{
    return cstr_n_cmp(lhs, rhs, n) != 0;
}

int cstr_cmp_case(char const* lhs, char const* rhs)
{
    while (tolower(*lhs) == tolower(*rhs) && *lhs != '\0')
    {
        ++lhs;
        ++rhs;
    }
    return tolower(*lhs) - tolower(*rhs);
}

bool cstr_eq_case(char const* lhs, char const* rhs)
{
    return cstr_cmp_case(lhs, rhs) == 0;
}

bool cstr_ne_case(char const* lhs, char const* rhs)
{
    return cstr_cmp_case(lhs, rhs) != 0;
}

int cstr_n_cmp_case(char const* lhs, char const* rhs, size_t n)
{
    if (n == 0)
    {
        return 0;
    }
    while (--n && tolower(*lhs) == tolower(*rhs) && *lhs != '\0')
    {
        ++lhs;
        ++rhs;
    }
    return tolower(*lhs) - tolower(*rhs);
}

bool cstr_n_eq_case(char const* lhs, char const* rhs, size_t n)
{
    return cstr_n_cmp_case(lhs, rhs, n) == 0;
}

bool cstr_n_ne_case(char const* lhs, char const* rhs, size_t n)
{
    return cstr_n_cmp_case(lhs, rhs, n) != 0;
}

size_t cstr_len(char const* str)
{
    return strlen(str);
}

bool cstr_len_ge(char const* str, size_t n)
{
    while (n--)
    {
        if (*str++ == '\0')
        {
            return false;
        }
    }
    return true;
}

void cstr_cat(char* dest, char const* src)
{
    strcat(dest, src);
}

void cstr_n_cat(char* dest, char const* src, size_t n)
{
    strncat(dest, src, n);
}

void cstr_cpy(char* dest, char const* src)
{
    strcpy(dest, src);
}

void cstr_n_cpy(char* dest, char const* src, size_t n)
{
    strncpy(dest, src, n);
}

//============================================================================//
//      NSTRING                                                               //
//============================================================================//
#define _NSTR_CAPACITY_MIN    ((size_t)10)
#define _NSTR_CAPACITY_FACTOR ((double)1.3)
#define _NSTR_NEW_CAPACITY_FROM_LEN(len)                                       \
(                                                                              \
    (len) < _NSTR_CAPACITY_MIN ?                                               \
        _NSTR_CAPACITY_MIN                                                     \
        :                                                                      \
        ((size_t)(((double) len)*_NSTR_CAPACITY_FACTOR))                       \
)
#define _NSTR_DATA_SIZE_FROM_CAPACITY(capacity) \
    (capacity + NULL_TERMINATOR_LEN)

#define _NSTR_DFLT_INIT_LEN      (0)
#define _NSTR_DFLT_INIT_CAPACITY (_NSTR_NEW_CAPACITY_FROM_LEN(_NSTR_DFLT_INIT_LEN))

// Used by nstr_assign_fmt and nstr_cat_fmt.
static void nstr_cat_vfmt(nstr_t* dest, char const* fmt, va_list args);

void nstr_init(nstr_t* nstr)
{
    nstr->data = nalloc(
        _NSTR_DATA_SIZE_FROM_CAPACITY(_NSTR_DFLT_INIT_CAPACITY)
    );
    nstr->len = _NSTR_DFLT_INIT_LEN;
    nstr->capacity = _NSTR_DFLT_INIT_CAPACITY;
    (nstr->data)[0] = '\0';
}

void nstr_init_cstr(nstr_t* nstr, char const* other)
{
    size_t const other_len = cstr_len(other);
    size_t const capacity = _NSTR_NEW_CAPACITY_FROM_LEN(other_len);

    nstr->data = nalloc(_NSTR_DATA_SIZE_FROM_CAPACITY(capacity));
    cstr_cpy(nstr->data, other);

    nstr->len = other_len;
    nstr->capacity = capacity;
}

void nstr_init_nstr(nstr_t* nstr, nstr_t const* other)
{
    size_t const other_len = other->len;
    size_t const capacity = _NSTR_NEW_CAPACITY_FROM_LEN(other_len);

    nstr->data = nalloc(_NSTR_DATA_SIZE_FROM_CAPACITY(capacity));
    memcpy(nstr->data, other->data, other_len + NULL_TERMINATOR_LEN);

    nstr->len = other_len;
    nstr->capacity = capacity;
}

void nstr_init_fmt(nstr_t* nstr, char const* fmt, ...)
{
    nstr_init(nstr); // Init to empty string.

    va_list args;
    va_start(args, fmt);
    nstr_cat_vfmt(nstr, fmt, args);
    va_end(args);
}

void nstr_fini(nstr_t* nstr)
{
    nfree(nstr->data);
    nstr->data = NULL;
}

void nstr_resize(nstr_t* nstr, size_t len)
{
    if (nstr->len < len)
    {
        nstr_reserve(nstr, len - (nstr->len));
    }
    nstr->len = len;
    // Always set the end byte to null to prevent cstr overflows.
    (nstr->data)[nstr->len] = '\0';
}

void nstr_reserve(nstr_t* nstr, size_t nchars)
{
    size_t const min_capacity = nstr->len + nchars + NULL_TERMINATOR_LEN;
    size_t const curr_capacity = nstr->capacity;
    if (curr_capacity >= min_capacity)
    {
        return;
    }

    size_t new_capacity =
            (size_t)((double)min_capacity * _NSTR_CAPACITY_FACTOR);
    nstr->data = nrealloc(
        nstr->data,
        _NSTR_DATA_SIZE_FROM_CAPACITY(new_capacity)
    );
    nstr->capacity = new_capacity;
}

void nstr_clear(nstr_t* nstr)
{
    nstr_resize(nstr, 0);
}

void nstr_assign_cstr(nstr_t* nstr, char const* other)
{
    nstr->len = 0;
    nstr_cat_cstr(nstr, other);
}

void nstr_assign_nstr(nstr_t* nstr, nstr_t const* other)
{
    nstr->len = 0;
    nstr_cat_nstr(nstr, other);
}

void nstr_assign_fmt(nstr_t* nstr, char const* fmt, ...)
{
    nstr->len = 0;
    va_list args;
    va_start(args, fmt);
    nstr_cat_vfmt(nstr, fmt, args);
    va_end(args);
}

int nstr_cmp(nstr_t const* lhs, nstr_t const* rhs)
{
    return cstr_cmp(lhs->data, rhs->data);
}

bool nstr_eq(nstr_t const* lhs, nstr_t const* rhs)
{
    return nstr_cmp(lhs, rhs) == 0;
}

bool nstr_ne(nstr_t const* lhs, nstr_t const* rhs)
{
    return nstr_cmp(lhs, rhs) != 0;
}

int nstr_cmp_case(nstr_t const* lhs, nstr_t const* rhs)
{
    return cstr_cmp_case(lhs->data, rhs->data);
}

bool nstr_eq_case(nstr_t const* lhs, nstr_t const* rhs)
{
    return nstr_cmp_case(lhs, rhs) == 0;
}

bool nstr_ne_case(nstr_t const* lhs, nstr_t const* rhs)
{
    return nstr_cmp_case(lhs, rhs) != 0;
}

void nstr_cat_cstr(nstr_t* dest, char const* src)
{
    size_t const dest_len = dest->len;
    size_t const src_len = cstr_len(src);
    nstr_reserve(dest, src_len);
    memcpy(
        dest->data + dest_len,
        src,
        src_len + NULL_TERMINATOR_LEN
    );
    dest->len += src_len;
}

void nstr_cat_nstr(nstr_t* dest, nstr_t const* src)
{
    size_t const dest_len = dest->len;
    size_t const src_len = src->len;
    nstr_reserve(dest, src_len);
    memcpy(
        dest->data + dest_len,
        src->data,
        src_len + NULL_TERMINATOR_LEN
    );
    dest->len += src_len;
}

void nstr_cat_fmt(nstr_t* dest, char const* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    nstr_cat_vfmt(dest, fmt, args);
    va_end(args);
}

static void nstr_cat_vfmt(nstr_t* dest, char const* fmt, va_list args)
{
    va_list copy;
    va_copy(copy, args);
    size_t const fmt_len = (size_t)vsnprintf(NULL, 0, fmt, copy);
    va_end(copy);

    // A call to vsprintf that uses the destination buffer for the format string
    // or as a format parameter causes undefined behavior. A temporary buffer
    // is written to for the vsprintf call to avoid the UB. After the write, the
    // temporary is copied to the destination string.
    char* tmp = alloca(fmt_len + NULL_TERMINATOR_LEN);
    vsprintf(tmp, fmt, args);

    size_t pre_resize_len = dest->len;
    nstr_resize(dest, pre_resize_len + fmt_len);
    memcpy(dest->data + pre_resize_len, tmp, fmt_len);
}
