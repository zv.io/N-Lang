OBJS = $(patsubst %.c,%.o,$(wildcard *.c))

all: ${OBJS}

%.o: %.c
	$(CC) -c $(CFLAGS) $(LDFLAGS) $(EXTRAS) -o $@ $<

clean:
	@rm -fv $(wildcard *.o)
