= THE N PROGRAMMING LANGUAGE

== Introduction
**TODO**

== Building
=== POSIX

----
$ make
$ make install
----

Check that the nc compiler was correctly installed by running `nc --version`.

=== Docker

To use the N compiler, install the following:

. https://www.docker.com/community-edition/[Docker Community Edition]
. https://docs.docker.com/compose/[Docker Compose]

Then build and spin up the container as follows:

----
$ cd N-Lang
$ docker-compose build
$ docker-compose up
$ docker-compose run container
container$ cd examples
container$ nc hello_world.n
container$ ./a.out
Hello, world!
----

== Windows

Currently the compiler and tooling are only targeting POSIX compatible systems.

Use of the compiler and tooling on Windows requires Docker for the time being.

== License
Apache 2.0
