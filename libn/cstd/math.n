# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/primitives.n"a;

export extern func ceilf : (val : mut f32) -> mut f32;
export extern func ceil : (val : mut f64) -> mut f64;

export extern func floorf : (val : mut f32) -> mut f32;
export extern func floor : (val : mut f64) -> mut f64;

export extern func expf : (val : mut f32) -> mut f32;
export extern func exp : (val : mut f64) -> mut f64;

export extern func sinf : (val : mut f32) -> mut f32;
export extern func sin : (val : mut f64) -> mut f64;

export extern func cosf : (val : mut f32) -> mut f32;
export extern func cos : (val : mut f64) -> mut f64;

export extern func asinf : (val : mut f32) -> mut f32;
export extern func asin : (val : mut f64) -> mut f64;

export extern func atanf : (val : mut f32) -> mut f32;
export extern func atan : (val : mut f64) -> mut f64;

export extern func logf : (val : mut f32) -> mut f32;
export extern func log : (val : mut f64) -> mut f64;

export extern func log2f : (val : mut f32) -> mut f32;
export extern func log2 : (val : mut f64) -> mut f64;

export extern func log10f : (val : mut f32) -> mut f32;
export extern func log10 : (val : mut f64) -> mut f64;
