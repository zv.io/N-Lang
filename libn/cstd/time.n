# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/primitives.n"a;
# Taken from
#   userspace/glibc/sysdeps/unix/sysv/linux/x86/bits/typesizes.h
# and
#   gcc/include/bits/types.h
# time_t               --> __time_t
# __TIME_T_TYPE        --> __SYSCALL_SLONG_TYPE
# __SYSCALL_SLONG_TYPE --> long int
alias time_t = slong;

export extern func time : (timer : mut ^mut time_t) -> time_t;
