# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/primitives.n"a;

export extern func memcpy : (dest : mut ^mut void, src : mut ^void, n : mut size_t) -> mut ^mut void;
export extern func memmov : (dest : mut ^mut void, src : mut ^void, n : mut size_t) -> mut ^mut void;
export extern func memcmp : (ptr1 : mut ^void, ptr2 : mut ^void, n : mut size_t) -> mut sint;
export extern func memset : (ptr : mut ^mut void, value : mut sint, n : mut size_t) -> mut ^mut void;

export extern func strcpy  : (dest : mut ^mut ascii, src : mut ^ascii) -> mut ^mut ascii;
export extern func strncpy : (dest : mut ^mut ascii, src : mut ^ascii, n : mut size_t) -> mut ^mut ascii;

export extern func strcmp  : (str1 : mut ^ascii, str2 : mut ^ascii) -> mut sint;
export extern func strncmp : (str1 : mut ^ascii, str2 : mut ^ascii, n : mut size_t) -> mut sint;

export extern func strlen : (str : mut ^ascii) -> mut size_t;
