/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __N_CORE__
#define __N_CORE__

typedef unsigned char      n_u8;
typedef   signed char      n_s8;
typedef unsigned short     n_u16;
typedef   signed short     n_s16;
typedef unsigned int       n_u32;
typedef   signed int       n_s32;
typedef unsigned long long n_u64;
typedef   signed long long n_s64;

/* u and s types */
#if defined(__UINTPTR_TYPE__) && defined (__INTPTR_TYPE__)
    typedef __UINTPTR_TYPE__ n_u;
    typedef __INTPTR_TYPE__  n_s;
#elif defined(_M_X32)
    typedef n_u32 n_u;
    typedef n_s32 n_s;
#elif defined(_M_X64)
    typedef n_u64 n_u;
    typedef n_s64 n_s;
#else
#   error "No definition for u and s types."
#endif // u and s types

typedef float  n_f32;
typedef double n_f64;

typedef n_u8 n_bool;
#define n_true  1
#define n_false 0

typedef char n_ascii;

#define n_null ((void *)0)

#endif // __N_CORE__
