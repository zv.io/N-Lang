# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/str.n"a;
import "std/panic.n"a;
import "std/format.n"a;

export struct string;

export func string_init : (this : ^mut string) -> void;
export func string_init_str : (this : ^mut string, other : ^ascii) -> void;
export func string_init_string : (this : ^mut string, other : ^string) -> void;
export func string_fini : (this : ^mut string) -> void;

export func string_assign_str : (this : ^mut string, other : ^ascii) -> void;
export func string_assign_string : (this : ^mut string, other : ^string) -> void;

export func string_data : (this : ^string) -> ^ascii;
export func string_length : (this : ^string) -> u;
export func string_capacity : (this : ^string) -> u;

export func string_is_empty : (this : ^string) -> bool;

export func string_at : (this : ^string, idx : u) -> ^mut ascii;
export func string_front : (this : ^string) -> ^mut ascii;
export func string_back : (this : ^string) -> ^mut ascii;
export func string_end : (this : ^string) -> ^ascii;

export func string_resize : (this : ^mut string, length : u) -> void;
export func string_reserve : (this : ^mut string, nasciis : u) -> void;
export func string_clear : (this : ^mut string) -> void;

export func string_prepend_ascii : (this : ^mut string, ch : ascii) -> void;
export func string_prepend_str : (this : ^mut string, str : ^ascii) -> void;
export func string_prepend_string : (this : ^mut string, str : ^string) -> void;

export func string_append_ascii : (this : ^mut string, ch : ascii) -> void;
export func string_append_str : (this : ^mut string, str : ^ascii) -> void;
export func string_append_string : (this : ^mut string, str : ^string) -> void;

export func string_transform_upper : (this : ^mut string) -> void;
export func string_transform_lower : (this : ^mut string) -> void;
export func string_trim_front : (this : ^mut string) -> void;
export func string_trim_back : (this : ^mut string) -> void;

export struct string
{
    let _data : mut ^mut ascii;
    let _length : mut u;
    let _capacity : mut u;

    func init            = string_init;
    func init_str        = string_init_str;
    func init_string     = string_init_string;
    func fini            = string_fini;

    func assign_str      = string_assign_str;
    func assign_string   = string_assign_string;

    func data            = string_data;
    func length          = string_length;
    func capacity        = string_capacity;

    func is_empty        = string_is_empty;

    func at              = string_at;
    func front           = string_front;
    func back            = string_back;
    func end             = string_end;

    func resize          = string_resize;
    func reserve         = string_reserve;
    func clear           = string_clear;

    func prepend_ascii   = string_prepend_ascii;
    func prepend_str     = string_prepend_str;
    func prepend_string  = string_prepend_string;

    func append_ascii    = string_append_ascii;
    func append_str      = string_append_str;
    func append_string   = string_append_string;

    func transform_upper = string_transform_upper;
    func transform_lower = string_transform_lower;
    func trim_front      = string_trim_front;
    func trim_back       = string_trim_back;
}

################################################################################
###                                 INTERNAL                                 ###
################################################################################

let STRING_CAPACITY_MIN : u = 10u;
let STRING_CAPACITY_FACTOR : f64 = 2.0f64;

let STRING_DEFAULT_INIT_LENGTH : u = 0u;
let STRING_DEFAULT_INIT_CAPACITY : u = 10u;

func new_capacity : (length : u) -> u
{
    if length < STRING_CAPACITY_MIN
    {
        return STRING_CAPACITY_MIN;
    }
    return (length as f64 * STRING_CAPACITY_FACTOR) as u;
}

func data_alloc_size : (capcity : u) -> u
{
    return capcity + sizeof('\0'a);
}

export func string_init : (this : ^mut string) -> void
{
    let data_size : u = data_alloc_size(STRING_DEFAULT_INIT_CAPACITY);

    this->_data = allocate(data_alloc_size(STRING_DEFAULT_INIT_CAPACITY));
    this->_length = STRING_DEFAULT_INIT_LENGTH;
    this->_capacity = STRING_DEFAULT_INIT_CAPACITY;
    this->_data[0u] = '\0'a;
}

export func string_init_str : (this : ^mut string, other : ^ascii) -> void
{
    let other_length : u = str_length(other);
    let capacity : u = new_capacity(other_length);
    let data_size : u = data_alloc_size(capacity);

    this->_data = allocate(data_size);
    str_copy(this->_data, other, data_size);

    this->_length = other_length;
    this->_capacity = capacity;
}

export func string_init_string : (this : ^mut string, other : ^string) -> void
{
    let other_length : u = other->_length;
    let capacity : u = new_capacity(other_length);
    let data_size : u = data_alloc_size(capacity);

    this->_data = allocate(data_size);
    str_copy(this->_data, other->_data, data_size);

    this->_length = other_length;
    this->_capacity = capacity;
}

export func string_fini : (this : ^mut string) -> void
{
    deallocate(this->_data);
}

export func string_assign_str : (this : ^mut string, other : ^ascii) -> void
{
    let other_length : u = str_length(other);
    this->resize(other_length);
    str_copy(this->_data, other, other_length + 1u);
}

export func string_assign_string : (this : ^mut string, other : ^string) -> void
{
    let other_length : u = other->_length;
    this->resize(other_length);
    str_copy(this->_data, other->_data, other_length + 1u);
}

export func string_data : (this : ^string) -> ^ascii
{
    return this->_data;
}

export func string_length : (this : ^string) -> u
{
    return this->_length;
}

export func string_capacity : (this : ^string) -> u
{
    return this->_capacity;
}

export func string_is_empty : (this : ^string) -> bool
{
    return this->length() == 0u;
}

export func string_at : (this : ^string, idx : u) -> ^mut ascii
{
    if idx >= this->_length
    {
        panic(PANIC_INVALID_INDEX);
    }
    return ?this->_data[idx];
}

export func string_front : (this : ^string) -> ^mut ascii
{
    return this->at(0u);
}

export func string_back : (this : ^string) -> ^mut ascii
{
    return this->at(this->length() - 1u);
}

export func string_end : (this : ^string) -> ^ascii
{
    return this->at(this->length());
}

export func string_resize : (this : ^mut string, length : u) -> void
{
    if this->_length < length
    {
        string_reserve(this, length - (this->_length));
    }
    this->_length = length;
    this->_data[this->_length] = '\0'a;
}

export func string_reserve : (this : ^mut string, nasciis : u) -> void
{
    let min_capacity : u = this->_length + nasciis + sizeof('\0'a);
    let curr_capacity : u = this->_capacity;

    if curr_capacity >= min_capacity{return;}

    let capacity : u = (min_capacity as f64 * STRING_CAPACITY_FACTOR) as u;
    let data_size : u = data_alloc_size(capacity);

    this->_data = reallocate(this->_data, data_size);
    this->_capacity = capacity;
}

export func string_clear : (this : ^mut string) -> void
{
    string_resize(this, 0u);
}

export func string_prepend_ascii : (this : ^mut string, ch : ascii) -> void
{
    let old_len : u = this->length();

    this->resize(old_len + 1u);
    memory_copy_overlapping(this->_data +^ 1u, this->_data, old_len);
    @this->front() = ch;
}

export func string_prepend_str : (this : ^mut string, str : ^ascii) -> void
{
    let old_len : u = this->length();
    let str_len : u = str_length(str);

    this->resize(old_len + str_len);
    memory_copy_overlapping(this->_data +^ str_len, this->_data, old_len);
    memory_copy(this->_data, str, str_len);
}

export func string_prepend_string : (this : ^mut string, str : ^string) -> void
{
    let old_len : u = this->length();
    let str_len : u = str->length();

    this->resize(old_len + str_len);
    memory_copy_overlapping(this->_data +^ str_len, this->_data, old_len);
    memory_copy(this->_data, str->data(), str_len);
}

export func string_append_ascii : (this : ^mut string, ch : ascii) -> void
{
    this->resize(this->length() + 1u);
    @this->back() = ch;
}

export func string_append_str : (this : ^mut string, str : ^ascii) -> void
{
    let old_len : u = this->length();
    let str_len : u = str_length(str);

    this->resize(old_len + str_len);
    memory_copy(this->_data +^ old_len, str, str_len);
}

export func string_append_string : (this : ^mut string, str : ^string) -> void
{
    let old_len : u = this->length();
    let str_len : u = str->_length;

    this->resize(old_len + str_len);
    memory_copy(this->_data +^ old_len, str->data(), str_len);
}

export func string_transform_upper : (this : ^mut string) -> void
{
    let ch : mut ^mut ascii = this->front();
    let end : ^ascii = this->end();
    loop ch != end
    {
        @ch = ascii_to_upper(@ch);
        ch = ch +^ 1u;
    }
}

export func string_transform_lower : (this : ^mut string) -> void
{
    let ch : mut ^mut ascii = this->front();
    let end : ^ascii = this->end();
    loop ch != end
    {
        @ch = ascii_to_lower(@ch);
        ch = ch +^ 1u;
    }
}

export func string_trim_front : (this : ^mut string) -> void
{
    let front : ^ascii = this->front();
    if !ascii_is_space(@front){return;}

    let ch : mut ^ascii = front;
    loop ascii_is_space(@ch){ch = ch +^ 1u;}
    let num_asciis : u = ch -^^ front;

    let new_length : u = this->length() - num_asciis;
    memory_copy_overlapping(this->_data, this->_data +^ num_asciis, new_length);
    this->resize(new_length);
}

export func string_trim_back : (this : ^mut string) -> void
{
    let back : ^ascii = this->back();
    if !ascii_is_space(@back){return;}

    let ch : mut ^ascii = back;
    loop ascii_is_space(@ch){ch = ch -^ 1u;}
    let num_asciis : u = back -^^ ch;

    let new_length : u = this->length() - num_asciis;
    this->resize(new_length);
}
