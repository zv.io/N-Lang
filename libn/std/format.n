# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "std/str.n"a;
import "std/integer.n"a;
import "std/math.n"a;
import "std/memory.n"a;

# Format strings follow the pattern:
#   {[type]}
#
# Type:
#   'b' : Integer (binary).
#   'd' : Integer (decimal).
#   'x' : Integer (hexadecimal with lower case digits and 0x).
#   'X' : Integer (hexadecimal with upper case digits and 0X).
#   'f' : Floting point (decimal notation).
#   'e' : Floting point (scientific notation with lower case e).
#   'E' : Floting point (scientific notation with upper case E).
#   'c' : Character.
#   's' : Str.
#   'p' : Pointer.

export let FORMAT_TYPE_INTEGER_BIN       : ascii = 'b'a;
export let FORMAT_TYPE_INTEGER_DEC       : ascii = 'd'a;
export let FORMAT_TYPE_INTEGER_HEX_LOWER : ascii = 'x'a;
export let FORMAT_TYPE_INTEGER_HEX_UPPER : ascii = 'X'a;
export let FORMAT_TYPE_FLOAT_DEC         : ascii = 'f'a;
export let FORMAT_TYPE_FLOAT_SCI_LOWER   : ascii = 'e'a;
export let FORMAT_TYPE_FLOAT_SCI_UPPER   : ascii = 'E'a;
export let FORMAT_TYPE_ASCII             : ascii = 'a'a;
export let FORMAT_TYPE_ASTR              : ascii = 's'a;
export let FORMAT_TYPE_POINTER           : ascii = 'p'a;

export struct format_specifier;
export func format_specifier_is_integer : (this : ^format_specifier) -> bool;
export func format_specifier_is_float : (this : ^format_specifier) -> bool;
export func format_specifier_parse : (
    this : ^mut format_specifier,
    parse_begin : ^ascii
) -> u;
export func format_specifier_write_u64 : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    value : u64
) -> u;
export func format_specifier_write_s64 : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    value : smax
) -> u;
export func format_specifier_write_ascii : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    ch : ascii
) -> u;
export func format_specifier_write_astr : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    str : ^ascii
) -> u;
export func format_specifier_write_astr_range : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    str_begin : ^ascii,
    str_end : ^ascii
) -> u;
export func format_specifier_write_pointer : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    ptr : ^void
) -> u;

struct format_specifier
{
    let format_type : mut ascii;

    func parse = format_specifier_parse;

    func is_integer = format_specifier_is_integer;
    func is_float   = format_specifier_is_float;

    func write_u64      = format_specifier_write_u64;
    func write_s64      = format_specifier_write_s64;
    func write_ascii     = format_specifier_write_ascii;
    func write_str       = format_specifier_write_astr;
    func write_str_range = format_specifier_write_astr_range;
    func write_pointer   = format_specifier_write_pointer;
}

export struct str_formatter;
export func str_formatter_assign : (
    this : ^mut str_formatter,
    write_buf : ^mut ascii,
    write_buf_length : u,
    format : ^ascii
) -> void;
export func str_formatter_update_buffer : (
    this : ^mut str_formatter,
    new_write_buf : ^mut ascii,
    new_write_buf_length : u,
) -> void;
export func str_formatter_next_specifier : (
    this : ^mut str_formatter
) -> ^format_specifier;
export func str_formatter_format_u8 : (
    this : ^mut str_formatter,
    value : u8
) -> void;
export func str_formatter_format_u16 : (
    this : ^mut str_formatter,
    value : u16
) -> void;
export func str_formatter_format_u32 : (
    this : ^mut str_formatter,
    value : u32
) -> void;
export func str_formatter_format_u64 : (
    this : ^mut str_formatter,
    value : u64
) -> void;
export func str_formatter_format_u : (
    this : ^mut str_formatter,
    value : u
) -> void;
export func str_formatter_format_s8 : (
    this : ^mut str_formatter,
    value : s8
) -> void;
export func str_formatter_format_s16 : (
    this : ^mut str_formatter,
    value : s16
) -> void;
export func str_formatter_format_s32 : (
    this : ^mut str_formatter,
    value : s32
) -> void;
export func str_formatter_format_s64 : (
    this : ^mut str_formatter,
    value : s64
) -> void;
export func str_formatter_format_s : (
    this : ^mut str_formatter,
    value : s
) -> void;
export func str_formatter_format_u64 : (
    this : ^mut str_formatter,
    value : u64
) -> void;
export func str_formatter_format_smax : (
    this : ^mut str_formatter,
    value : smax
) -> void;
export func str_formatter_format_ascii : (
    this : ^mut str_formatter,
    ch : ascii
) -> void;
export func str_formatter_format_astr : (
    this : ^mut str_formatter,
    str : ^ascii
) -> void;
export func str_formatter_format_astr_range : (
    this : ^mut str_formatter,
    str_begin : ^ascii,
    str_end : ^ascii
) -> void;
export func str_formatter_format_pointer : (
    this : ^mut str_formatter,
    ptr : ^void
) -> void;

export struct str_formatter
{
    let write_buf_begin : mut ^mut ascii;
    let write_buf_end : mut ^ascii;
    let format : mut ^ascii;

    let write_ptr : mut ^mut ascii;
    let format_ptr : mut ^ascii;

    let _has_next_specifier : mut bool;
    let _next_specifier : mut format_specifier;

    func assign = str_formatter_assign;

    func next_specifier = str_formatter_next_specifier;

    func format_u8         = str_formatter_format_u8;
    func format_u16        = str_formatter_format_u16;
    func format_u32        = str_formatter_format_u32;
    func format_u64        = str_formatter_format_u64;
    func format_u          = str_formatter_format_u;
    func format_s8         = str_formatter_format_s8;
    func format_s16        = str_formatter_format_s16;
    func format_s32        = str_formatter_format_s32;
    func format_s64        = str_formatter_format_s64;
    func format_s          = str_formatter_format_s;
    func format_ascii      = str_formatter_format_ascii;
    func format_astr       = str_formatter_format_astr;
    func format_astr_range = str_formatter_format_astr_range;
    func format_pointer    = str_formatter_format_pointer;
}

export let PANIC_FORMAT_BUFFER_TOO_SMALL : ^ascii =
    $"Format buffer too small."a;
export let PANIC_FORMAT_STRING_DOES_NOT_CONTAIN_SPECIFIER : ^ ascii =
    $"Format string does not contain specifier."a;
export let PANIC_INVALID_FORMAT_SPECIFIER : ^ascii =
    $"Invalid format specifier."a;
export let PANIC_INVALID_FORMAT_STRING : ^ascii =
    $"Invalid format string."a;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

export func format_specifier_is_integer : (this : ^format_specifier) -> bool
{
    return this->format_type == FORMAT_TYPE_INTEGER_BIN
        || this->format_type == FORMAT_TYPE_INTEGER_DEC
        || this->format_type == FORMAT_TYPE_INTEGER_HEX_LOWER
        || this->format_type == FORMAT_TYPE_INTEGER_HEX_UPPER;
}

export func format_specifier_is_float : (this : ^format_specifier) -> bool
{
    return this->format_type == FORMAT_TYPE_FLOAT_DEC
        || this->format_type == FORMAT_TYPE_FLOAT_SCI_LOWER
        || this->format_type == FORMAT_TYPE_FLOAT_SCI_UPPER;
}

export func format_specifier_parse : (
    this : ^mut format_specifier,
    parse_begin : ^ascii
) -> u
{
    let cur : mut ^ascii = parse_begin;

    # Opening '{' (required).
    if @cur != '{'a {panic(PANIC_INVALID_FORMAT_SPECIFIER);}
    cur = cur +^ 1u;

    # Type (required).
    this->format_type = @cur;
    if this->format_type != FORMAT_TYPE_INTEGER_BIN
        && this->format_type != FORMAT_TYPE_INTEGER_DEC
        && this->format_type != FORMAT_TYPE_INTEGER_HEX_LOWER
        && this->format_type != FORMAT_TYPE_INTEGER_HEX_UPPER
        && this->format_type != FORMAT_TYPE_FLOAT_DEC
        && this->format_type != FORMAT_TYPE_FLOAT_SCI_LOWER
        && this->format_type != FORMAT_TYPE_FLOAT_SCI_UPPER
        && this->format_type != FORMAT_TYPE_ASCII
        && this->format_type != FORMAT_TYPE_ASTR
        && this->format_type != FORMAT_TYPE_POINTER
    {
        panic(PANIC_INVALID_FORMAT_SPECIFIER);
    }
    cur = cur +^ 1u;

    # Closing '}' (required).
    if @cur != '}'a {panic(PANIC_INVALID_FORMAT_SPECIFIER);}
    cur = cur +^ 1u;

    return cur -^^ parse_begin;
}

export func format_specifier_write_u64 : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    value : u64
) -> u
{
    let digit_buf : [128u]mut ascii = void;
    let digit_buf_end : ^mut ascii = $digit_buf +^ countof(digit_buf);
    let digit_buf_ptr : mut ^mut ascii = digit_buf_end;
    let write_cur : mut ^mut ascii = write_begin;
    let mut_value : mut u64 = value;

    let digits : mut ^ascii = null;
    if this->format_type == FORMAT_TYPE_INTEGER_HEX_LOWER
    {
        digits = $DIGITS_LOWER;
    }
    else
    {
        digits = $DIGITS_UPPER;
    }

    let digit_base : mut u = void;
    if   this->format_type == FORMAT_TYPE_INTEGER_BIN      {digit_base = 2u;}
    elif this->format_type == FORMAT_TYPE_INTEGER_DEC      {digit_base = 10u;}
    elif this->format_type == FORMAT_TYPE_INTEGER_HEX_LOWER{digit_base = 16u;}
    elif this->format_type == FORMAT_TYPE_INTEGER_HEX_UPPER{digit_base = 16u;}
    else{panic(PANIC_UNEXPECTED_CONTROL_FLOW);}

    let digits_written : mut u = 0u;
    loop true
    {
        let remainder : u64 = u64_rem(mut_value, digit_base as u64);
        let digit_ascii : ascii = digits[remainder];

        digit_buf_ptr = digit_buf_ptr -^ 1u;
        @digit_buf_ptr = digit_ascii;
        mut_value = mut_value / (digit_base as u64);
        digits_written = digits_written + 1u;

        if mut_value == 0u as u64{break;}
    }

    loop digit_buf_ptr != digit_buf_end
    {
        if write_cur == write_end{panic(PANIC_FORMAT_BUFFER_TOO_SMALL);}
        @write_cur = @digit_buf_ptr;
        write_cur = write_cur +^ 1u;
        digit_buf_ptr = digit_buf_ptr +^ 1u;
    }

    return digits_written;
}

export func format_specifier_write_s64 : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    value : s64
) -> u
{
    let digit_buf : [128u]mut ascii = void;
    let digit_buf_end : ^mut ascii = $digit_buf +^ countof(digit_buf);
    let digit_buf_ptr : mut ^mut ascii = digit_buf_end;
    let write_cur : mut ^mut ascii = write_begin;
    let mut_value : mut s64 = value;

    let digits : mut ^ascii = null;
    if this->format_type == FORMAT_TYPE_INTEGER_HEX_LOWER
    {
        digits = $DIGITS_LOWER;
    }
    else
    {
        digits = $DIGITS_UPPER;
    }

    let digit_base : mut u = void;
    if   this->format_type == FORMAT_TYPE_INTEGER_BIN      {digit_base = 2u;}
    elif this->format_type == FORMAT_TYPE_INTEGER_DEC      {digit_base = 10u;}
    elif this->format_type == FORMAT_TYPE_INTEGER_HEX_LOWER{digit_base = 16u;}
    elif this->format_type == FORMAT_TYPE_INTEGER_HEX_UPPER{digit_base = 16u;}
    else{panic(PANIC_UNEXPECTED_CONTROL_FLOW);}

    let digits_written : mut u = 0u;
    loop true
    {
        let remainder : u =
            s64_abs(s64_rem(mut_value, digit_base as s64)) as u;
        let digit_ascii : ascii = digits[remainder];

        digit_buf_ptr = digit_buf_ptr -^ 1u;
        @digit_buf_ptr = digit_ascii;
        mut_value = mut_value / (digit_base as s64);
        digits_written = digits_written + 1u;

        if mut_value == 0s64{break;}
    }

    loop digit_buf_ptr != digit_buf_end
    {
        if write_cur == write_end{panic(PANIC_FORMAT_BUFFER_TOO_SMALL);}
        @write_cur = @digit_buf_ptr;
        write_cur = write_cur +^ 1u;
        digit_buf_ptr = digit_buf_ptr +^ 1u;
    }

    return digits_written;
}

export func format_specifier_write_ascii : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    ch : ascii
) -> u
{
    if write_begin == write_end{panic(PANIC_FORMAT_BUFFER_TOO_SMALL);}
    @write_begin = ch;
    return sizeof(ascii);
}

export func format_specifier_write_astr : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    str : ^ascii
) -> u
{
    let write_cur : mut ^mut ascii = write_begin;
    let str_cur : mut ^ascii = str;
    let asciis_written : mut u = 0u;

    loop @str_cur != '\0'a
    {
        if write_cur == write_end{panic(PANIC_FORMAT_BUFFER_TOO_SMALL);}
        @write_cur = @str_cur;
        write_cur = write_cur +^ 1u;
        str_cur = write_cur +^ 1u;
        asciis_written = asciis_written + 1u;
    }

    return asciis_written;
}

export func format_specifier_write_astr_range : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    str_begin : ^ascii,
    str_end : ^ascii
) -> u
{
    let write_cur : mut ^mut ascii = write_begin;
    let str_cur : mut ^ascii = str_begin;
    let asciis_written : mut u = 0u;

    loop str_cur != str_end
    {
        if write_cur == write_end{panic(PANIC_FORMAT_BUFFER_TOO_SMALL);}
        @write_cur = @str_cur;
        write_cur = write_cur +^ 1u;
        str_cur = str_cur +^ 1u;
        asciis_written = asciis_written + 1u;
    }

    return asciis_written;
}

export func format_specifier_write_pointer : (
    this : ^format_specifier,
    write_begin : ^mut ascii,
    write_end : ^ascii,
    ptr : ^void
) -> u
{
    let write_cur : mut ^mut ascii = write_begin;
    let asciis_written : mut u = 0u;

    if write_cur == write_end{panic(PANIC_FORMAT_BUFFER_TOO_SMALL);}
    @write_cur = '0'a;
    write_cur = write_cur +^ 1u;
    asciis_written = asciis_written + 1u;

    if write_cur == write_end{panic(PANIC_FORMAT_BUFFER_TOO_SMALL);}
    @write_cur = 'x'a;
    write_cur = write_cur +^ 1u;
    asciis_written = asciis_written + 1u;

    this->format_type = FORMAT_TYPE_INTEGER_HEX_LOWER;
    asciis_written = asciis_written +
        this->write_u64(write_cur, write_end, ptr as u64);

    return asciis_written;
}

func advance_to_next_specifier : (fmtr : ^str_formatter) -> void
{
    loop @fmtr->format_ptr != '\0'a
    {
        if fmtr->write_ptr == fmtr->write_buf_end
        {
            panic(PANIC_FORMAT_BUFFER_TOO_SMALL);
        }

        if @fmtr->format_ptr == '{'a
        {
            if @(fmtr->format_ptr +^ 1u ) == '{'a
            {
                @fmtr->write_ptr = '{'a;
                fmtr->write_ptr = fmtr->write_ptr +^ 1u;
                fmtr->format_ptr = fmtr->format_ptr +^ 2u;
            }
            else
            {
                fmtr->format_ptr = fmtr->format_ptr +^
                    fmtr->_next_specifier.parse(fmtr->format_ptr);
                fmtr->_has_next_specifier = true;
                return;
            }
        }
        elif @fmtr->format_ptr == '}'a
        {
            if @(fmtr->format_ptr +^ 1u ) == '}'a
            {
                @fmtr->write_ptr = '}'a;
                fmtr->write_ptr = fmtr->write_ptr +^ 1u;
                fmtr->format_ptr = fmtr->format_ptr +^ 2u;
            }
            else
            {
                panic(PANIC_INVALID_FORMAT_STRING);
            }
        }
        else
        {
            @fmtr->write_ptr = @fmtr->format_ptr;
            fmtr->write_ptr = fmtr->write_ptr +^ 1u;
            fmtr->format_ptr = fmtr->format_ptr +^ 1u;
        }
    }
    @fmtr->write_ptr = '\0'a;
    fmtr->_has_next_specifier = false;
}

export func str_formatter_assign : (
    this : ^mut str_formatter,
    write_buf : ^mut ascii,
    write_buf_length : u,
    format : ^ascii
) -> void
{
    this->write_buf_begin = write_buf;
    this->write_buf_end = write_buf +^ write_buf_length;
    this->format = format;

    this->write_ptr = write_buf;
    this->format_ptr = format;

    advance_to_next_specifier(this);
}

export func str_formatter_update_buffer : (
    this : ^mut str_formatter,
    new_write_buf : ^mut ascii,
    new_write_buf_length : u,
) -> void
{
    let write_idx : u = this->write_ptr -^^ this->write_buf_begin;

    this->write_buf_begin = new_write_buf;
    this->write_buf_end = new_write_buf +^ new_write_buf_length;
    this->write_ptr = new_write_buf +^ write_idx;
}

export func str_formatter_next_specifier : (
    this : ^mut str_formatter
) -> ^format_specifier
{
    if this->_has_next_specifier
    {
        return ?this->_next_specifier;
    }
    return null;
}

export func str_formatter_format_u8 : (
    this : ^mut str_formatter,
    value : u8
) -> void
{
    this->format_u64(value as u64);
}

export func str_formatter_format_u16 : (
    this : ^mut str_formatter,
    value : u16
) -> void
{
    this->format_u64(value as u64);
}

export func str_formatter_format_u32 : (
    this : ^mut str_formatter,
    value : u32
) -> void
{
    this->format_u64(value as u64);
}

export func str_formatter_format_u : (
    this : ^mut str_formatter,
    value : u
) -> void
{
    this->format_u64(value as u64);
}

export func str_formatter_format_s8 : (
    this : ^mut str_formatter,
    value : s8
) -> void
{
    this->format_s64(value as smax);
}

export func str_formatter_format_s16 : (
    this : ^mut str_formatter,
    value : s16
) -> void
{
    this->format_s64(value as smax);
}

export func str_formatter_format_s32 : (
    this : ^mut str_formatter,
    value : s32
) -> void
{
    this->format_s64(value as smax);
}

export func str_formatter_format_s : (
    this : ^mut str_formatter,
    value : s
) -> void
{
    this->format_s64(value as s64);
}

export func str_formatter_format_u64 : (
    this : ^mut str_formatter,
    value : u64
) -> void
{
    let fmt_spec : ^format_specifier = this->next_specifier();
    this->write_ptr = this->write_ptr +^ fmt_spec->write_u64(
        this->write_ptr,
        this->write_buf_end,
        value
    );
    advance_to_next_specifier(this);
}

export func str_formatter_format_s64 : (
    this : ^mut str_formatter,
    value : s64
) -> void
{
    let fmt_spec : ^format_specifier = this->next_specifier();
    this->write_ptr = this->write_ptr +^ fmt_spec->write_s64(
        this->write_ptr,
        this->write_buf_end,
        value
    );
    advance_to_next_specifier(this);
}

export func str_formatter_format_ascii : (
    this : ^mut str_formatter,
    ch : ascii
) -> void
{
    let fmt_spec : ^format_specifier = this->next_specifier();
    this->write_ptr = this->write_ptr +^ fmt_spec->write_ascii(
        this->write_ptr,
        this->write_buf_end,
        ch
    );
    advance_to_next_specifier(this);
}

export func str_formatter_format_astr : (
    this : ^mut str_formatter,
    str : ^ascii
) -> void
{
    let fmt_spec : ^format_specifier = this->next_specifier();
    this->write_ptr = this->write_ptr +^ fmt_spec->write_str(
        this->write_ptr,
        this->write_buf_end,
        str
    );
    advance_to_next_specifier(this);
}

export func str_formatter_format_astr_range : (
    this : ^mut str_formatter,
    str_begin : ^ascii,
    str_end : ^ascii
) -> void
{
    let fmt_spec : ^format_specifier = this->next_specifier();
    this->write_ptr = this->write_ptr +^ fmt_spec->write_str_range(
        this->write_ptr,
        this->write_buf_end,
        str_begin,
        str_end
    );
    advance_to_next_specifier(this);
}

export func str_formatter_format_pointer : (
    this : ^mut str_formatter,
    ptr : ^void
) -> void
{
    let fmt_spec : ^format_specifier = this->next_specifier();
    this->write_ptr = this->write_ptr +^ fmt_spec->write_pointer(
        this->write_ptr,
        this->write_buf_end,
        ptr
    );
    advance_to_next_specifier(this);
}
