# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/stdio.n"a;
import "cstd/stdlib.n"a;
import "cstd/string.n"a;

export func panic : (msg : ^ascii) -> void;

export let PANIC_INVALID_ARGUMENT : ^ascii =
    $"Invalid argument."a;
export let PANIC_INVALID_INDEX : ^ascii =
    $"Invalid index."a;
export let PANIC_INVALID_INIT : ^ascii =
    $"Invalid init call."a;
export let PANIC_INVALID_FINI : ^ascii =
    $"Invalid fini call."a;
export let PANIC_UNEXPECTED_CONTROL_FLOW : ^ascii =
    $"Unexpected control flow."a;

################################################################################
###                                 INTERNAL                                 ###
################################################################################

let PANIC_BASE_STR : [8u]ascii = "panic: "a;

export func panic : (msg : ^ascii) -> void
{
    if msg != null
    {
        fwrite($PANIC_BASE_STR as ^mut void, 1u, countof(PANIC_BASE_STR) - 1u, stderr);
        fputs(msg, stderr);
        fputc('\n'a as sint, stderr);
    }
    abort();
}
