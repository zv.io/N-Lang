# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# ANSI Escape Sequences and Terminal Controls.
# Based off of ANSI/VT100 terminal codes and the ECMA-48 Standard.

# Convenient variables for indexing rows and columns.
export let ANSIESC_FIRST_ROW : u = 1u;
export let ANSIESC_FIRST_COL : u = 1u;

# Control Sequence
export let ANSIESC_CSI : ^ascii = $"\e["a;

# Cursor Up (default 1 cell)
export let ANSIESC_CUU : ^ascii = $"\e[{d}A"a;
export let ANSIESC_CUU_DEFAULT : ^ascii = $"\e[A"a;

# Cursor Down (default 1 cell)
export let ANSIESC_CUD : ^ascii = $"\e[{d}B"a;
export let ANSIESC_CUD_DEFAULT : ^ascii = $"\e[B"a;

# Cursor Forward (default 1 cell)
export let ANSIESC_CUF : ^ascii = $"\e[{d}C"a;
export let ANSIESC_CUF_DEFAULT : ^ascii = $"\e[C"a;

# Cursor Backward (default 1 cell)
export let ANSIESC_CUB : ^ascii = $"\e[{d}D"a;
export let ANSIESC_CUB_DEFAULT : ^ascii = $"\e[D"a;

# Cursor Next Line(default 1 line)
export let ANSIESC_CNL : ^ascii = $"\e[{d}E"a;
export let ANSIESC_CNL_DEFAULT : ^ascii = $"\e[E"a;

# Cursor Previous Line (default 1 line)
export let ANSIESC_CPL : ^ascii = $"\e[{d}F"a;
export let ANSIESC_CPL_DEFAULT : ^ascii = $"\e[F"a;

# Cursor Horizontal Absolute (1-indexed)
export let ANSIESC_CHA : ^ascii = $"\e[{d}G"a;

# Cursor Position (1-indexed) <horizontal>:<vertical>
export let ANSIESC_CUP : ^ascii = $"\e[{d};{d}H"a;
export let ANSIESC_HVP : ^ascii = $"\e[{d};{d}f"a; # Identical to CUP

# Erase Display
# 0 : Clear from cursor to end of screen.
# 1 : Clear from cursor to beginning of screen.
# 2 : Clear entire screen.
# 3 : Clear entire screen & delete all lines in the scrollback buffer.
export let ANSIESC_ED : ^ascii = $"\e[{d}J"a;

# Erase Line
# 0 : Clear from cursor to end of line.
# 1 : Clear from cursor to beginning of line.
# 2 : Clear entire line.
export let ANSIESC_EL : ^ascii = $"\e[{d}K"a;

# Scroll Up (default 1-line)
export let ANSIESC_SU : ^ascii = $"\e[{d}S"a;
export let ANSIESC_SU_DEFAULT : ^ascii = $"\e[S"a;

# Scroll Down (default 1-line)
export let ANSIESC_SD : ^ascii = $"\e[{d}T"a;
export let ANSIESC_SD_DEFAULT : ^ascii = $"\e[T"a;

# Save Cursor Position and State
export let ANSIESC_SCP : ^ascii = $"\e[s"a;

# Restore Cursor Position and State
export let ANSIESC_RCP : ^ascii = $"\e[u"a;

# Set Graphic Rendition
# Set the attributes for characters written after the SGR sequence.
# Multiple attributes can be set at once by separating each attribute with a
# semicolon.
# All SGR strings should start with ANSIESC_SGR_START and end with
# ANSIESC_SGR_END.
# NOTE
#   + Sometimes the 'bright' attribute is referred to as 'bold'.
#   + Sometimes the 'dim' attribute is referred to as 'faint'.
#   + Italics are not widely supported. Use with caution.
#   + Fast blink is not widely supported. Use with caution.
#   + Conceal is not widely supported. Use with caution.
#   + Fraktur is rarely supported. Use with caution.
export let ANSIESC_SGR_START : ^ascii = $"\e["a;
export let ANSIESC_SGR_END   : ^ascii = $"m"a;

# Reset graphic redition to default settings.
# This str should not be enclosed in ANSIESC_SGR_START/ANSIESC_SGR_END.
export let ANSIESC_SGR_DEFAULT : ^ascii = $"\e[0m"a;

export let ANSIESC_SGR_ATTR_RESET                : ^ascii = $"0"a;
export let ANSIESC_SGR_ATTR_BRIGHT               : ^ascii = $"1"a;
export let ANSIESC_SGR_ATTR_DIM                  : ^ascii = $"2"a;
export let ANSIESC_SGR_ATTR_ITALIC               : ^ascii = $"3"a;
export let ANSIESC_SGR_ATTR_UNDERLINE            : ^ascii = $"4"a;
export let ANSIESC_SGR_ATTR_BLINK_SLOW           : ^ascii = $"5"a;
export let ANSIESC_SGR_ATTR_BLINK_FAST           : ^ascii = $"6"a;
export let ANSIESC_SGR_ATTR_INVERSE              : ^ascii = $"7"a;
export let ANSIESC_SGR_ATTR_CONCEAL              : ^ascii = $"8"a;
export let ANSIESC_SGR_ATTR_STRIKETHROUGH        : ^ascii = $"9"a;
export let ANSIESC_SGR_ATTR_PRIMARY_FONT         : ^ascii = $"10"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_1     : ^ascii = $"11"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_2     : ^ascii = $"12"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_3     : ^ascii = $"13"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_4     : ^ascii = $"14"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_5     : ^ascii = $"15"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_6     : ^ascii = $"16"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_7     : ^ascii = $"17"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_8     : ^ascii = $"18"a;
export let ANSIESC_SGR_ATTR_ALTERNATE_FONT_9     : ^ascii = $"19"a;
export let ANSIESC_SGR_ATTR_FRAKTUR              : ^ascii = $"20"a;
export let ANSIESC_SGR_ATTR_INTENSITY_OFF        : ^ascii = $"22"a;
export let ANSIESC_SGR_ATTR_ITALIC_OFF           : ^ascii = $"23"a;
export let ANSIESC_SGR_ATTR_UNDERLINE_OFF        : ^ascii = $"24"a;
export let ANSIESC_SGR_ATTR_BLINK_OFF            : ^ascii = $"25"a;
export let ANSIESC_SGR_ATTR_INVERSE_OFF          : ^ascii = $"27"a;
export let ANSIESC_SGR_ATTR_CONCEAL_OFF          : ^ascii = $"28"a;
export let ANSIESC_SGR_ATTR_STRIKETHROUGH_OFF    : ^ascii = $"29"a;
export let ANSIESC_SGR_ATTR_FG_BLACK             : ^ascii = $"30"a;
export let ANSIESC_SGR_ATTR_FG_RED               : ^ascii = $"31"a;
export let ANSIESC_SGR_ATTR_FG_GREEN             : ^ascii = $"32"a;
export let ANSIESC_SGR_ATTR_FG_YELLOW            : ^ascii = $"33"a;
export let ANSIESC_SGR_ATTR_FG_BLUE              : ^ascii = $"34"a;
export let ANSIESC_SGR_ATTR_FG_MAGENTA           : ^ascii = $"35"a;
export let ANSIESC_SGR_ATTR_FG_CYAN              : ^ascii = $"36"a;
export let ANSIESC_SGR_ATTR_FG_WHITE             : ^ascii = $"37"a;
export let ANSIESC_SGR_ATTR_FG_DEFAULT           : ^ascii = $"39"a;
export let ANSIESC_SGR_ATTR_BG_BLACK             : ^ascii = $"40"a;
export let ANSIESC_SGR_ATTR_BG_RED               : ^ascii = $"41"a;
export let ANSIESC_SGR_ATTR_BG_GREEN             : ^ascii = $"42"a;
export let ANSIESC_SGR_ATTR_BG_YELLOW            : ^ascii = $"43"a;
export let ANSIESC_SGR_ATTR_BG_BLUE              : ^ascii = $"44"a;
export let ANSIESC_SGR_ATTR_BG_MAGENTA           : ^ascii = $"45"a;
export let ANSIESC_SGR_ATTR_BG_CYAN              : ^ascii = $"46"a;
export let ANSIESC_SGR_ATTR_BG_WHITE             : ^ascii = $"47"a;
export let ANSIESC_SGR_ATTR_BG_DEFAULT           : ^ascii = $"49"a;
export let ANSIESC_SGR_ATTR_FRAMED               : ^ascii = $"51"a;
export let ANSIESC_SGR_ATTR_ENCIRCLED            : ^ascii = $"52"a;
export let ANSIESC_SGR_ATTR_OVERLINED            : ^ascii = $"53"a;
export let ANSIESC_SGR_ATTR_FRAMED_ENCIRCLED_OFF : ^ascii = $"54"a;
export let ANSIESC_SGR_ATTR_OVERLINED_OFF        : ^ascii = $"55"a;
