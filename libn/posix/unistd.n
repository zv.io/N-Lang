# Copyright 2018 N-Lang Project Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import "cstd/primitives.n"a;

# Linux glibc *_t definitions.
# Taken from "glibc/sysdeps/unix/sysv/linux/x86/bits/typesizes.h".
export alias mode_t = u32;
export alias pid_t  = s32;

# Linux access function constants.
export let F_OK : sint = 0x0s as sint;
export let R_OK : sint = 0x4s as sint;
export let W_OK : sint = 0x2s as sint;
export let X_OK : sint = 0x1s as sint;

# Linux open function constants.
export let O_RDONLY : sint = 0x000s as sint;
export let O_WRONLY : sint = 0x001s as sint;
export let O_RDWR   : sint = 0x002s as sint;
export let O_CREAT  : sint = 0x040s as sint;
export let O_TRUNC  : sint = 0x200s as sint;
export let O_APPEND : sint = 0x400s as sint;

# Linux lseek and fcntl function constants.
export let SEEK_CUR : sint = 0x1s as sint;
export let SEEK_END : sint = 0x2s as sint;
export let SEEK_SET : sint = 0x0s as sint;

# File stream constants.
export let STDIN_FILENO  : sint = 0s as sint;
export let STDOUT_FILENO : sint = 1s as sint;
export let STDERR_FILENO : sint = 2s as sint;

export extern func access : (pathname : mut ^ascii, mode : mut sint) -> sint;

export extern func chdir  : (path : mut ^ascii) -> mut sint;
export extern func fchdir : (fd : sint) -> sint;

export extern func open : (pathname : mut ^ascii, flags : mut sint, mode: mut mode_t) -> sint;
export extern func close : (fd : mut sint) -> sint;

export extern func read : (fd : mut sint, buf : mut ^mut void, nbytes : mut size_t) -> ssize_t;
export extern func write : (fd : mut sint, buf : mut ^void, nbytes : mut size_t) -> ssize_t;
