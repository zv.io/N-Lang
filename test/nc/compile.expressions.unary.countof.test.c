/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(unary_countof__array_variables)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : [10u]s = void;                                                   \n\
    countof(x);                                                              \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_countof__array_literals)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    countof([10u, 11u, 12u]);                                                \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_countof__array_data_types)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    countof([10u]s32);                                                       \n\
    countof([10u]^s);                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_countof__valid)
{
    EMU_ADD(unary_countof__array_variables);
    EMU_ADD(unary_countof__array_literals);
    EMU_ADD(unary_countof__array_data_types);
    EMU_END_GROUP();
}

EMU_GROUP(unary_countof)
{
    EMU_ADD(unary_countof__valid);
    EMU_END_GROUP();
}
