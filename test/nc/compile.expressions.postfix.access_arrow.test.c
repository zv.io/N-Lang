/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(access_arrow__member_var_yeilds_rvalue)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : mut u;                                                         \n\
    let baz : mut s;                                                         \n\
    let qux : mut u32;                                                       \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let myfoo : ^foo = void;                                                 \n\
    let myu : u = myfoo->bar;                                                \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(access_arrow__member_var_yeilds_lvalue)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : mut u;                                                         \n\
    let baz : mut s;                                                         \n\
    let qux : mut u32;                                                       \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let myfoo : ^mut foo = void;                                             \n\
    let myu : u = 123u;                                                      \n\
    myfoo->bar = myu;                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(access_arrow__member_nested_access)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : mut u;                                                         \n\
}                                                                            \n\
                                                                             \n\
struct baz                                                                   \n\
{                                                                            \n\
    let qux : mut ^foo;                                                      \n\
    let quux : mut u;                                                        \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let mybaz : ^baz = void;                                                 \n\
    let myu : u = mybaz->qux->bar;                                           \n\
    mybaz->qux->bar = myu;                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(access_arrow__member_func_access_executes_function_call__no_args)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
struct foo;                                                                  \n\
export func foo_get_bar : (this : ^foo) -> u;                                \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : mut u;                                                         \n\
    func get_bar = foo_get_bar;                                              \n\
}                                                                            \n\
                                                                             \n\
func foo_get_bar : (this : ^foo) -> u                                        \n\
{                                                                            \n\
    return this->bar;                                                        \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let myfoo : ^foo = void;                                                 \n\
    let myu : u = myfoo->get_bar();                                          \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(access_arrow__member_func_access_executes_function_call__multiple_args)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
struct foo;                                                                  \n\
export func foo_add_to_bar : (this : ^foo, first : u, second : u) -> u;      \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : mut u;                                                         \n\
    func add_to_bar = foo_add_to_bar;                                        \n\
}                                                                            \n\
                                                                             \n\
func foo_add_to_bar : (this : ^foo, first : u, second : u) -> u              \n\
{                                                                            \n\
    return this->bar + first + second;                                       \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let myfoo : ^foo = void;                                                 \n\
    let myu : u = myfoo->add_to_bar(10u, 35u);                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(access_arrow__valid)
{
    EMU_ADD(access_arrow__member_var_yeilds_rvalue);
    EMU_ADD(access_arrow__member_var_yeilds_lvalue);
    EMU_ADD(access_arrow__member_nested_access);
    EMU_ADD(access_arrow__member_func_access_executes_function_call__no_args);
    EMU_ADD(access_arrow__member_func_access_executes_function_call__multiple_args);
    EMU_END_GROUP();
}

EMU_TEST(access_arrow__access_member_not_in_struct)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : mut u;                                                         \n\
    let baz : mut s;                                                         \n\
    let qux : mut u32;                                                       \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let myfoo : ^foo = void;                                                 \n\
    myfoo->bad_member;                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(access_arrow__access_member_in_non_composite_type)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let myu : mut ^u = void;                                                 \n\
    myu->bad_member;                                                         \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(access_arrow__access_member_from_non_pointer_to_struct)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : mut u;                                                         \n\
    let baz : mut s;                                                         \n\
    let qux : mut u32;                                                       \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let myfoo : mut foo = void;                                              \n\
    myfoo->bar;                                                              \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(access_arrow__invalid)
{
    EMU_ADD(access_arrow__access_member_not_in_struct);
    EMU_ADD(access_arrow__access_member_in_non_composite_type);
    EMU_ADD(access_arrow__access_member_from_non_pointer_to_struct);
    EMU_END_GROUP();
}

EMU_GROUP(postfix__access_arrow)
{
    EMU_ADD(access_arrow__valid);
    EMU_ADD(access_arrow__invalid);
    EMU_END_GROUP();
}

