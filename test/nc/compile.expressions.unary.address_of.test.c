/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(unary_address_of__declared_variable)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let banana   :  s = void;                                                \n\
    let p_banana : ^s = ?banana;                                             \n\
                                                                             \n\
    let pear   :  s = void;                                                  \n\
    let p_pear : ^s = ?pear;                                                 \n\
                                                                             \n\
    let apple   :  [3u]mut ^u32 = void;                                      \n\
    let p_apple : ^[3u]mut ^u32 = ?apple;                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_address_of__valid)
{
    EMU_ADD(unary_address_of__declared_variable);
    EMU_END_GROUP();
}

EMU_TEST(unary_address_of__declared_function)
{
    // Note that since function types are themselves function pointers, using
    // the address-of operator on a function pointer don't make sense.
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func apple : () -> void {}                                                   \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p_apple: ^() -> void = ?apple;                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__u8)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^u8 = ?10u8;                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__u16)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^u16 = ?10u16;                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__u32)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^u32 = ?10u32;                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__u64)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^u64 = ?10u64;                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__u)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^u = ?10u32;                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__s8)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^s8 = ?10s8;                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__s16)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^s16 = ?10s16;                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__s32)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^s32 = ?10s32;                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__s64)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^s64 = ?10s64;                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__s)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^s = ?10s;                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__ascii)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^ascii = ?'\\0'a;                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__bool)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^bool = ?false;                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_address_of__literal__string)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p: ^ascii = ?\"banana\"a;                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_address_of__literal)
{
    EMU_ADD(unary_address_of__literal__u8);
    EMU_ADD(unary_address_of__literal__u16);
    EMU_ADD(unary_address_of__literal__u32);
    EMU_ADD(unary_address_of__literal__u64);
    EMU_ADD(unary_address_of__literal__u);
    EMU_ADD(unary_address_of__literal__s8);
    EMU_ADD(unary_address_of__literal__s16);
    EMU_ADD(unary_address_of__literal__s32);
    EMU_ADD(unary_address_of__literal__s64);
    EMU_ADD(unary_address_of__literal__s);
    EMU_ADD(unary_address_of__literal__ascii);
    EMU_ADD(unary_address_of__literal__bool);
    EMU_ADD(unary_address_of__literal__string);
    EMU_END_GROUP();
}

EMU_GROUP(unary_address_of__invalid)
{
    EMU_ADD(unary_address_of__declared_function);
    EMU_ADD(unary_address_of__literal);
    EMU_END_GROUP();
}

EMU_GROUP(unary_address_of)
{
    EMU_ADD(unary_address_of__valid);
    EMU_ADD(unary_address_of__invalid);
    EMU_END_GROUP();
}
