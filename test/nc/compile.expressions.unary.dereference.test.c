/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(unary_dereference__pointer_variable)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p : ^s = void;                                                       \n\
    let d : s = @p;                                                          \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_dereference__valid)
{
    EMU_ADD(unary_dereference__pointer_variable);
    EMU_END_GROUP();
}

EMU_TEST(unary_dereference__array_variable)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let p : [10u]s = void;                                                   \n\
    let d : s = @p;                                                          \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__declared_function)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func apple : () -> void {}                                                   \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @apple;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__u8)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10u8;                                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__u16)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10u16;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__u32)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10u32;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__u64)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10u64;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__u)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10u32;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__s8)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10s8;                                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__s16)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10s16;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__s32)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10s32;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__s64)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10s64;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__s)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @10s;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__ascii)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @'\\0'a;                                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__bool)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @false;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_dereference__literal__string)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    @\"banana\"a;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_dereference__literal)
{
    EMU_ADD(unary_dereference__literal__u8);
    EMU_ADD(unary_dereference__literal__u16);
    EMU_ADD(unary_dereference__literal__u32);
    EMU_ADD(unary_dereference__literal__u64);
    EMU_ADD(unary_dereference__literal__u);
    EMU_ADD(unary_dereference__literal__s8);
    EMU_ADD(unary_dereference__literal__s16);
    EMU_ADD(unary_dereference__literal__s32);
    EMU_ADD(unary_dereference__literal__s64);
    EMU_ADD(unary_dereference__literal__s);
    EMU_ADD(unary_dereference__literal__ascii);
    EMU_ADD(unary_dereference__literal__bool);
    EMU_ADD(unary_dereference__literal__string);
    EMU_END_GROUP();
}

EMU_GROUP(unary_dereference__invalid)
{
    EMU_ADD(unary_dereference__array_variable);
    EMU_ADD(unary_dereference__declared_function);
    EMU_ADD(unary_dereference__literal);
    EMU_END_GROUP();
}

EMU_GROUP(unary_dereference)
{
    EMU_ADD(unary_dereference__valid);
    EMU_ADD(unary_dereference__invalid);
    EMU_END_GROUP();
}
