/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(struct_declarations__declaration_with_definition__one_member_var)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : u;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__declaration_with_definition__multiple_member_vars)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : u;                                                             \n\
    let baz : s;                                                             \n\
    let qux : u32;                                                           \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__single_forward_declaration)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct foo;                                                                  \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__multiple_forward_declarations)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct foo;                                                                  \n\
struct foo;                                                                  \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__declarations_with_definition_last)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct foo;                                                                  \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : u;                                                             \n\
    let baz : s;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__nest_defined_struct)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : u;                                                             \n\
}                                                                            \n\
                                                                             \n\
struct baz                                                                   \n\
{                                                                            \n\
    let qux : foo;                                                           \n\
    let quux : u;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__nest_pointer_to_undefined_struct)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct foo;                                                                  \n\
                                                                             \n\
struct baz                                                                   \n\
{                                                                            \n\
    let qux : ^foo;                                                          \n\
    let quux : u;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(struct_declarations__valid)
{
    EMU_ADD(struct_declarations__declaration_with_definition__one_member_var);
    EMU_ADD(struct_declarations__declaration_with_definition__multiple_member_vars);
    EMU_ADD(struct_declarations__single_forward_declaration);
    EMU_ADD(struct_declarations__multiple_forward_declarations);
    EMU_ADD(struct_declarations__declarations_with_definition_last);
    EMU_ADD(struct_declarations__nest_defined_struct);
    EMU_ADD(struct_declarations__nest_pointer_to_undefined_struct);
    EMU_END_GROUP();
}

EMU_TEST(struct_declarations__duplicate_member_identifiers)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : u;                                                             \n\
    let bar : u;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__declarations_with_definition_not_last)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : u;                                                             \n\
}                                                                            \n\
struct foo;                                                                  \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__nest_undefined_struct)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
struct foo;                                                                  \n\
                                                                             \n\
struct baz                                                                   \n\
{                                                                            \n\
    let qux : foo;                                                           \n\
    let quux : u;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(struct_declarations__member_with_init_expr)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                   \n\
struct foo                                                                   \n\
{                                                                            \n\
    let bar : u = 5u;                                                        \n\
}                                                                            \n\
struct foo;                                                                  \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(struct_declarations__invalid)
{
    EMU_ADD(struct_declarations__duplicate_member_identifiers);
    EMU_ADD(struct_declarations__declarations_with_definition_not_last);
    EMU_ADD(struct_declarations__nest_undefined_struct);
    EMU_ADD(struct_declarations__member_with_init_expr);
    EMU_END_GROUP();
}

EMU_GROUP(compile__struct_declarations)
{
    EMU_ADD(struct_declarations__valid);
    EMU_ADD(struct_declarations__invalid);
    EMU_END_GROUP();
}
