/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(conditional_if)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    if true                                                                  \n\
    {                                                                        \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(conditional_if_elif)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    if false                                                                 \n\
    {                                                                        \n\
    }                                                                        \n\
    elif true                                                                \n\
    {                                                                        \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(conditional_if_elif_elif)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    if false                                                                 \n\
    {                                                                        \n\
    }                                                                        \n\
    elif false                                                               \n\
    {                                                                        \n\
    }                                                                        \n\
    elif true                                                                \n\
    {                                                                        \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(conditional_if_elif_else)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    if false                                                                 \n\
    {                                                                        \n\
    }                                                                        \n\
    elif false                                                               \n\
    {                                                                        \n\
    }                                                                        \n\
    elif false                                                               \n\
    {                                                                        \n\
    }                                                                        \n\
    else                                                                     \n\
    {                                                                        \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(conditional_if_else)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    if false                                                                 \n\
    {                                                                        \n\
    }                                                                        \n\
    else                                                                     \n\
    {                                                                        \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(if_elif_else__valid)
{
    EMU_ADD(conditional_if);
    EMU_ADD(conditional_if_elif);
    EMU_ADD(conditional_if_elif_elif);
    EMU_ADD(conditional_if_elif_else);
    EMU_ADD(conditional_if_else);
    EMU_END_GROUP();
}

EMU_TEST(conditional_if__missing_scope)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                     \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    if true                                                                  \n\
        let foo : s = void;                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(conditional_elif__missing_scope)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                     \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    if true{}                                                                \n\
    elif true                                                                \n\
        let foo : s = void;                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(conditional_else__missing_scope)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                     \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    if true{}                                                                \n\
    elif true{}                                                              \n\
    else                                                                     \n\
        let foo : s = void;                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(if_elif_else__invalid)
{
    EMU_ADD(conditional_if__missing_scope);
    EMU_ADD(conditional_elif__missing_scope);
    EMU_ADD(conditional_else__missing_scope);
    EMU_END_GROUP();
}

EMU_GROUP(compile__if_elif_else)
{
    EMU_ADD(if_elif_else__valid);
    EMU_ADD(if_elif_else__invalid);
    EMU_END_GROUP();
}
