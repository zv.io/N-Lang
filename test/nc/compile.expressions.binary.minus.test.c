/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(binary_minus__two_operands)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    1s - 1s;                                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_minus__many_operands)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    1s - 1s - 1s;                                                            \n\
    1s - 2s - 3s - 4s;                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}
EMU_GROUP(binary_minus__valid)
{
    EMU_ADD(binary_minus__two_operands);
    EMU_ADD(binary_minus__many_operands);
    EMU_END_GROUP();
}

EMU_TEST(binary_minus__incompatible_integer_types)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    1s - 1u;                                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_minus__not_defined_for_bool_type)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    true - false;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_minus__not_defined_for_array_types)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [10u]s = void;                                                 \n\
    let bar : [10u]s = void;                                                 \n\
    foo - bar;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_minus__invalid)
{
    EMU_ADD(binary_minus__incompatible_integer_types);
    EMU_ADD(binary_minus__not_defined_for_bool_type);
    EMU_ADD(binary_minus__not_defined_for_array_types);
    EMU_END_GROUP();
}

EMU_GROUP(binary_minus)
{
    EMU_ADD(binary_minus__valid);
    EMU_ADD(binary_minus__invalid);
    EMU_END_GROUP();
}
