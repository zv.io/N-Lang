/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_DECLARE(primary__identifier);
EMU_DECLARE(primary__literal);
EMU_DECLARE(primary__paren);

EMU_GROUP(expressions__primary)
{
    EMU_ADD(primary__identifier);
    EMU_ADD(primary__literal);
    EMU_ADD(primary__paren);
    EMU_END_GROUP();
}
