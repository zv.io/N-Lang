/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_DECLARE(binary_assign);
EMU_DECLARE(binary_plus);
EMU_DECLARE(binary_minus);
EMU_DECLARE(binary_mult);
EMU_DECLARE(binary_div);
EMU_DECLARE(binary_cast);

EMU_GROUP(expressions__binary)
{
    EMU_ADD(binary_assign);
    EMU_ADD(binary_plus);
    EMU_ADD(binary_minus);
    EMU_ADD(binary_mult);
    EMU_ADD(binary_div);
    EMU_ADD(binary_cast);
    EMU_END_GROUP();
}
