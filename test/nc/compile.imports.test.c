/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(import__exported_variables)
{
    COMPILE_TEST_VALID_MODULE("import \"exported_variables.n\"a;\n");
    EMU_END_TEST();
}

EMU_TEST(import__exported_functions)
{
    COMPILE_TEST_VALID_MODULE("import \"exported_functions.n\"a;\n");
    EMU_END_TEST();
}

EMU_TEST(import__exported_alias)
{
    COMPILE_TEST_VALID_MODULE("import \"exported_alias.n\"a;\n");
    EMU_END_TEST();
}

EMU_GROUP(compile__imports)
{
    COMPILE_WRITE_CSTR_TO_FILE(
        COMPILED_OUTPUT_PATH "exported_variables.n",
        "export let foo_var : u8 = 10u8;\n"
        "export extern let errno : s32;\n"
    );
    COMPILE_WRITE_CSTR_TO_FILE(
        COMPILED_OUTPUT_PATH "exported_functions.n",
        "export func foo_func : () -> void {}\n"
        "export extern func exit : (status : s32) -> void;\n"
    );
    COMPILE_WRITE_CSTR_TO_FILE(
        COMPILED_OUTPUT_PATH "exported_alias.n",
        "export alias u8alias = u8;\n"
    );

    EMU_ADD(import__exported_variables);
    EMU_ADD(import__exported_functions);
    EMU_ADD(import__exported_alias);
    EMU_END_GROUP();
}
