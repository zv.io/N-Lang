/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(paren__contains_another_expression)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    (1u);                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(paren__valid)
{
    EMU_ADD(paren__contains_another_expression);
    EMU_END_GROUP();
}

EMU_TEST(paren__contains_nothing_between_parentheses)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                     \n\
func entry : () -> u                                                         \n\
{                                                                            \n\
    ();                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(paren__invalid)
{
    EMU_ADD(paren__contains_nothing_between_parentheses);
    EMU_END_GROUP();
}

EMU_GROUP(primary__paren)
{
    EMU_ADD(paren__valid);
    EMU_ADD(paren__invalid);
    EMU_END_GROUP();
}
