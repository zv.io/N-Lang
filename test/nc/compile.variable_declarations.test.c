/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(variable_declarations__integers_without_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
let A : u8 = void;                                                           \n\
let B : u16 = void;                                                          \n\
let C : u32 = void;                                                          \n\
let D : u64 = void;                                                          \n\
let E : u = void;                                                            \n\
let F : s8 = void;                                                           \n\
let G : s16 = void;                                                          \n\
let H : s32 = void;                                                          \n\
let I : s64 = void;                                                          \n\
let J : s = void;                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__integers_without_initialization__flocal)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let A : u8 = void;                                                       \n\
    let B : u16 = void;                                                      \n\
    let C : u32 = void;                                                      \n\
    let D : u64 = void;                                                      \n\
    let E : u = void;                                                        \n\
    let F : s8 = void;                                                       \n\
    let G : s16 = void;                                                      \n\
    let H : s32 = void;                                                      \n\
    let I : s64 = void;                                                      \n\
    let J : s = void;                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__integers_with_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
let A : u8  = 10u8;                                                          \n\
let B : u16 = 10u16;                                                         \n\
let C : u32 = 10u32;                                                         \n\
let D : u64 = 10u64;                                                         \n\
let E : u   = 10u;                                                           \n\
let F : s8  = 10s8;                                                          \n\
let G : s16 = 10s16;                                                         \n\
let H : s32 = 10s32;                                                         \n\
let I : s64 = 10s64;                                                         \n\
let J : s   = 10s;                                                           \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__integers_with_initialization__flocal)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let A : u8  = 10u8;                                                      \n\
    let B : u16 = 10u16;                                                     \n\
    let C : u32 = 10u32;                                                     \n\
    let D : u64 = 10u64;                                                     \n\
    let E : u   = 10u;                                                       \n\
    let F : s8  = 10s8;                                                      \n\
    let G : s16 = 10s16;                                                     \n\
    let H : s32 = 10s32;                                                     \n\
    let I : s64 = 10s64;                                                     \n\
    let J : s   = 10s;                                                       \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(variable_declarations__integers__valid)
{
    EMU_ADD(variable_declarations__integers_without_initialization__global);
    EMU_ADD(variable_declarations__integers_without_initialization__flocal);
    EMU_ADD(variable_declarations__integers_with_initialization__global);
    EMU_ADD(variable_declarations__integers_with_initialization__flocal);
    EMU_END_GROUP();
}

EMU_GROUP(variable_declarations__integers)
{
    EMU_ADD(variable_declarations__integers__valid);
    EMU_END_GROUP();
}

EMU_TEST(variable_declarations__bool_without_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
let is_foo : bool = void;                                                    \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__bool_without_initialization__flocal)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let is_foo : bool = void;                                                \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__bool_with_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
let is_foo : bool = true;                                                    \n\
let is_bar : bool = false;                                                   \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__bool_with_initialization__flocal)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let is_foo : bool = true;                                                \n\
    let is_bar : bool = false;                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(variable_declarations__bool__valid)
{
    EMU_ADD(variable_declarations__bool_without_initialization__global);
    EMU_ADD(variable_declarations__bool_without_initialization__flocal);
    EMU_ADD(variable_declarations__bool_with_initialization__global);
    EMU_ADD(variable_declarations__bool_with_initialization__flocal);
    EMU_END_GROUP();
}

EMU_GROUP(variable_declarations__bool)
{
    EMU_ADD(variable_declarations__bool__valid);
    EMU_END_GROUP();
}

EMU_TEST(variable_declarations__ascii_without_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
let foo : ascii = void;                                                      \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__ascii_without_initialization__flocal)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : ascii = void;                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__ascii_with_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
let foo : ascii = 'a'a;                                                      \n\
let bar : ascii = '\\t'a;                                                    \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__ascii_with_initialization__flocal)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : ascii = 'a'a;                                                  \n\
    let bar : ascii = '\\t'a;                                                \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(variable_declarations__ascii__valid)
{
    EMU_ADD(variable_declarations__ascii_without_initialization__global);
    EMU_ADD(variable_declarations__ascii_without_initialization__flocal);
    EMU_ADD(variable_declarations__ascii_with_initialization__global);
    EMU_ADD(variable_declarations__ascii_with_initialization__flocal);
    EMU_END_GROUP();
}

EMU_GROUP(variable_declarations__ascii)
{
    EMU_ADD(variable_declarations__ascii__valid);
    EMU_END_GROUP();
}

EMU_TEST(variable_declarations__struct_without_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct mystruct                                                              \n\
{                                                                            \n\
    let A : u;                                                               \n\
    let B : s;                                                               \n\
    let C : u32;                                                             \n\
}                                                                            \n\
                                                                             \n\
let foo : mystruct = void;                                                   \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__struct_without_initialization__flocal)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
struct mystruct                                                              \n\
{                                                                            \n\
    let A : u;                                                               \n\
    let B : s;                                                               \n\
    let C : u32;                                                             \n\
}                                                                            \n\
                                                                             \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mystruct = void;                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(variable_declarations__struct__valid)
{
    EMU_ADD(variable_declarations__struct_without_initialization__global);
    EMU_ADD(variable_declarations__struct_without_initialization__flocal);
    EMU_END_GROUP();
}

EMU_GROUP(variable_declarations__struct)
{
    EMU_ADD(variable_declarations__struct__valid);
    EMU_END_GROUP();
}

EMU_TEST(variable_declarations__function_without_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("let foo : (s8, s16) -> s32 = void;  ");
    EMU_END_TEST();
}

EMU_TEST(variable_declarations__function_with_initialization__global)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func f : (A : s8, B : s16) -> s32                                            \n\
{                                                                            \n\
    return 0s32;                                                             \n\
}                                                                            \n\
                                                                             \n\
let foo : mut (s8, s16) -> s32 = f;                                          \n\
let bar : (s8, s16) -> s32 = f;                                              \n\
");
    EMU_END_TEST();
}

EMU_GROUP(variable_declarations__function__valid)
{
    EMU_ADD(variable_declarations__function_without_initialization__global);
    EMU_ADD(variable_declarations__function_with_initialization__global);
    EMU_END_GROUP();
}

EMU_GROUP(variable_declarations__function)
{
    EMU_ADD(variable_declarations__function__valid);
    EMU_END_GROUP();
}

EMU_TEST(variable_declarations__minimum_required_number_of_pointer_modifiers)
{
    // The C standard specifies a minimum of 12 pointer, array, or function
    // declarators as modifiers to a base type.
    // http://c0x.coding-guidelines.com/5.2.4.1.html
    // Make sure that at least 12 pointers can be used as modifiers.
    COMPILE_TEST_VALID_MODULE("                                              \n\
let foo01 : ^s = void;                                                       \n\
let foo02 : ^^s = void;                                                      \n\
let foo03 : ^^^s = void;                                                     \n\
let foo04 : ^^^^s = void;                                                    \n\
let foo05 : ^^^^^s = void;                                                   \n\
let foo06 : ^^^^^^s = void;                                                  \n\
let foo07 : ^^^^^^^s = void;                                                 \n\
let foo08 : ^^^^^^^^s = void;                                                \n\
let foo09 : ^^^^^^^^^s = void;                                               \n\
let foo10 : ^^^^^^^^^^s = void;                                              \n\
let foo11 : ^^^^^^^^^^^s = void;                                             \n\
let foo12 : ^^^^^^^^^^^^s = void;                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(variable_declarations__pointer__valid)
{
    EMU_ADD(variable_declarations__minimum_required_number_of_pointer_modifiers);
    EMU_END_GROUP();
}

EMU_GROUP(variable_declarations__pointer)
{
    EMU_ADD(variable_declarations__pointer__valid);
    EMU_END_GROUP();
}

EMU_TEST(variable_declarations__minimum_required_number_of_array_modifiers)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
let foo01 : [1u]s = void;                                                    \n\
let foo02 : [1u][2u]s = void;                                                \n\
let foo03 : [1u][2u][3u]s = void;                                            \n\
let foo04 : [1u][2u][3u][4u]s = void;                                        \n\
let foo05 : [1u][2u][3u][4u][5u]s = void;                                    \n\
let foo06 : [1u][2u][3u][4u][5u][6u]s = void;                                \n\
let foo07 : [1u][2u][3u][4u][5u][6u][7u]s = void;                            \n\
let foo08 : [1u][2u][3u][4u][5u][6u][7u][8u]s = void;                        \n\
let foo09 : [1u][2u][3u][4u][5u][6u][7u][8u][9u]s = void;                    \n\
let foo10 : [1u][2u][3u][4u][5u][6u][7u][8u][9u][10u]s = void;               \n\
let foo11 : [1u][2u][3u][4u][5u][6u][7u][8u][9u][10u][11u]s = void;          \n\
let foo12 : [1u][2u][3u][4u][5u][6u][7u][8u][9u][10u][11u][12u]s = void;     \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(variable_declarations__array__valid)
{
    EMU_ADD(variable_declarations__minimum_required_number_of_array_modifiers);
    EMU_END_GROUP();
}

EMU_TEST(variable_declarations__array_with_length_zero)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
let foo : [0u]s = void;                                                      \n\
    ");

    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
let foo : [1u][0u]s = void;                                                  \n\
    ");

    EMU_END_TEST();
}

EMU_TEST(variable_declarations__array_with_qualifiers)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
let foo : mut [0u]s = void;                                                  \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(variable_declarations__array__invalid)
{
    EMU_ADD(variable_declarations__array_with_length_zero);
    EMU_ADD(variable_declarations__array_with_qualifiers);
    EMU_END_GROUP();
}

EMU_GROUP(variable_declarations__array)
{
    EMU_ADD(variable_declarations__array__valid);
    EMU_ADD(variable_declarations__array__invalid);
    EMU_END_GROUP();
}

EMU_GROUP(variable_declarations__modifiers)
{
    EMU_ADD(variable_declarations__pointer);
    EMU_ADD(variable_declarations__array);
    EMU_END_GROUP();
}

EMU_TEST(variable_declarations__with_identifer_starting_with_keyword__should_succeed)
{
    COMPILE_TEST_VALID_MODULE("let lettuce : s = void;  ");
    COMPILE_TEST_VALID_MODULE("let let_tucee : s = void;  ");
    EMU_END_TEST();
}

EMU_GROUP(compile__variable_declarations)
{
    EMU_ADD(variable_declarations__integers);
    EMU_ADD(variable_declarations__bool);
    EMU_ADD(variable_declarations__ascii);
    EMU_ADD(variable_declarations__struct);
    EMU_ADD(variable_declarations__function);
    EMU_ADD(variable_declarations__modifiers);
    EMU_ADD(variable_declarations__with_identifer_starting_with_keyword__should_succeed);
    EMU_END_GROUP();
}
