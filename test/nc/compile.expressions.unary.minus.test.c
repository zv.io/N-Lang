/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(unary_minus__integer_variables__s8)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s8 = void;                                                       \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__integer_variables__s16)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s16 = void;                                                      \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__integer_variables__s32)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s32 = void;                                                      \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__integer_variables__s64)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s64 = void;                                                      \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__integer_variables__s)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s = void;                                                        \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_minus__signed_integer_variables)
{
    EMU_ADD(unary_minus__integer_variables__s8);
    EMU_ADD(unary_minus__integer_variables__s16);
    EMU_ADD(unary_minus__integer_variables__s32);
    EMU_ADD(unary_minus__integer_variables__s64);
    EMU_ADD(unary_minus__integer_variables__s);
    EMU_END_GROUP();
}

EMU_TEST(unary_minus__float_variables__f32)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : f32 = void;                                                      \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__float_variables__f64)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : f64 = void;                                                      \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_minus__float_variables)
{
    EMU_ADD(unary_minus__float_variables__f32);
    EMU_ADD(unary_minus__float_variables__f64);
    EMU_END_GROUP();
}

EMU_TEST(unary_minus__chained_minus)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s = void;                                                        \n\
    - -x;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_minus__valid)
{
    EMU_ADD(unary_minus__signed_integer_variables);
    EMU_ADD(unary_minus__float_variables);
    EMU_ADD(unary_minus__chained_minus);
    EMU_END_GROUP();
}

EMU_TEST(unary_minus__integer_variables__u8)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u8 = void;                                                       \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__integer_variables__u16)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u16 = void;                                                      \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__integer_variables__u32)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u32 = void;                                                      \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__integer_variables__u64)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u64 = void;                                                      \n\
    -x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_minus__integer_variables__u)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u = void;                                                        \n\
    -x  ;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_minus__unsigned_integer_variables)
{
    EMU_ADD(unary_minus__integer_variables__u8);
    EMU_ADD(unary_minus__integer_variables__u16);
    EMU_ADD(unary_minus__integer_variables__u32);
    EMU_ADD(unary_minus__integer_variables__u64);
    EMU_ADD(unary_minus__integer_variables__u);
    EMU_END_GROUP();
}

EMU_GROUP(unary_minus__invalid)
{
    EMU_ADD(unary_minus__unsigned_integer_variables);
    EMU_END_GROUP();
}

EMU_GROUP(unary_minus)
{
    EMU_ADD(unary_minus__valid);
    EMU_ADD(unary_minus__invalid);
    EMU_END_GROUP();
}
