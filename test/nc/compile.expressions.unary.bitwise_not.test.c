/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(unary_bitwise_not__integer_variables__u8)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u8 = void;                                                       \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__u16)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u16 = void;                                                      \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__u32)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u32 = void;                                                      \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__u64)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u64 = void;                                                      \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__u)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : u = void;                                                        \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__s8)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s8 = void;                                                       \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__s16)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s16 = void;                                                      \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__s32)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s32 = void;                                                      \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__s64)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s64 = void;                                                      \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_variables__s)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let x : s = void;                                                        \n\
    ~x;                                                                      \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_bitwise_not__integer_variables)
{
    EMU_ADD(unary_bitwise_not__integer_variables__u8);
    EMU_ADD(unary_bitwise_not__integer_variables__u16);
    EMU_ADD(unary_bitwise_not__integer_variables__u32);
    EMU_ADD(unary_bitwise_not__integer_variables__u64);
    EMU_ADD(unary_bitwise_not__integer_variables__u);
    EMU_ADD(unary_bitwise_not__integer_variables__s8);
    EMU_ADD(unary_bitwise_not__integer_variables__s16);
    EMU_ADD(unary_bitwise_not__integer_variables__s32);
    EMU_ADD(unary_bitwise_not__integer_variables__s64);
    EMU_ADD(unary_bitwise_not__integer_variables__s);
    EMU_END_GROUP();
}

EMU_TEST(unary_bitwise_not__integer_literal__u8)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10u8;                                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__u16)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10u16;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__u32)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10u32;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__u64)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10u64;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__u)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10u;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__s8)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10s8;                                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__s16)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10s16;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__s32)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10s32;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__s64)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10s64;                                                                  \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__integer_literal__s)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~10s;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_bitwise_not__integer_literals)
{
    EMU_ADD(unary_bitwise_not__integer_literal__u8);
    EMU_ADD(unary_bitwise_not__integer_literal__u16);
    EMU_ADD(unary_bitwise_not__integer_literal__u32);
    EMU_ADD(unary_bitwise_not__integer_literal__u64);
    EMU_ADD(unary_bitwise_not__integer_literal__u);
    EMU_ADD(unary_bitwise_not__integer_literal__s8);
    EMU_ADD(unary_bitwise_not__integer_literal__s16);
    EMU_ADD(unary_bitwise_not__integer_literal__s32);
    EMU_ADD(unary_bitwise_not__integer_literal__s64);
    EMU_ADD(unary_bitwise_not__integer_literal__s);
    EMU_END_GROUP();
}

EMU_GROUP(unary_bitwise_not__valid)
{
    EMU_ADD(unary_bitwise_not__integer_variables);
    EMU_ADD(unary_bitwise_not__integer_literals);
    EMU_END_GROUP();
}

EMU_TEST(unary_bitwise_not__bool_variable)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let foo : bool = void;                                                   \n\
    ~foo;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__bool_literal)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~true;                                                                   \n\
}                                                                            \n\
    ");

    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~false;                                                                  \n\
}                                                                            \n\
    ");

    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__ascii_variable)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let foo : ascii = void;                                                  \n\
    ~foo;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__ascii_literal)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~'A'a;                                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__string_literal)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    ~\"foobar\"a;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__array_variable)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let foo : [10u]s = void;                                                 \n\
    ~foo;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(unary_bitwise_not__pointer_variable)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void                                                        \n\
{                                                                            \n\
    let foo : ^s = void;                                                     \n\
    ~foo;                                                                    \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(unary_bitwise_not__invalid)
{
    EMU_ADD(unary_bitwise_not__bool_variable);
    EMU_ADD(unary_bitwise_not__bool_literal);
    EMU_ADD(unary_bitwise_not__ascii_variable);
    EMU_ADD(unary_bitwise_not__ascii_literal);
    EMU_ADD(unary_bitwise_not__string_literal);
    EMU_ADD(unary_bitwise_not__array_variable);
    EMU_ADD(unary_bitwise_not__pointer_variable);
    EMU_END_GROUP();
}

EMU_GROUP(unary_bitwise_not)
{
    EMU_ADD(unary_bitwise_not__valid);
    EMU_ADD(unary_bitwise_not__invalid);
    EMU_END_GROUP();
}
