/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(subscript__array_access_element)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [10u]s = void;                                                 \n\
    foo[0u];                                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(subscript__array_access_is_lvalue)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : [10u]mut s = void;                                             \n\
    foo[0u] = 5s;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(subscript__pointer_access_element)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : ^mut s = void;                                                 \n\
    foo[0u];                                                                 \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(subscript__pointer_access_is_lvalue)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : ^mut s = void;                                                 \n\
    foo[0u] = 5s;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(subscript__valid)
{
    EMU_ADD(subscript__array_access_element);
    EMU_ADD(subscript__array_access_is_lvalue);
    EMU_ADD(subscript__pointer_access_element);
    EMU_ADD(subscript__pointer_access_is_lvalue);
    EMU_END_GROUP();
}

EMU_GROUP(postfix__subscript)
{
    EMU_ADD(subscript__valid);
    EMU_END_GROUP();
}
