/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(break__within_loop)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
let total : mut u = 0u;                                                      \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    loop i : mut u in [0u, 10u)                                              \n\
    {                                                                        \n\
        if i == 5u                                                           \n\
        {                                                                    \n\
            break;                                                           \n\
        }                                                                    \n\
        total =  total + i;                                                  \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(break__should_bypass_unexecuted_defer_statement)
{
    // This test only makes sure this program compiles. Eventually we may add
    // testing of programs from within a shell to make sure this program never
    // actually prints anything but for now it will have to be tested manually.
    COMPILE_TEST_VALID_PROG("                                                \n\
import \"cstd/stdio.n\"a;                                                    \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    loop i : mut u in [0u, 10u)                                              \n\
    {                                                                        \n\
        defer {puts($\"This should be printed!\"a);}                         \n\
        break;                                                               \n\
        defer {puts($\"This should never be printed!\"a);}                   \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(break__valid)
{
    EMU_ADD(break__within_loop);
    EMU_ADD(break__should_bypass_unexecuted_defer_statement);
    EMU_END_GROUP();
}

EMU_TEST(break__outside_of_loop)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    break;                                                                   \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(break__within_deferred_scope__primary)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    defer{break;}                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(break__within_deferred_scope__nested)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    defer{defer{break;}}                                                     \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(break__invalid)
{
    EMU_ADD(break__outside_of_loop);
    EMU_ADD(break__within_deferred_scope__primary);
    EMU_ADD(break__within_deferred_scope__nested);
    EMU_END_GROUP();
}

EMU_GROUP(compile__break_statements)
{
    EMU_ADD(break__valid);
    EMU_ADD(break__invalid);
    EMU_END_GROUP();
}
