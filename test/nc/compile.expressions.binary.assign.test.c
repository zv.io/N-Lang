/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(binary_compatible_assignment_to_u8)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u8 = void;                                                 \n\
    foo = 10u8;                                                              \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_u16)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u16 = void;                                                \n\
    foo = 10u16;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_u32)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u32 = void;                                                \n\
    foo = 10u32;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_u64)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u64 = void;                                                \n\
    foo = 10u64;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_u)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10u;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_s8)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut s8 = void;                                                 \n\
    foo = 10s8;                                                              \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_s16)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut s16 = void;                                                \n\
    foo = 10s16;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_s32)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut s32 = void;                                                \n\
    foo = 10s32;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_s64)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut s64 = void;                                                \n\
    foo = 10s64;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_s)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut s = void;                                                  \n\
    foo = 10s;                                                               \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_compatible_assignment_to_integer_base_types)
{
    EMU_ADD(binary_compatible_assignment_to_u8);
    EMU_ADD(binary_compatible_assignment_to_u16);
    EMU_ADD(binary_compatible_assignment_to_u32);
    EMU_ADD(binary_compatible_assignment_to_u64);
    EMU_ADD(binary_compatible_assignment_to_u);
    EMU_ADD(binary_compatible_assignment_to_s8);
    EMU_ADD(binary_compatible_assignment_to_s16);
    EMU_ADD(binary_compatible_assignment_to_s32);
    EMU_ADD(binary_compatible_assignment_to_s64);
    EMU_ADD(binary_compatible_assignment_to_s);
    EMU_END_GROUP();
}

EMU_TEST(binary_compatible_assignment_to_f32)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut f32 = void;                                                \n\
    foo = 10.0f32;                                                           \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_f64)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut f64 = void;                                                \n\
    foo = 10.0f64;                                                           \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_compatible_assignment_to_float_base_types)
{
    EMU_ADD(binary_compatible_assignment_to_f32);
    EMU_ADD(binary_compatible_assignment_to_f64);
    EMU_END_GROUP();
}

EMU_TEST(binary_compatible_assignment_to_bool_base_type)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = true;                                                              \n\
    foo = false;                                                             \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(binary_compatible_assignment_to_ascii_base_type)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut ascii = void;                                              \n\
    foo = '\\0'a;                                                            \n\
    foo = 't'a;                                                              \n\
    foo = '\\t'a;                                                            \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(binary_assign__valid)
{
    EMU_ADD(binary_compatible_assignment_to_integer_base_types);
    EMU_ADD(binary_compatible_assignment_to_float_base_types);
    EMU_ADD(binary_compatible_assignment_to_bool_base_type);
    EMU_ADD(binary_compatible_assignment_to_ascii_base_type);
    EMU_END_GROUP();
}

EMU_TEST(binary_incompatible_assignment_to_integer_base_types)
{
    // Testing every base type combination is an O(N^2) problem, so just test
    // with LHS u.
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10u8;                                                              \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10u16;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10u32;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10u32;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10u64;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10s8;                                                              \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10s16;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10s32;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10s64;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = 10s;                                                               \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut u = void;                                                  \n\
    foo = true;                                                              \n\
}                                                                            \n\
    ");

    EMU_END_TEST();
}

EMU_TEST(binary_incompatible_assignment_to_bool_base_type)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10u8;                                                              \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10u16;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10u32;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10u64;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10u;                                                               \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10s8;                                                              \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10s16;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10s32;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10s64;                                                             \n\
}                                                                            \n\
    ");

    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let foo : mut bool = void;                                               \n\
    foo = 10s;                                                               \n\
}                                                                            \n\
    ");

    EMU_END_TEST();
}

EMU_TEST(binary_incompatible_assignment_after_global_variable_declaration)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
let foo : mut s = void;                                                      \n\
foo = 5s;                                                                    \n\
    ");

    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
let bar : mut s = 0s;                                                        \n\
bar = 5s;                                                                    \n\
    ");

    EMU_END_TEST();
}

EMU_GROUP(binary_assign__invalid)
{
    EMU_ADD(binary_incompatible_assignment_to_integer_base_types);
    EMU_ADD(binary_incompatible_assignment_to_bool_base_type);
    EMU_ADD(binary_incompatible_assignment_after_global_variable_declaration);
    EMU_END_GROUP();
}

EMU_GROUP(binary_assign)
{
    EMU_ADD(binary_assign__valid);
    EMU_ADD(binary_assign__invalid);
    EMU_END_GROUP();
}
