/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_TEST(function_declarations__single_forward_declaration)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void;                                                       \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(function_declarations__multiple_forward_declarations)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void;                                                       \n\
func foo : () -> void;                                                       \n\
    ");

    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void;                                                       \n\
func foo : () -> void;                                                       \n\
func foo : () -> void;                                                       \n\
    ");

    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void;                                                       \n\
func foo : (   ) -> void;                                                    \n\
func foo :   ( ) -> void ;                                                   \n\
func foo : () -> void;                                                       \n\
    ");

    EMU_END_TEST();
}

EMU_TEST(function_declarations__declarations_with_definition_last)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : () -> void;                                                       \n\
func foo : () -> void {}                                                     \n\
    ");

    EMU_END_TEST();
}

EMU_GROUP(function_declarations__valid)
{
    EMU_ADD(function_declarations__single_forward_declaration);
    EMU_ADD(function_declarations__multiple_forward_declarations);
    EMU_ADD(function_declarations__declarations_with_definition_last);
    EMU_END_GROUP();
}

EMU_TEST(function_declarations__declarations_with_definition_not_last)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : () -> void {}                                                     \n\
func foo : () -> void;                                                       \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(function_declarations__definition_with_unclosed_curly_paren)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                     \n\
func entry : () -> s {                                                       \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(function_declarations__declaration_with_non_function_type)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                   \n\
func foo : s;                                                                \n\
    ");

    COMPILE_TEST_MODULE(COMPILE_FAILURE_SYNTAX_ANALYSIS, "                   \n\
func foo : s {}                                                              \n\
    ");

    EMU_END_TEST();
}

EMU_TEST(function_declarations__declaration_with_pass_by_value_array_param)
{
    COMPILE_TEST_MODULE(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                 \n\
func foo : (bar : [10u]s) -> void {}                                         \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(function_declarations__invalid)
{
    EMU_ADD(function_declarations__declarations_with_definition_not_last);
    EMU_ADD(function_declarations__definition_with_unclosed_curly_paren);
    EMU_ADD(function_declarations__declaration_with_non_function_type);
    EMU_ADD(function_declarations__declaration_with_pass_by_value_array_param);
    EMU_END_GROUP();
}

EMU_TEST(function_definition__single_param__void_return__empty_body)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : (some_param : mut s) -> void {}                                   \n\
func bar : (some_param : s) -> void {}                                       \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(function_definition__single_param__non_void_return__simple_body)
{
    COMPILE_TEST_VALID_MODULE("                                              \n\
func foo : (some_param : s) -> s                                             \n\
{                                                                            \n\
    return some_param;                                                       \n\
}                                                                            \n\
    ");

    COMPILE_TEST_VALID_MODULE("                                              \n\
func incr : (some_param : u) -> u                                            \n\
{                                                                            \n\
    return some_param + 1u;                                                  \n\
}                                                                            \n\
    ");

    EMU_END_TEST();
}

EMU_GROUP(function_definition_samples)
{
    EMU_ADD(function_definition__single_param__void_return__empty_body);
    EMU_ADD(function_definition__single_param__non_void_return__simple_body);
    EMU_END_GROUP();
}

EMU_GROUP(compile__function_declarations)
{
    EMU_ADD(function_declarations__valid);
    EMU_ADD(function_declarations__invalid);
    EMU_ADD(function_definition_samples);
    EMU_END_GROUP();
}
