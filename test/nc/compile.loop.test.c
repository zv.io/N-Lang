/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"
EMU_TEST(loop__conditional)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let sum : mut u = 0u;                                                    \n\
    let i : mut u = 0u;                                                      \n\
    loop i < 10u                                                             \n\
    {                                                                        \n\
        sum = sum + 1u;                                                      \n\
        i = i + 1u;                                                          \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}
EMU_TEST(loop__range__inclusive_inclusive)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let sum : mut u = 0u;                                                    \n\
    loop i : mut u in [0u, 10u]                                              \n\
    {                                                                        \n\
        sum = sum + 1u;                                                      \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(loop__range__inclusive_exclusive)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let sum : mut u = 0u;                                                    \n\
    loop i : mut u in [0u, 10u)                                              \n\
    {                                                                        \n\
        sum = sum + 1u;                                                      \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(loop__range__exclusive_inclusive)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let sum : mut u = 0u;                                                    \n\
    loop i : mut u in (0u, 10u]                                              \n\
    {                                                                        \n\
        sum = sum + 1u;                                                      \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_TEST(loop__range__exclusive_exclusive)
{
    COMPILE_TEST_VALID_PROG("                                                \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    let sum : mut u = 0u;                                                    \n\
    loop i : mut u in (0u, 10u)                                              \n\
    {                                                                        \n\
        sum = sum + 1u;                                                      \n\
    }                                                                        \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(loop__range__valid)
{
    EMU_ADD(loop__range__inclusive_inclusive);
    EMU_ADD(loop__range__inclusive_exclusive);
    EMU_ADD(loop__range__exclusive_inclusive);
    EMU_ADD(loop__range__exclusive_exclusive);
    EMU_END_GROUP();
}

EMU_TEST(loop__range__non_mut_loop_variable)
{
    COMPILE_TEST_PROG(COMPILE_FAILURE_SEMANTIC_ANALYSIS, "                   \n\
func entry : () -> void                                                      \n\
{                                                                            \n\
    loop i : u in (0u, 10u){}                                                \n\
}                                                                            \n\
    ");
    EMU_END_TEST();
}

EMU_GROUP(loop__range__invalid)
{
    EMU_ADD(loop__range__non_mut_loop_variable);
    EMU_END_GROUP();
}

EMU_GROUP(loop__range)
{
    EMU_ADD(loop__range__valid);
    EMU_ADD(loop__range__invalid);
    EMU_END_GROUP();
}

EMU_GROUP(compile__loop)
{
    EMU_ADD(loop__conditional);
    EMU_ADD(loop__range);
    EMU_END_GROUP();
}
