/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nc.testutils.h"

EMU_DECLARE(unary_address_of);
EMU_DECLARE(unary_dereference);
EMU_DECLARE(unary_plus);
EMU_DECLARE(unary_minus);
EMU_DECLARE(unary_bitwise_not);
EMU_DECLARE(unary_logical_not);
EMU_DECLARE(unary_countof);
EMU_DECLARE(unary_sizeof);
EMU_DECLARE(unary_typeof);

EMU_GROUP(expressions__unary)
{
    EMU_ADD(unary_address_of);
    EMU_ADD(unary_dereference);
    EMU_ADD(unary_plus);
    EMU_ADD(unary_minus);
    EMU_ADD(unary_bitwise_not);
    EMU_ADD(unary_logical_not);
    EMU_ADD(unary_countof);
    EMU_ADD(unary_sizeof);
    EMU_ADD(unary_typeof);
    EMU_END_GROUP();
}
