/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define _EMU_ENABLE_COLOR_
#include <nutils/nutils.h>
#include <nc/config.h>
#include <EMU/EMUtest.h>

EMU_DECLARE(primitives_h);
EMU_DECLARE(nir_h);
EMU_DECLARE(compile_h);
EMU_DECLARE(std);

EMU_GROUP(nc)
{
    EMU_ADD(primitives_h);
    EMU_ADD(nir_h);
    EMU_ADD(compile_h);
    EMU_ADD(std);
    EMU_END_GROUP();
}

__attribute__((constructor))
void nc_test_constructor(void)
{
    install_generic_sighandler();
}

void nc_tests_print_usage(void)
{
    fprintf(
        stdout,
        "Usage: nc.tests [options...]\n"
        "Options:\n"
        "  -d, --debug      Run the tests debug mode.\n"
        "  -e, --error-all  Show error and fatal strings for all tests.\n"
    );
}

bool is_error_all = false;

static char const* opts_str = "de";
static struct option opts_long[] = {
    {"debug",     no_argument, 0, 'd'},
    {"error-all", no_argument, 0, 'e'},
    {0, 0, 0, 0}
};
stbool nc_tests_parse_args(int argc, char** argv)
{
    int option_index;
    int option;

    for (int i = 0; i < argc; ++i)
    {
        if (cstr_eq(argv[i], "-h") || cstr_eq(argv[i], "--help"))
        {
            return STBOOL_FAILURE;
        }
    }

    LOOP_FOREVER
    {
        option = getopt_long(argc, argv, opts_str, opts_long, &option_index);
        if (-1 == option)
        {
            break;
        }

        switch (option)
        {
        case 'd':
            if (config.is_debug_mode)
            {
                nlogf(LOG_ERROR, "Debug mode flag already set.");
                return STBOOL_FAILURE;
            }
            config.is_debug_mode = true;
            break;

        case 'e':
            if (is_error_all)
            {
                nlogf(LOG_ERROR, "Error-all flag already set.");
                return STBOOL_FAILURE;
            }
            is_error_all = true;
            break;

        case ':': /* intentional fallthrough */
        case '?':
            return STBOOL_FAILURE;

        default:
            NPANIC_DFLT_CASE();
        }
    }

    return STBOOL_SUCCESS;
}

int main(int argc, char** argv)
{
    if (!nc_tests_parse_args(argc, argv))
    {
        nc_tests_print_usage();
        exit(EXIT_FAILURE);
    }

    return EMU_RUN(nc);
}
