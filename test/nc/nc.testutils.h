/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include <EMU/EMUtest.h>
#include <nc/config.h>
#include <nc/compile.h>

extern bool is_error_all;

static char ATTR_WARN_UNUSED_VARIABLE _infile_name[512];
static char ATTR_WARN_UNUSED_VARIABLE _outfile_name[512];
static FILE* ATTR_WARN_UNUSED_VARIABLE _fp = NULL;

#define COMPILED_OUTPUT_PATH "build/compiled/"
#define EXAMPLES_SRC_PATH "examples/"

#define COMPILE_TEST_BEGIN() {
#define COMPILE_TEST_END()   }

#define _COMPILE_TEST_SETUP_ASSIGN_INFILE()                                    \
    strcpy(_infile_name, COMPILED_OUTPUT_PATH);                                \
    strcat(_infile_name, _EMU_TEST_NAME);                                      \
    strcat(_infile_name, ".n");

#define _COMPILE_TEST_SETUP_ASSIGN_OUTFILE(contains_entry)                     \
    strcpy(_outfile_name, COMPILED_OUTPUT_PATH);                               \
    strcat(_outfile_name, _EMU_TEST_NAME);                                     \
    if (!contains_entry){strcat(_outfile_name, ".o");}

#define _COMPILE_TEST_SETUP_WRITE_SRC_TO_FILE(src_cstr)                        \
    COMPILE_WRITE_CSTR_TO_FILE(_infile_name, src_cstr)

#define _COMPILE_TEST_SETUP_CONFIG(is_valid, contains_entry)                   \
    config.target = TARGET_NATIVE;                                             \
    config.is_gen_debug_info = true;                                           \
    config.is_compile_only = !contains_entry;                                  \
    config.is_save_temps = true;                                               \
    config.input_file = _infile_name;                                          \
    config.output_file = _outfile_name;                                        \
    log_config = (LOG_DEBUG * config.is_debug_mode)                            \
        | LOG_INFO                                                             \
        | LOG_WARNING                                                          \
        | (LOG_ERROR * (is_valid || (!is_valid && is_error_all)))              \
        | (LOG_FATAL * (is_valid || (!is_valid && is_error_all)));             \

#define _COMPILE_TEST_SETUP_MODULE()                                           \
    struct module input_module;                                                \
    module_init(&input_module);

#define COMPILE_TEST_SETUP(src_cstr, is_valid, contains_entry)                 \
    COMPILE_TEST_BEGIN()                                                       \
    _COMPILE_TEST_SETUP_ASSIGN_INFILE()                                        \
    _COMPILE_TEST_SETUP_ASSIGN_OUTFILE(contains_entry)                         \
    _COMPILE_TEST_SETUP_WRITE_SRC_TO_FILE(src_cstr)                            \
    _COMPILE_TEST_SETUP_CONFIG(is_valid, contains_entry)                       \
    _COMPILE_TEST_SETUP_MODULE()

#define COMPILE_TEST_SETUP_MODULE(src_cstr, is_valid) \
    COMPILE_TEST_SETUP(src_cstr, is_valid, false)
#define COMPILE_TEST_SETUP_PROG(src_cstr, is_valid) \
    COMPILE_TEST_SETUP(src_cstr, is_valid, true)

#define COMPILE_TEST_SETUP_VALID_MODULE(src_cstr) \
    COMPILE_TEST_SETUP_MODULE(src_cstr, true)
#define COMPILE_TEST_SETUP_VALID_PROG(src_cstr) \
    COMPILE_TEST_SETUP_PROG(src_cstr, true)

#define COMPILE_TEST_MODULE(expected_compile_rtn, src_cstr) \
    COMPILE_TEST_SETUP_MODULE(src_cstr, !expected_compile_rtn); \
    EMU_EXPECT_EQ_INT(expected_compile_rtn, compilep(_infile_name, &input_module)); \
    COMPILE_TEST_END()
#define COMPILE_TEST_PROG(expected_compile_rtn, src_cstr) \
    COMPILE_TEST_SETUP_PROG(src_cstr, !expected_compile_rtn); \
    EMU_EXPECT_EQ_INT(expected_compile_rtn, compilep(_infile_name, &input_module)); \
    COMPILE_TEST_END()

#define COMPILE_TEST_VALID_MODULE(src_cstr) \
    COMPILE_TEST_MODULE(COMPILE_SUCCESS, src_cstr)
#define COMPILE_TEST_VALID_PROG(src_cstr) \
    COMPILE_TEST_PROG(COMPILE_SUCCESS, src_cstr)

#define COMPILE_TEST_EXAMPLE_PROG(path_cstr)                                   \
    COMPILE_TEST_BEGIN()                                                       \
    strcpy(_infile_name, EXAMPLES_SRC_PATH path_cstr);                         \
    _COMPILE_TEST_SETUP_ASSIGN_OUTFILE(true)                                   \
    _COMPILE_TEST_SETUP_CONFIG(true, true)                                     \
    _COMPILE_TEST_SETUP_MODULE()                                               \
    EMU_EXPECT_EQ_INT(COMPILE_SUCCESS, compilep(_infile_name, &input_module)); \
    COMPILE_TEST_END()

#define COMPILE_WRITE_CSTR_TO_FILE(_filename, _cstr)                           \
    _fp = fopen(_filename, "w");                                               \
    fprintf(_fp, "%s", _cstr);                                                 \
    fclose(_fp);
