/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "_base.h"

//! Generic signal handler that should report most erroneous signals and exit
//! appropriately.
void generic_sighandler(int signum);

//! Install #generic_sighandler as the defualt `sigaction` handler for most
//! signals. Intended to be run before or just shortly after control reaches
//! `main`.
void install_generic_sighandler(void);
