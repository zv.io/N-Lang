/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "_base.h"

//! malloc-like allocator. Returns a non-`NULL` pointer or causes a panic.
//! @param size
//!     Number of bytes to allocate.
//! @return
//!     Pointer to the start of the allocated memory.
//! @note
//!     #nalloc will never return `NULL`.
//! @note
//!     #nalloc does NOT zero the memory it returns.
void* nalloc(size_t size);

#define NALLOC_SIZEOF_TYPEOF(p_variable) nalloc(sizeof(typeof(*(p_variable))))

//! realloc-like allocator. Returns a non-`NULL` pointer or causes a panic.
//! @param ptr
//!     Pointer to the start of a memory block allocated with #nalloc or
//!     #nrealloc.
//! @param size
//!     New size of the reallocated memory block in bytes.
//! @return
//!     Pointer to the start of the reallocated memory.
//! @note
//!     #nrealloc will never return `NULL`.
//! @note
//!     #nalloc does NOT zero newly allocated memory memory block.
//! @warning
//!     Do not call #nrealloc on memory allocated with `malloc` or `realloc`.
void* nrealloc(void* ptr, size_t size);

//! Free memory allocated with #nalloc or #nrealloc.
//! @param ptr
//!     Pointer to the start of a memory block allocated with #nalloc or
//!     #nrealloc.
//! @warning
//!     Do not call #nfree on memory allocated with `malloc` or `realloc`.
void nfree(void* ptr);

//! Swap two objects of a given size in memory.
//! @param p1
//!     Pointer to the first object.
//! @param p2
//!     Pointer to the second object.
//! @param size
//!     `sizeof` @p p1 and @p p2.
void memswap(void* p1, void* p2, size_t size);
