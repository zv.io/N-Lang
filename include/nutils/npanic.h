/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "_base.h"

//! Call when an unexpected/unrecoverable error has occurred.
//! Prints a formatted message and backtrace to `stderr` before calling `exit`
//! with argument `EXIT_FAILURE`.
//! @param fmt
//!     printf-style format string.
//! @param ...
//!     Arguments to the format specifiers of @p fmt.
void npanic(char const* fmt, ...)
    __attribute__((noreturn));

#define _NPANIC_DEBUG_FILE_LINE \
    "\n    FILE: " __FILE__ \
    "\n    LINE: " STRINGIFY(__LINE__)

//! Panic because a default case in a `switch` was reached that should never
//! have been reached.
#define NPANIC_DFLT_CASE() \
    npanic("switch failed to match a non-default case" _NPANIC_DEBUG_FILE_LINE)

//! Panic because an "unset" enum was hit in a switch.
#define NPANIC_UNSET_CASE() \
    npanic("switch reached an unexpected \"unset\" enum" _NPANIC_DEBUG_FILE_LINE)

//! Panic because control should never have reached this macro.
#define NPANIC_UNEXPECTED_CTRL_FLOW() \
    npanic("unexpected control flow" _NPANIC_DEBUG_FILE_LINE)

//! Panic because we were too lazy to implement something and it turns out it is
//! needed.
#define NPANIC_TODO() \
    npanic("TODO" _NPANIC_DEBUG_FILE_LINE)

//! Panic because a section of code relying on a particular feature has not yet
//! been implemented.
#define NPANIC_NOT_YET_IMPL(feature) \
    npanic(STRINGIFY(feature) " not yet implemented" _NPANIC_DEBUG_FILE_LINE)

//! Panic due to an out of bounds error.
#define NPANIC_OUT_OF_BOUNDS_ERROR() \
    npanic("out of Bounds error" _NPANIC_DEBUG_FILE_LINE)

