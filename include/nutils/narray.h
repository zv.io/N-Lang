/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "_base.h"

//! Container-style type that provides a way to explicitly state that an array
//! is an narray.
#define narr_t(T) T*

//! Allocate an array of @p numelem elements each of size @p elemsize.
//! @param numelem
//!     The initial number of elements in the narray.
//! @param elemsize
//!     The `sizeof` elements in the narray.
//! @return
//!     Heap allocated narray with length @p numelem.
void* narr_alloc(size_t numelem, size_t elemsize)
    __attribute__((warn_unused_result));

//! Free narray @p narr allocated with #narr_alloc.
//! @param narr
//!     Target narray.
void narr_free(void* narr);

//! Returns the length (number of elements) in target narray.
//! @param narr
//!     Target narray.
//! @return
//!     Length of @p narr.
size_t narr_length(void const* narr);

//! Returns the maximum number of elements target narray can hold without
//! requiring memory reallocation.
//! @param narr
//!     Target narray.
//! @return
//!     Capacity of @p narr.
size_t narr_capacity(void const* narr);

//! Returns the `sizeof` elements in target narray.
//! @param narr
//!     Target narray.
//! @return
//!     The `sizeof` elements in @p narr.
size_t narr_sizeof_elem(void const* narr);

//! Guarantee that at least @p numelem elements beyond the current length of
//! narray @p narr can be inserted/pushed without requiring memory reallocation.
//! @param narr
//!     Target narray.
//! @param numelem
//!     Number of elements to be reserved.
//! @return
//!     @p narr with reserved space.
//! @note
//!     Does NOT change the length of @p narr.
//! @note
//!     May change the capacity of @p narr.
void* narr_reserve(void* narr, size_t numelem)
    __attribute__((warn_unused_result));

//! Change the length of a narray @p narr to @p len.
//! @param narr
//!     Target narray.
//! @param len
//!     New length of the narray.
//! @return
//!     @p narr with changed length.
void* narr_resize(void* narr, size_t len)
    __attribute__((warn_unused_result));

//! Append the object pointed to by @p ptr_to_val to the back of narray @p narr.
//! @param narr
//!     Target narray.
//! @param ptr_to_val
//!     Pointer to an object type-compatible with the elements of @p narr.
//! @return
//!     @p narr who's last value is a copy of *@p ptr_to_val.
//! @note
//!     Increases the length of @p narr by 1.
//! @note
//!     *@p ptr_to_val is memory copied to the back of @p narr. If you wish to
//!     create a duplicate of the object pointed to by *@p ptr_to_val with its
//!     own resources you must manually duplicate *@p ptr_to_val before pushing.
void* narr_push(void* narr, void const* ptr_to_val)
    __attribute__((warn_unused_result));

//! Remove the last element off the back of @p narr.
//! @param narr
//!     Target narray.
//! @note
//!     Decreases the length of @p narr by 1.
void narr_pop(void* narr);

//! Prepend the object pointed to by @p ptr_to_val to the front of narray @p
//! narr.
//! @param narr
//!     Target narray.
//! @param ptr_to_val
//!     Pointer to an object type-compatible with the elements of @p narr.
//! @return
//!     @p narr who's first value is a copy of *@p ptr_to_val.
//! @note
//!     Increases the length of @p narr by 1.
//! @note
//!     *@p ptr_to_val is memory copied to the back of @p narr. If you wish to
//!     create a duplicate of the object pointed to by *@p ptr_to_val with its
//!     own resources you must manually duplicate *@p ptr_to_val before
//!     unshifting.
void* narr_unshift(void* narr, void const* ptr_to_val)
    __attribute__((warn_unused_result));

//! Remove the first element off the front of @p narr.
//! @param narr
//!     Target narray.
//! @note
//!     Decreases the length of @p narr by 1.
void narr_shift(void* narr);

//! Get the first element in target narray.
//! @param narr
//!     Target narray.
//! @return
//!     Pointer to the first element of @p narr if it exists.
//! @return
//!     `NULL` if the @p narr has no first element.
void* narr_front(void const* narr);

//! Get the last element in target narray.
//! @param narr
//!     Target narray.
//! @return
//!     Pointer to the last element of @p narr if it exists.
//! @return
//!     `NULL` if @p narr has no last element.
void* narr_back(void const* narr);
