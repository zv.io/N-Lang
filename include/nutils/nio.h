/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "_base.h"
#include "nansi_escape.h"
#include "nstring.h"

#define FMT_FAILED_TO_LOCATE_FILE    "Failed to locate file '%s'."
#define FMT_FAILED_TO_READ_FROM_FILE "Failed to read from file '%s'."
#define FMT_FAILED_TO_WRITE_TO_FILE  "Failed to write to file '%s'."

#define LOG_DEBUG   (1 << 0) //!< Log level for debugging purposes.
#define LOG_METRIC  (1 << 1) //!< Log level for performance metrics.
#define LOG_INFO    (1 << 2) //!< Log level with no positive or negative connotation.
#define LOG_WARNING (1 << 3) //!< Log level indicating a warning.
#define LOG_ERROR   (1 << 4) //!< Log level indicating an error.
#define LOG_FATAL   (1 << 5) //!< Log level indicating an unrecoverable error.

//! Global logging configuration for the program, designed to be set once just
//! after application startup.
//! Assign to #log_config the logical OR of all the log levels you want #nlogf
//! and #nlogf_flc to write.
extern int log_config;
//! Default value of #log_config at application startup.
#define DEFAULT_LOG_CONFIG \
    (LOG_INFO | LOG_DEBUG | LOG_WARNING | LOG_ERROR | LOG_FATAL)

//! Pointer to the output file stream that all logging is written to.
extern FILE *const * log_stream;
//! Default value of #log_stream at application startup.
#define DEFAULT_LOG_STREAM (&stdout)

//! Log a message at the specified log level using printf-style formatting.
//! @param level
//!     Log level the message will be written at.
//! @param fmt
//!     printf-style format string.
//! @param ...
//!     Arguments to the format specifiers of @p fmt.
//! @note
//!     A newline is automatically appended to the format string.
void nlogf(
    int level,
    char const* fmt,
    ...
) __attribute__((format (printf, 2, 3)));
//! nlogf variant that prints `<file>:<line>:<column>` before the log-level
//! string. Used for printing a location in source code along with a message.
//! @param level
//!     Log level the message will be written at.
//! @param path
//!     File path string used for `<file>`.
//! @param line
//!     Used for `<line>`.
//! @param col
//!     Used for `<column>`.
//! @param fmt
//!     printf-style format string.
//! @param ...
//!     Arguments to the format specifiers of @p fmt.
//! @note
//!     A newline is automatically appended to the format string.
void nlogf_flc(
    int level,
    char const* path,
    size_t line,
    size_t col,
    char const* fmt,
    ...
) __attribute__((format (printf, 5, 6)));

//! Check the existence of a file.
//! @param path
//!     Relative path of the file.
//! @return
//!     `true` if the file exists.
bool file_exist(char const* path);

//! Check the size of a file in bytes.
//! @param path
//!     Relative path of the file.
//! @return
//!     The size of the file in bytes on success.
//! @return
//!     -1 on failure.
ssize_t file_size(char const* path);

//! Read all text from a file opened in binary mode.
//! @param path
//!     Relative path of the file.
//! @param buff
//!     Buffer to store the contents of the file.
//! @param buffsize
//!     Number of characters that @p buff can hold, including the null
//!     terminator.
//! @return
//!     Number of bytes read on success.
//! @return
//!     -1 on failure, including if @p buffsize was not large enough to hold the
//!     null terminated string containing the file's content.
ssize_t file_read_into_buff(char const* path, char* buff, size_t buffsize);

//! Read all text from a file opened in binary mode into an nstring.
//! @param path
//!     Relative path of the file.
//! @param nstr
//!     Target nstring.
//! @return
//!     Number of bytes read on success.
//! @return
//!     -1 on failure.
ssize_t file_read_into_nstr(char const* path, nstr_t* nstr);

//! Get the length of the formatted string (without its null terminator) using
//! printf-style formatting.
//! @param fmt
//!     printf-style format string.
//! @return
//!     Length of the formatted string without its null terminator on success.
//! @return
//!     -1 on failure.
//! @note
//!     A buffer of `fmt_len(format, ...args)+1` is exactly large enough to hold
//!     the contents of `sprintf(format, ...args)`.
ssize_t fmt_len(char const* fmt, ...);
