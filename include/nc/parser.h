/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "nir.h"
#include "token.h"
#include "compile.h"

#define PROD_SUCCESS      (0)
#define PROD_FAILURE_SOFT (1)
#define PROD_FAILURE_HARD (-1)

//! Match a terminal #token against the next #token in buffer pointed to by @p
//! next. If the token is matched `*next` is advanced by one #token.
//! @return
//!     #STBOOL_SUCCESS on successful match.
//! @return
//!     #STBOOL_FAILURE on unsuccessful match.
stbool match_terminal(struct token** next, enum token_type ttype);

void log_redeclaration_of_defined_identifier(
    struct symbol const* existing_symbol,
    struct id const* new_id
);

struct processing_context
{
    //! Pointer to the module of compilation.
    struct module* module;

    //! Current #scope that statements are being processed within.
    struct scope* curr_scope;

    //! The function that @p curr_scope is a part of.
    //! #NULL if @p curr_scope is not within a function.
    //! @note
    //!     Not necessarily the immediate child #scope of the function itself in
    //!     the case of additional nested scopes within the function.
    struct func_sig* curr_func;
};
//! @init
//! @param ctx
//!     Context to initialize.
//! @param module
//!     Target module of compilation.
void processing_context_init(
    struct processing_context* ctx,
    struct module* module
);
void processing_context_fini(
    struct processing_context* ctx
);

//============================================================================//
//      IDENTIFIER                                                            //
//============================================================================//
stbool prod_id(
    struct id* id,
    struct token** next
);
stbool proc_id(
    struct id* id,
    struct processing_context* ctx
);

//============================================================================//
//      QUALIFIERS                                                             //
//============================================================================//
stbool prod_qualifiers(
    struct qualifiers* q,
    struct token** next
);
stbool proc_qualifiers(
    struct qualifiers* q,
    struct processing_context* ctx
);

//============================================================================//
//      DATA TYPES                                                            //
//============================================================================//
//====================================//
//      DATA TYPE                     //
//====================================//
stbool prod_dt(
    struct dt* dt,
    struct token** next
);
stbool proc_dt(
    struct dt* dt,
    struct processing_context* ctx
);

//====================================//
//      STRUCT DEFINITION             //
//====================================//
//! @return
//!     #PROD_SUCCESS on successful production.
//! @return
//!     #PROD_FAILURE_SOFT on potentially recoverable production failure.
//! @return
//!     #PROD_FAILURE_HARD on unrecoverable production failure.
int prod_struct_def(
    struct struct_def* def,
    struct token** next
);
stbool proc_struct_def(
    struct struct_def* def,
    struct processing_context* ctx
);

//====================================//
//      FUNCTION SIGNATURE            //
//====================================//
stbool prod_func_sig(
    struct func_sig* func_sig,
    narr_t(struct id)* param_ids,
    struct token** next
);
stbool proc_func_sig(
    struct func_sig* func_sig,
    struct processing_context* ctx
);

//============================================================================//
//      LITERAL VALUE                                                         //
//============================================================================//
stbool prod_literal(
    struct literal* literal,
    struct token** next
);
stbool proc_literal(
    struct literal* literal,
    struct processing_context* ctx
);

//============================================================================//
//      EXPRESSION                                                            //
//============================================================================//
stbool prod_expr(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_assignment(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_assignment_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_logical_or(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_logical_or_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_logical_and(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_logical_and_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_bitwise_or(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_bitwise_or_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_bitwise_xor(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_bitwise_xor_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_bitwise_and(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_bitwise_and_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_relational_exact(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_relational_exact_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_relational_inexact(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_relational_inexact_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_shift(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_shift_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_additive(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_additive_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_multiplicative(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_multiplicative_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_binary_cast(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_binary_cast_p(
    struct expr* bexpr,
    struct token** next
);
stbool prod_expr_unary(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_postfix(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_postfix_p(
    struct expr* expr,
    struct token** next
);
stbool prod_expr_primary(
    struct expr* expr,
    struct token** next
);

//! Check if the LHS and RHS of the provided binary expression are both base
//! types.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are both base types.
//! @return
//!     #STBOOL_FAILURE` otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_xhs_are_base(struct expr const* bexpr);

//! Check if the LHS and RHS of the provided binary expression are both
//! pointers.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are both pointers.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_xhs_are_ptr(struct expr const* bexpr);

//! Check if the LHS and RHS of the provided binary expression are not `void`.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not `void`.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_xhs_are_not_void(struct expr const* bexpr);

//! Check if the LHS and RHS of the provided binary expression are not integer
//! types.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not integers.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_xhs_are_not_integer(struct expr const* bexpr);

//! Check if the LHS and RHS of the provided binary expression are not floating
//! point types.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not floating point types.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_xhs_are_not_floating_point(struct expr const* bexpr);

//! Check if the LHS and RHS of the provided binary expression are not `bool`
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not `bool`.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_xhs_are_not_bool(struct expr const* bexpr);

//! Check if the LHS and RHS of the provided binary expression are not `char`
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are not `char`.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_xhs_are_not_char(struct expr const* bexpr);

//! Check if the LHS of the provided assignment expression is an lvalue and the
//! RHS of the provided expression is an rvalue, i.e. the LHS and RHS of the
//! expression have valid value categories for an assignment expression.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!      Assumes the @p bexpr is one of the binary assignment expressions.
stbool check_assignment_value_categories(struct expr const* bexpr);

//! Check if the LHS of the provided assignment expression yields a mutable
//! value.
//! @return
//!     #STBOOL_SUCCESS if the LHS is mutable.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!      Assumes the @p bexpr is one of the binary assignment expressions.
stbool check_assignment_mutability_constraints(struct expr const* bexpr);

//! Check if the RHS of the provided assignment expression can be implicitly
//! converted to the LHS of the assignment expression.
//! @return
//!     #STBOOL_SUCCESS if the RHS data type can be converted to the LHS data
//!     type.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!      Assumes the @p bexpr is one of the binary assignment expressions.
stbool check_assignment_implicit_conversion(struct expr const* bexpr);

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for a bit shift expression expression.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_bit_shift_compatability(struct expr const* bexpr);

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for an arithmetic binary expression acting on base types with the
//! same data type class. For example the plus expression in
//! ````
//! let foo : s64;
//! let bar : const s64;
//! foo + bar;
//! ````
//! is compatible but the plus expression in
//! ````
//! let foo : s8;
//! let bar : s64;
//! foo + bar;
//! ````
//! is not.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
//! @note
//!     Arithmetic operators are not allowed for `bool` types.
stbool check_type_equal_arithmetic_compatability(struct expr const* bexpr);

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for a bitwise binary expression acting on base types with the
//! same data type class. Bitwise binary expressions are valid only for integer
//! types and the bool type.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_type_equal_bitwise_compatability(struct expr const* bexpr);

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for a relational binary expression.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_type_equal_relational_compatability(struct expr const* bexpr);

//! Check if the LHS and RHS side of the provided binary expression are
//! compatible for a logical binary expression.
//! @return
//!     #STBOOL_SUCCESS if the LHS and RHS are compatible.
//! @return
//!     #STBOOL_FAILURE otherwise.
//! @note
//!     Logs error messages as appropriate.
//! @note
//!     Assumes @p bexpr is binary.
stbool check_type_equal_logical_compatability(struct expr const* bexpr);

stbool proc_expr_primary(
    struct expr* expr,
    struct processing_context* ctx
);
stbool proc_expr_postfix(
    struct expr* expr,
    struct processing_context* ctx
);
stbool proc_expr_unary(
    struct expr* expr,
    struct processing_context* ctx
);
stbool proc_expr_binary(
    struct expr* expr,
    struct processing_context* ctx
);
stbool proc_expr(
    struct expr* expr,
    struct processing_context* ctx
);

//============================================================================//
//      IN-SOURCE IDENTIFIER DECLARATION                                      //
//============================================================================//
//====================================//
//      VARIABLE DECLARATION          //
//====================================//
//! @return
//!     #PROD_SUCCESS on successful production.
//! @return
//!     #PROD_FAILURE_SOFT on potentially recoverable production failure.
//! @return
//!     #PROD_FAILURE_HARD on unrecoverable production failure.
int prod_var_decl(
    struct var_decl* decl,
    struct token** next
);
stbool proc_var_decl(
    struct var_decl* decl,
    bool is_stack_decl,
    struct processing_context* ctx
);

//====================================//
//      FUNCTION DECLARATION          //
//====================================//
//! @return
//!     #PROD_SUCCESS on successful production.
//! @return
//!     #PROD_FAILURE_SOFT on potentially recoverable production failure.
//! @return
//!     #PROD_FAILURE_HARD on unrecoverable production failure.
int prod_func_decl(
    struct func_decl* decl,
    struct token** next
);
stbool proc_func_decl(
    struct func_decl* decl,
    struct processing_context* ctx
);

//====================================//
//      MEMBER VARIABLE               //
//====================================//
stbool prod_member_var(
    struct member_var* memb,
    struct token** next
);
stbool proc_member_var(
    struct member_var* memb,
    struct processing_context* ctx
);

//====================================//
//      MEMBER FUNCTION               //
//====================================//
stbool prod_member_func(
    struct member_func* memb,
    struct token** next
);
stbool proc_member_func(
    struct member_func* memb,
    struct processing_context* ctx
);

//====================================//
//      STRUCT DECLARATION            //
//====================================//
//! @return
//!     #PROD_SUCCESS on successful production.
//! @return
//!     #PROD_FAILURE_SOFT on potentially recoverable production failure.
//! @return
//!     #PROD_FAILURE_HARD on unrecoverable production failure.
int prod_struct_decl(
    struct struct_decl* decl,
    struct token** next
);
stbool proc_struct_decl(
    struct struct_decl* decl,
    struct processing_context* ctx
);

//============================================================================//
//      CONTROL CONSTRUCTS                                                    //
//============================================================================//
//====================================//
//      LOOP                          //
//====================================//
//! @return
//!     #PROD_SUCCESS on successful production.
//! @return
//!     #PROD_FAILURE_SOFT on potentially recoverable production failure.
//! @return
//!     #PROD_FAILURE_HARD on unrecoverable production failure.
int prod_loop(
    struct loop* loop,
    struct token** next
);
stbool proc_loop(
    struct loop* loop,
    struct processing_context* ctx
);

//============================================================================//
//      ALIAS                                                                 //
//============================================================================//
//! @return
//!     #PROD_SUCCESS on successful production.
//! @return
//!     #PROD_FAILURE_SOFT on potentially recoverable production failure.
//! @return
//!     #PROD_FAILURE_HARD on unrecoverable production failure.
int prod_alias(
    struct alias* alias,
    struct token** next
);
stbool proc_alias(
    struct alias* alias,
    struct processing_context* ctx
);

//============================================================================//
//      STATEMENT                                                             //
//============================================================================//
stbool prod_stmt(
    struct stmt* stmt,
    struct token** next
);
stbool proc_stmt(
    struct stmt* stmt,
    struct processing_context* ctx
);

//============================================================================//
//      LEXICAL SCOPE                                                         //
//============================================================================//
//====================================//
//      SCOPE                         //
//====================================//
//! @return
//!     #PROD_SUCCESS on successful production.
//! @return
//!     #PROD_FAILURE_SOFT on potentially recoverable production failure.
//! @return
//!     #PROD_FAILURE_HARD on unrecoverable production failure.
int prod_scope(
    struct scope* scope,
    struct token** next
);
stbool preproc_scope(
    struct scope* scope,
    struct processing_context* ctx
);
stbool proc_scope(
    struct scope* scope,
    struct processing_context* ctx
);

//! Perform the syntax analysis phase of the compiling process.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool dophase_syntax_analysis(struct module* module);
//! Perform the semantic analysis phase of the compiling process.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool dophase_semantic_analysis(struct module* module);
