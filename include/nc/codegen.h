/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include "config.h"
#include "nir.h"

//! Perform the code generation phase of the compiling process.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool dophase_codegen(struct module* module);

//============================================================================//
//      NATIVE                                                                //
//============================================================================//
//! Generate native code.
//! @param module
//!     Target module of compilation.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool native_codegen(struct module* module);

//============================================================================//
//      C99                                                                   //
//============================================================================//
struct c99_codegen_context
{
    struct module* module;
    size_t indent_level;
    uint32_t current_scope_uid;

    struct scope* defer_stack[4096];
    size_t defer_stack_len;

    nstr_t c_code_nstr;
};
//! @init
void c99_codegen_context_init(struct c99_codegen_context* ctx, struct module* module);
//! @fini
void c99_codegen_context_fini(struct c99_codegen_context* ctx);

//! Concatenate the a format string with arguments to the back of C source code
//! nstring within the provided #c99_codegen_context.
//! @param p_context
//!     Pointer to a #c99_codegen_context.
//! @param ...
//!     Format string with arguments that will be appended to the c99 source
//!     code in @p p_context.
#define C99_SRC_APPEND(p_context, ...) \
    nstr_cat_fmt(&((p_context)->c_code_nstr), __VA_ARGS__);

//! Generate C99 code for this compilation unit.
//! @param module
//!     Target module of compilation.
//! @param out_path_override @nullable
//!     Override for the output file path.
//! @return
//!     #STBOOL_SUCCESS on success.
//! @return
//!     #STBOOL_FAILURE on failure.
stbool c99_codegen(struct module* module, char const* out_path_override);

void c99_codegen_scope(
    struct c99_codegen_context* ctx,
    struct scope* scope
);

void c99_codegen_stmt(
    struct c99_codegen_context* ctx,
    struct stmt* stmt
);

void c99_codegen_alias(
    struct c99_codegen_context* ctx,
    struct alias* alias
);

void c99_codegen_var_decl(
    struct c99_codegen_context* ctx,
    struct var_decl* decl
);

void c99_codegen_func_decl(
    struct c99_codegen_context* ctx,
    struct func_decl* decl
);

void c99_codegen_struct_decl(
    struct c99_codegen_context* ctx,
    struct struct_decl* decl,
    struct struct_def* def
);

void c99_codegen_loop(
    struct c99_codegen_context* ctx,
    struct loop* loop
);

void c99_codegen_expr(
    struct c99_codegen_context* ctx,
    struct expr* expr
);

void c99_codegen_id(
    struct c99_codegen_context* ctx,
    struct id* id
);

void c99_codegen_dt(
    struct c99_codegen_context* ctx,
    struct dt* dt,
    struct id* id
);

void c99_codegen_struct_def(
    struct c99_codegen_context* ctx,
    struct struct_def* def
);

void c99_codegen_literal(
    struct c99_codegen_context* ctx,
    struct literal* literal
);
