/* Copyright 2018 N-Lang Project Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma once
#include <nutils/nutils.h>

enum _target
{
    TARGET_UNSET,

    TARGET_NATIVE,
    TARGET_C99
};

struct _config
{
    enum _target target;
    bool is_compile_only;
    bool is_debug_mode;
    bool is_metric_mode;
    bool is_gen_debug_info;
    bool is_optimize;
    bool is_save_temps;
    char const* input_file;
    char const* output_file;
};
extern struct _config config;

void print_usage(void);
stbool prod_config(int argc, char** argv);
stbool proc_config(void);
